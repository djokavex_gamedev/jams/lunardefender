extends Node

# Camera signals
signal shake_camera(amount)

# Enemy signals
signal add_enemy(type)
signal remove_enemy(type)

# Gold signals
signal add_gold(amount)
signal add_ground_gold(amount)
signal gold_amount_changed()
signal ground_gold_amount_changed()
signal add_super_gold(amount)
signal super_gold_amount_changed()

# Player points
signal add_points(amount)

# Tower signals
signal unholding_tower()
signal dropped_tower()
signal tower_upgraded()

# Miner signals
signal miner_selected(selected)
signal new_miner()

# Collector signals
signal create_collector()
signal collector_selected(selected)
signal update_collector_number(number_out, total_number)

# Bonus signals
signal collect_bonus(number, type)
signal unholding_bonus()
signal update_bonus()

# Game phase signals
signal force_start_phase()

# Laser signals
signal spawn_laser(i_movement, start_position, i_size, i_damage, i_color, speed)

