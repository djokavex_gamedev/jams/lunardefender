extends Node

# Signals
signal select_tower(tower)

# Variables
var holding_tower = null
var tower_offset = Vector2(0, 0);
var selected_tower = null
var selecting_collect_target = null
var selecting_miner = false
var holding_bonus = null
var bonus_offset = Vector2(0, 0);


func _ready():
	#Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	var _result = connect("select_tower", self, "on_select_tower")
	_result = EventBus.connect("miner_selected", self, "miner_selected")
	_result = EventBus.connect("collector_selected", self, "collector_selected")
	pass

func _process(_delta):
	if selected_tower == null:
		var building_area = get_node_or_null("../BattleField/Ground/SelectedBuilding")
		if building_area != null:
			building_area.visible = false
	pass

func _unhandled_input(event : InputEvent):
		#print("input process de merde")
		if event is InputEventMouseMotion:
				if holding_tower && holding_tower.picked:
					holding_tower.global_position = get_viewport().canvas_transform.affine_inverse().xform(event.position) - tower_offset;
				if holding_bonus && holding_bonus.picked:
					holding_bonus.global_position = get_viewport().canvas_transform.affine_inverse().xform(event.position) - bonus_offset;
		elif event is InputEventMouseButton and event.is_pressed():
			match event.button_index:
				BUTTON_LEFT:
					# left button clicked
					if selecting_collect_target:
						pass
					else:
						if holding_tower && holding_tower.picked:
							if holding_tower.drop_on_ground():
								match holding_tower.type:
									global.tower_type.TOWER_LASER:
										EventBus.emit_signal("add_gold", -global.towers["small"]["price"])
									global.tower_type.TOWER_BOMB:
										EventBus.emit_signal("add_gold", -global.towers["bomber"]["price"])
									global.tower_type.TOWER_THUNDER:
										EventBus.emit_signal("add_gold", -global.towers["thunder"]["price"])
									global.tower_type.TOWER_HEAL:
										EventBus.emit_signal("add_gold", -global.towers["healer"]["price"])
									global.tower_type.TOWER_SNIPER:
										EventBus.emit_signal("add_gold", -global.towers["sniper"]["price"])
									global.tower_type.TOWER_FOCUS_LASER:
										EventBus.emit_signal("add_gold", -global.towers["focus_laser"]["price"])
									_:
										EventBus.emit_signal("add_gold", -global.towers["small"]["price"])
								holding_tower = null
								EventBus.emit_signal("unholding_tower")
						else:
							if holding_bonus && holding_bonus.picked:
								EventBus.emit_signal("collect_bonus", -1, holding_bonus.type)
								holding_bonus.launch_bonus()
								holding_bonus = null
								EventBus.emit_signal("unholding_bonus")
							#print("Pas de holding")
							pass
						
					if selected_tower:
						pass
				BUTTON_RIGHT:
					# right button clicked
					if holding_tower && holding_tower.picked:
						holding_tower.queue_free()
						holding_tower = null
						EventBus.emit_signal("unholding_tower")
						audio_manager.play_sfx(load("res://assets/sfx/gui/bipBack.wav"), 0, 1)
						
					if holding_bonus && holding_bonus.picked:
						holding_bonus.queue_free()
						holding_bonus = null
						EventBus.emit_signal("unholding_bonus")
						audio_manager.play_sfx(load("res://assets/sfx/gui/bipBack.wav"), 0, 1)
					
					if selected_tower:
						#print("stop selecting tower")
						if is_instance_valid(selected_tower):
							selected_tower.tower_unselected()
						selected_tower = null
						get_node("../BattleField/Ground/SelectedBuilding").visible = false
						audio_manager.play_sfx(load("res://assets/sfx/gui/bipBack.wav"), 0, 1)
						pass
						
					if selecting_collect_target:
						selecting_collect_target = null
						EventBus.emit_signal("collector_selected", false)
						audio_manager.play_sfx(load("res://assets/sfx/gui/bipBack.wav"), 0, 1)
						pass
						
					if selecting_miner:
						EventBus.emit_signal("miner_selected", false)
						audio_manager.play_sfx(load("res://assets/sfx/gui/bipBack.wav"), 0, 1)
		else:
			pass

func on_select_tower(tower):
	if selected_tower:
		if is_instance_valid(selected_tower):
			selected_tower.tower_unselected()
	selected_tower = tower
	selected_tower.tower_selected()
	var building_area = get_node("../BattleField/Ground/SelectedBuilding")
	building_area.rect_size = Vector2(tower.tower_size * 2.0, tower.tower_size * 2.0)
	building_area.rect_global_position = tower.global_position + Vector2(-tower.tower_size / 2.0,-tower.tower_size / 2.0)
	building_area.visible = true
	building_area.get_node("AnimationPlayer").play("idle")
	pass

func miner_selected(selected):
	selecting_miner = selected
	pass

func collector_selected(selected):
	selecting_collect_target = selected
	pass
