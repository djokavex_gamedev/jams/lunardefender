extends Position2D

onready var label = get_node("Label")
onready var tween = get_node("Tween")
var value = 0
var color = null
var target_scale = Vector2(1.0,1.0)

var velocity = Vector2(0,0)

# Called when the node enters the scene tree for the first time.
func _ready():
	if color != null:
		# Force color
		label.set("custom_colors/font_color", color)
		pass
	else:
		if value > 0:
			label.set("custom_colors/font_color", Color("ff3131"))
		else:
			if value < 0:
				label.set("custom_colors/font_color", Color("2eff27"))
				value = -value
			else:
				label.set("custom_colors/font_color", Color("ffffff"))
	
	label.set_text(str(value))
	randomize()
	var side_movement = randi() % 60 - 30
	velocity = Vector2(side_movement, 60)
	
	tween.interpolate_property(self, 'scale', scale, target_scale, 0.2, Tween.TRANS_LINEAR, Tween.EASE_OUT)
	tween.interpolate_property(self, 'scale', target_scale, Vector2(0.1,0.1), 0.7, Tween.TRANS_LINEAR, Tween.EASE_OUT, 0.3)
	tween.start()
	
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	global_rotation = 0
	global_position -= velocity * delta
	pass


func _on_Tween_tween_all_completed():
	self.queue_free()
	pass # Replace with function body.
