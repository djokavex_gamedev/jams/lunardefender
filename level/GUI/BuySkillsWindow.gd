extends Control


# Called when the node enters the scene tree for the first time.
func _ready():
	$AnimationPlayer.play("start")
	$SkillsContainer/SkillTree1/Step1.icon = load(global.unlockable_skills["tree1"]["step1_img_path"])
	$SkillsContainer/SkillTree1/Step2.icon = load(global.unlockable_skills["tree1"]["step2_img_path"])
	$SkillsContainer/SkillTree1/Step3.icon = load(global.unlockable_skills["tree1"]["step3_img_path"])
	$SkillsContainer/SkillTree1/Step4.icon = load(global.unlockable_skills["tree1"]["step4_img_path"])
	
	$SkillsContainer/SkillTree2/Step1.icon = load(global.unlockable_skills["tree2"]["step1_img_path"])
	$SkillsContainer/SkillTree2/Step2.icon = load(global.unlockable_skills["tree2"]["step2_img_path"])
	$SkillsContainer/SkillTree2/Step3.icon = load(global.unlockable_skills["tree2"]["step3_img_path"])
	$SkillsContainer/SkillTree2/Step4.icon = load(global.unlockable_skills["tree2"]["step4_img_path"])
	
	$SkillsContainer/SkillTree3/Step1.icon = load(global.unlockable_skills["tree3"]["step1_img_path"])
	$SkillsContainer/SkillTree3/Step2.icon = load(global.unlockable_skills["tree3"]["step2_img_path"])
	$SkillsContainer/SkillTree3/Step3.icon = load(global.unlockable_skills["tree3"]["step3_img_path"])
	$SkillsContainer/SkillTree3/Step4.icon = load(global.unlockable_skills["tree3"]["step4_img_path"])
	
	$SkillsContainer/SkillTree4/Step1.icon = load(global.unlockable_skills["tree4"]["step1_img_path"])
	$SkillsContainer/SkillTree4/Step2.icon = load(global.unlockable_skills["tree4"]["step2_img_path"])
	$SkillsContainer/SkillTree4/Step3.icon = load(global.unlockable_skills["tree4"]["step3_img_path"])
	$SkillsContainer/SkillTree4/Step4.icon = load(global.unlockable_skills["tree4"]["step4_img_path"])
	
	$SkillsContainer/SkillTree5/Step1.icon = load(global.unlockable_skills["tree5"]["step1_img_path"])
	$SkillsContainer/SkillTree5/Step2.icon = load(global.unlockable_skills["tree5"]["step2_img_path"])
	$SkillsContainer/SkillTree5/Step3.icon = load(global.unlockable_skills["tree5"]["step3_img_path"])
	$SkillsContainer/SkillTree5/Step4.icon = load(global.unlockable_skills["tree5"]["step4_img_path"])
	
	update_buyable_skills()
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	# WHYYYYYYYYYYYY
	$SkillsContainer/SkillTree1.rect_rotation = 180.0
	$SkillsContainer/SkillTree1/Step1.rect_rotation = 180.0
	$SkillsContainer/SkillTree1/Step2.rect_rotation = 180.0
	$SkillsContainer/SkillTree1/Step3.rect_rotation = 180.0
	$SkillsContainer/SkillTree1/Step4.rect_rotation = 180.0
	#$SkillsContainer/SkillTree1/Step1.rect_scale = Vector2(2.0, 2.0)
	
	$SkillsContainer/SkillTree2.rect_rotation = 180.0
	$SkillsContainer/SkillTree2/Step1.rect_rotation = 180.0
	$SkillsContainer/SkillTree2/Step2.rect_rotation = 180.0
	$SkillsContainer/SkillTree2/Step3.rect_rotation = 180.0
	$SkillsContainer/SkillTree2/Step4.rect_rotation = 180.0
	
	$SkillsContainer/SkillTree3.rect_rotation = 180.0
	$SkillsContainer/SkillTree3/Step1.rect_rotation = 180.0
	$SkillsContainer/SkillTree3/Step2.rect_rotation = 180.0
	$SkillsContainer/SkillTree3/Step3.rect_rotation = 180.0
	$SkillsContainer/SkillTree3/Step4.rect_rotation = 180.0
	
	$SkillsContainer/SkillTree4.rect_rotation = 180.0
	$SkillsContainer/SkillTree4/Step1.rect_rotation = 180.0
	$SkillsContainer/SkillTree4/Step2.rect_rotation = 180.0
	$SkillsContainer/SkillTree4/Step3.rect_rotation = 180.0
	$SkillsContainer/SkillTree4/Step4.rect_rotation = 180.0
	
	$SkillsContainer/SkillTree5.rect_rotation = 180.0
	$SkillsContainer/SkillTree5/Step1.rect_rotation = 180.0
	$SkillsContainer/SkillTree5/Step2.rect_rotation = 180.0
	$SkillsContainer/SkillTree5/Step3.rect_rotation = 180.0
	$SkillsContainer/SkillTree5/Step4.rect_rotation = 180.0
	pass

func update_buyable_skills():
	var available_experience = global.player_experience-global.experience_used
	$Experience.text = tr("KEY_XP_AVAILABLE") + ": " + str(available_experience) +"\n"+tr("KEY_XP_USED")+": " + str(global.experience_used)
	
	# TREE 1
	if global.unlockable_skills["tree1"]["step1"]:
		enable_skill($SkillsContainer/SkillTree1/Step1)
		if global.unlockable_skills["tree1"]["step2"]:
			enable_skill($SkillsContainer/SkillTree1/Step2)
			if global.unlockable_skills["tree1"]["step3"]:
				enable_skill($SkillsContainer/SkillTree1/Step3)
				if global.unlockable_skills["tree1"]["step4"]:
					enable_skill($SkillsContainer/SkillTree1/Step4)
				else:
					if global.unlockable_skills["tree1"]["step4_cost"] <= (available_experience):
						buyable_skill($SkillsContainer/SkillTree1/Step4)
					else:
						disable_skill($SkillsContainer/SkillTree1/Step4)
						pass
					pass
				
				pass
			else:
				if global.unlockable_skills["tree1"]["step3_cost"] <= (available_experience):
					buyable_skill($SkillsContainer/SkillTree1/Step3)
				else:
					disable_skill($SkillsContainer/SkillTree1/Step3)
					pass
				pass
				disable_skill($SkillsContainer/SkillTree1/Step4)
			
			pass
		else:
			if global.unlockable_skills["tree1"]["step2_cost"] <= (available_experience):
				buyable_skill($SkillsContainer/SkillTree1/Step2)
			else:
				disable_skill($SkillsContainer/SkillTree1/Step2)
				pass
			pass
			disable_skill($SkillsContainer/SkillTree1/Step3)
			disable_skill($SkillsContainer/SkillTree1/Step4)
		
		pass
	else:
		if global.unlockable_skills["tree1"]["step1_cost"] <= (available_experience):
			buyable_skill($SkillsContainer/SkillTree1/Step1)
		else:
			disable_skill($SkillsContainer/SkillTree1/Step1)
			pass
		disable_skill($SkillsContainer/SkillTree1/Step2)
		disable_skill($SkillsContainer/SkillTree1/Step3)
		disable_skill($SkillsContainer/SkillTree1/Step4)
		pass
		
	# TREE 2
	if global.unlockable_skills["tree2"]["step1"]:
		enable_skill($SkillsContainer/SkillTree2/Step1)
		if global.unlockable_skills["tree2"]["step2"]:
			enable_skill($SkillsContainer/SkillTree2/Step2)
			if global.unlockable_skills["tree2"]["step3"]:
				enable_skill($SkillsContainer/SkillTree2/Step3)
				if global.unlockable_skills["tree2"]["step4"]:
					enable_skill($SkillsContainer/SkillTree2/Step4)
				else:
					if global.unlockable_skills["tree2"]["step4_cost"] <= (available_experience):
						buyable_skill($SkillsContainer/SkillTree2/Step4)
					else:
						disable_skill($SkillsContainer/SkillTree2/Step4)
						pass
					pass
				
				pass
			else:
				if global.unlockable_skills["tree2"]["step3_cost"] <= (available_experience):
					buyable_skill($SkillsContainer/SkillTree2/Step3)
				else:
					disable_skill($SkillsContainer/SkillTree2/Step3)
					pass
				pass
				disable_skill($SkillsContainer/SkillTree2/Step4)
			
			pass
		else:
			if global.unlockable_skills["tree2"]["step2_cost"] <= (available_experience):
				buyable_skill($SkillsContainer/SkillTree2/Step2)
			else:
				disable_skill($SkillsContainer/SkillTree2/Step2)
				pass
			pass
			disable_skill($SkillsContainer/SkillTree2/Step3)
			disable_skill($SkillsContainer/SkillTree2/Step4)
		
		pass
	else:
		if global.unlockable_skills["tree2"]["step1_cost"] <= (available_experience):
			buyable_skill($SkillsContainer/SkillTree2/Step1)
		else:
			disable_skill($SkillsContainer/SkillTree2/Step1)
			pass
		disable_skill($SkillsContainer/SkillTree2/Step2)
		disable_skill($SkillsContainer/SkillTree2/Step3)
		disable_skill($SkillsContainer/SkillTree2/Step4)
		pass
		
	# TREE 3
	if global.unlockable_skills["tree3"]["step1"]:
		enable_skill($SkillsContainer/SkillTree3/Step1)
		if global.unlockable_skills["tree3"]["step2"]:
			enable_skill($SkillsContainer/SkillTree3/Step2)
			if global.unlockable_skills["tree3"]["step3"]:
				enable_skill($SkillsContainer/SkillTree3/Step3)
				if global.unlockable_skills["tree3"]["step4"]:
					enable_skill($SkillsContainer/SkillTree3/Step4)
				else:
					if global.unlockable_skills["tree3"]["step4_cost"] <= (available_experience):
						buyable_skill($SkillsContainer/SkillTree3/Step4)
					else:
						disable_skill($SkillsContainer/SkillTree3/Step4)
						pass
					pass
				
				pass
			else:
				if global.unlockable_skills["tree3"]["step3_cost"] <= (available_experience):
					buyable_skill($SkillsContainer/SkillTree3/Step3)
				else:
					disable_skill($SkillsContainer/SkillTree3/Step3)
					pass
				pass
				disable_skill($SkillsContainer/SkillTree3/Step4)
			
			pass
		else:
			if global.unlockable_skills["tree3"]["step2_cost"] <= (available_experience):
				buyable_skill($SkillsContainer/SkillTree3/Step2)
			else:
				disable_skill($SkillsContainer/SkillTree3/Step2)
				pass
			pass
			disable_skill($SkillsContainer/SkillTree3/Step3)
			disable_skill($SkillsContainer/SkillTree3/Step4)
		
		pass
	else:
		if global.unlockable_skills["tree3"]["step1_cost"] <= (available_experience):
			buyable_skill($SkillsContainer/SkillTree3/Step1)
		else:
			disable_skill($SkillsContainer/SkillTree3/Step1)
			pass
		disable_skill($SkillsContainer/SkillTree3/Step2)
		disable_skill($SkillsContainer/SkillTree3/Step3)
		disable_skill($SkillsContainer/SkillTree3/Step4)
		pass
		
	# TREE 4
	if global.unlockable_skills["tree4"]["step1"]:
		enable_skill($SkillsContainer/SkillTree4/Step1)
		if global.unlockable_skills["tree4"]["step2"]:
			enable_skill($SkillsContainer/SkillTree4/Step2)
			if global.unlockable_skills["tree4"]["step3"]:
				enable_skill($SkillsContainer/SkillTree4/Step3)
				if global.unlockable_skills["tree4"]["step4"]:
					enable_skill($SkillsContainer/SkillTree4/Step4)
				else:
					if global.unlockable_skills["tree4"]["step4_cost"] <= (available_experience):
						buyable_skill($SkillsContainer/SkillTree4/Step4)
					else:
						disable_skill($SkillsContainer/SkillTree4/Step4)
						pass
					pass
				
				pass
			else:
				if global.unlockable_skills["tree4"]["step3_cost"] <= (available_experience):
					buyable_skill($SkillsContainer/SkillTree4/Step3)
				else:
					disable_skill($SkillsContainer/SkillTree4/Step3)
					pass
				pass
				disable_skill($SkillsContainer/SkillTree4/Step4)
			
			pass
		else:
			if global.unlockable_skills["tree4"]["step2_cost"] <= (available_experience):
				buyable_skill($SkillsContainer/SkillTree4/Step2)
			else:
				disable_skill($SkillsContainer/SkillTree4/Step2)
				pass
			pass
			disable_skill($SkillsContainer/SkillTree4/Step3)
			disable_skill($SkillsContainer/SkillTree4/Step4)
		
		pass
	else:
		if global.unlockable_skills["tree4"]["step1_cost"] <= (available_experience):
			buyable_skill($SkillsContainer/SkillTree4/Step1)
		else:
			disable_skill($SkillsContainer/SkillTree4/Step1)
			pass
		disable_skill($SkillsContainer/SkillTree4/Step2)
		disable_skill($SkillsContainer/SkillTree4/Step3)
		disable_skill($SkillsContainer/SkillTree4/Step4)
		pass
		
	# TREE 5
	if global.unlockable_skills["tree5"]["step1"]:
		enable_skill($SkillsContainer/SkillTree5/Step1)
		if global.unlockable_skills["tree5"]["step2"]:
			enable_skill($SkillsContainer/SkillTree5/Step2)
			if global.unlockable_skills["tree5"]["step3"]:
				enable_skill($SkillsContainer/SkillTree5/Step3)
				if global.unlockable_skills["tree5"]["step4"]:
					enable_skill($SkillsContainer/SkillTree5/Step4)
				else:
					if global.unlockable_skills["tree5"]["step4_cost"] <= (available_experience):
						buyable_skill($SkillsContainer/SkillTree5/Step4)
					else:
						disable_skill($SkillsContainer/SkillTree5/Step4)
						pass
					pass
				
				pass
			else:
				if global.unlockable_skills["tree5"]["step3_cost"] <= (available_experience):
					buyable_skill($SkillsContainer/SkillTree5/Step3)
				else:
					disable_skill($SkillsContainer/SkillTree5/Step3)
					pass
				pass
				disable_skill($SkillsContainer/SkillTree5/Step4)
			
			pass
		else:
			if global.unlockable_skills["tree5"]["step2_cost"] <= (available_experience):
				buyable_skill($SkillsContainer/SkillTree5/Step2)
			else:
				disable_skill($SkillsContainer/SkillTree5/Step2)
				pass
			pass
			disable_skill($SkillsContainer/SkillTree5/Step3)
			disable_skill($SkillsContainer/SkillTree5/Step4)
		
		pass
	else:
		if global.unlockable_skills["tree5"]["step1_cost"] <= (available_experience):
			buyable_skill($SkillsContainer/SkillTree5/Step1)
		else:
			disable_skill($SkillsContainer/SkillTree5/Step1)
			pass
		disable_skill($SkillsContainer/SkillTree5/Step2)
		disable_skill($SkillsContainer/SkillTree5/Step3)
		disable_skill($SkillsContainer/SkillTree5/Step4)
		pass
		
	update_tower_stats()
	global.change_tower_available()
	global.save_game_data(global.current_save)
	pass

func enable_skill(skill_button):
	skill_button.disabled = true
	skill_button.icon = load("res://assets/img/GUI/settings_0005_small_button1.png")
	get_node("Tween").interpolate_property(skill_button, "modulate", skill_button.modulate, Color(1.2, 1.2, 1.2, 1.0), 0.3, Tween.TRANS_LINEAR, Tween.EASE_OUT)
	get_node("Tween").interpolate_property(skill_button, "self_modulate", skill_button.self_modulate, Color(1.5, 1.5, 1.5, 1.0), 0.3, Tween.TRANS_LINEAR, Tween.EASE_OUT)
	get_node("Tween").start()
	
	#skill_button.modulate = Color(1.2, 1.2, 1.2, 1.0)
	#skill_button.self_modulate = Color(1.5, 1.5, 1.5, 1.0)
#	skill_button.get_node("Particles2D").emitting = true
#	skill_button.get_node("Particles2D").modulate = Color(0, 1, 0, 0.5)
#	skill_button.get_node("Particles2D").visible = true
	if skill_button.get_node_or_null("Line2D") != null:
		skill_button.get_node("Beam").visible = true
		skill_button.get_node("Beam").rect_global_position = skill_button.rect_global_position - Vector2(210, 30)
		skill_button.get_node("Line2D").visible = false
		skill_button.get_node("Line2D").width = 4
		skill_button.get_node("Line2D").modulate = Color(0.8,0.8,1.3, 1.0)
	pass
func disable_skill(skill_button):
	skill_button.disabled = true
	skill_button.icon = load("res://assets/img/GUI/settings_0006_small_button2.png")
	get_node("Tween").interpolate_property(skill_button, "modulate", skill_button.modulate, Color(1, 1, 1, 1.0), 0.3, Tween.TRANS_LINEAR, Tween.EASE_OUT)
	get_node("Tween").interpolate_property(skill_button, "self_modulate", skill_button.self_modulate, Color(1.0, 0.5, 0.5, 1), 0.3, Tween.TRANS_LINEAR, Tween.EASE_OUT)
	get_node("Tween").start()
	#skill_button.modulate = Color(1, 1, 1, 1.0)
	#skill_button.self_modulate = Color(0.5, 0.5, 0.5, 0.5)
#	skill_button.get_node("Particles2D").emitting = false
#	skill_button.get_node("Particles2D").visible = false
	if skill_button.get_node_or_null("Line2D") != null:
		skill_button.get_node("Beam").visible = false
		skill_button.get_node("Line2D").visible = true
		skill_button.get_node("Line2D").width = 0.5
		skill_button.get_node("Line2D").modulate = Color(0.8,0.8,1.0, 0.50)
	pass
func buyable_skill(skill_button):
	skill_button.disabled = false
	skill_button.icon = load("res://assets/img/GUI/settings_0005_small_button1.png")
	get_node("Tween").interpolate_property(skill_button, "modulate", skill_button.modulate, Color(1, 1, 1, 1.0), 0.3, Tween.TRANS_LINEAR, Tween.EASE_OUT)
	get_node("Tween").interpolate_property(skill_button, "self_modulate", skill_button.self_modulate, Color(0.5, 0.5, 0.5, 1), 0.3, Tween.TRANS_LINEAR, Tween.EASE_OUT)
	get_node("Tween").start()
	#skill_button.modulate = Color(1, 1, 1, 1.0)
	#skill_button.self_modulate = Color(0.5, 0.5, 0.5, 0.5)
#	skill_button.get_node("Particles2D").emitting = true
#	skill_button.get_node("Particles2D").modulate = Color(1, 1, 1, 0.5)
#	skill_button.get_node("Particles2D").visible = true
	if skill_button.get_node_or_null("Line2D") != null:
		skill_button.get_node("Beam").visible = false
		skill_button.get_node("Line2D").visible = true
		skill_button.get_node("Line2D").width = 0.5
		skill_button.get_node("Line2D").modulate = Color(0.8,0.8,1.0, 0.50)
	pass

#func enable_skill(skill_button):
#	skill_button.disabled = true
#	skill_button.get_node("Particles2D").emitting = true
#	skill_button.get_node("Particles2D").modulate = Color(0, 1, 0, 0.5)
#	skill_button.get_node("Particles2D").visible = true
#	if skill_button.get_node_or_null("Line2D") != null:
#		skill_button.get_node("Line2D").width = 4
#		skill_button.get_node("Line2D").modulate = Color(0.8,0.8,1.3, 1.0)
#	pass
#func disable_skill(skill_button):
#	skill_button.disabled = true
#	skill_button.get_node("Particles2D").emitting = false
#	skill_button.get_node("Particles2D").visible = false
#	if skill_button.get_node_or_null("Line2D") != null:
#		skill_button.get_node("Line2D").width = 0.5
#		skill_button.get_node("Line2D").modulate = Color(0.8,0.8,1.0, 0.50)
#	pass
#func buyable_skill(skill_button):
#	skill_button.disabled = false
#	skill_button.get_node("Particles2D").emitting = true
#	skill_button.get_node("Particles2D").modulate = Color(1, 1, 1, 0.5)
#	skill_button.get_node("Particles2D").visible = true
#	if skill_button.get_node_or_null("Line2D") != null:
#		skill_button.get_node("Line2D").width = 0.5
#		skill_button.get_node("Line2D").modulate = Color(0.8,0.8,1.0, 0.50)
#	pass

func update_tower_stats():
	var damage = global.towers["small"]["swarm_damage"]
	var firerate = global.towers["small"]["swarm_fire_rate"]
	if global.unlockable_skills["tree1"]["step2"]:
		damage = damage + global.unlockable_skills["tree1"]["step2_value"]
	if global.unlockable_skills["tree1"]["step3"]:
		firerate = firerate + global.unlockable_skills["tree1"]["step3_value"]
	$SkillsContainer/VBoxContainer/TowerSmallDescription.text = global.towers["small"]["name"] + "\n" + tr(global.towers["small"]["tooltip"]) % [global.towers["small"]["swarm_health"], damage, firerate]
	
	damage = global.towers["healer"]["swarm_damage"]
	firerate = global.towers["healer"]["swarm_fire_rate"]
	if global.unlockable_skills["tree2"]["step2"]:
		damage = damage + global.unlockable_skills["tree2"]["step2_value"]
	if global.unlockable_skills["tree2"]["step3"]:
		damage = damage + global.unlockable_skills["tree2"]["step3_value"]
	$SkillsContainer/VBoxContainer/TowerHealerDescription.text = global.towers["healer"]["name"] + "\n" + tr(global.towers["healer"]["tooltip"]) % [global.towers["healer"]["swarm_health"], damage, firerate]
	
	damage = global.towers["thunder"]["swarm_damage"]
	firerate = global.towers["thunder"]["swarm_fire_rate"]
	if global.unlockable_skills["tree3"]["step2"]:
		damage = damage + global.unlockable_skills["tree3"]["step2_value"]
	if global.unlockable_skills["tree3"]["step4"]:
		firerate = firerate + global.unlockable_skills["tree3"]["step4_value"]
	$SkillsContainer/VBoxContainer/TowerThunderDescription.text = global.towers["thunder"]["name"] + "\n" + tr(global.towers["thunder"]["tooltip"]) % [global.towers["thunder"]["swarm_health"], damage, firerate]
	
	damage = global.towers["bomber"]["swarm_damage"]
	firerate = global.towers["bomber"]["swarm_fire_rate"]
	if global.unlockable_skills["tree4"]["step2"]:
		damage = damage + global.unlockable_skills["tree4"]["step2_value"]
	$SkillsContainer/VBoxContainer/TowerBomberDescription.text = global.towers["bomber"]["name"] + "\n" + tr(global.towers["bomber"]["tooltip"]) % [global.towers["bomber"]["swarm_health"], damage, firerate]
	
	damage = global.towers["sniper"]["swarm_damage"]
	firerate = global.towers["sniper"]["swarm_fire_rate"]
	$SkillsContainer/VBoxContainer/TowerSniperDescription.text = global.towers["sniper"]["name"] + "\n" + tr(global.towers["sniper"]["tooltip"]) % [global.towers["sniper"]["swarm_health"], damage, firerate]
	
	damage = global.towers["focus_laser"]["swarm_damage"]
	firerate = global.towers["focus_laser"]["swarm_fire_rate"]
	$SkillsContainer/VBoxContainer/TowerFocusDescription.text = global.towers["focus_laser"]["name"] + "\n" + tr(global.towers["focus_laser"]["tooltip"]) % [global.towers["focus_laser"]["swarm_health"], damage, firerate]
	
	if global.is_tower_unlocked(global.tower_type.TOWER_BOMB):
		$SkillsContainer/VBoxContainer/TowerBomberDescription.modulate = Color(1,1,1,1)
	else:
		$SkillsContainer/VBoxContainer/TowerBomberDescription.modulate = Color(0.5,0.5,0.5,1)
	if global.is_tower_unlocked(global.tower_type.TOWER_THUNDER):
		$SkillsContainer/VBoxContainer/TowerThunderDescription.modulate = Color(1,1,1,1)
	else:
		$SkillsContainer/VBoxContainer/TowerThunderDescription.modulate = Color(0.5,0.5,0.5,1)
	if global.is_tower_unlocked(global.tower_type.TOWER_HEAL):
		$SkillsContainer/VBoxContainer/TowerHealerDescription.modulate = Color(1,1,1,1)
	else:
		$SkillsContainer/VBoxContainer/TowerHealerDescription.modulate = Color(0.5,0.5,0.5,1)
	if global.is_tower_unlocked(global.tower_type.TOWER_SNIPER):
		$SkillsContainer/VBoxContainer/TowerSniperDescription.modulate = Color(1,1,1,1)
	else:
		$SkillsContainer/VBoxContainer/TowerSniperDescription.modulate = Color(0.5,0.5,0.5,1)
	if global.is_tower_unlocked(global.tower_type.TOWER_FOCUS_LASER):
		$SkillsContainer/VBoxContainer/TowerFocusDescription.modulate = Color(1,1,1,1)
	else:
		$SkillsContainer/VBoxContainer/TowerFocusDescription.modulate = Color(0.5,0.5,0.5,1)
	
	pass

func _on_ResetButton_pressed():
	audio_manager.play_sfx(load("res://assets/sfx/gui/BipNOK.wav"), 0, 1)
	global.experience_used = 0
	global.unlockable_skills["tree1"]["step1"] = true # TODO: CHANGE
	global.unlockable_skills["tree1"]["step2"] = false
	global.unlockable_skills["tree1"]["step3"] = false
	global.unlockable_skills["tree1"]["step4"] = false
	
	global.unlockable_skills["tree2"]["step1"] = false
	global.unlockable_skills["tree2"]["step2"] = false
	global.unlockable_skills["tree2"]["step3"] = false
	global.unlockable_skills["tree2"]["step4"] = false
	
	global.unlockable_skills["tree3"]["step1"] = false
	global.unlockable_skills["tree3"]["step2"] = false
	global.unlockable_skills["tree3"]["step3"] = false
	global.unlockable_skills["tree3"]["step4"] = false
	
	global.unlockable_skills["tree4"]["step1"] = false
	global.unlockable_skills["tree4"]["step2"] = false
	global.unlockable_skills["tree4"]["step3"] = false
	global.unlockable_skills["tree4"]["step4"] = false
	
	global.unlockable_skills["tree5"]["step1"] = false
	global.unlockable_skills["tree5"]["step2"] = false
	global.unlockable_skills["tree5"]["step3"] = false
	global.unlockable_skills["tree5"]["step4"] = false
	
	update_buyable_skills()
	pass # Replace with function body.
	
func _on_ExitButton_pressed():
	audio_manager.play_sfx(load("res://assets/sfx/gui/bipBack.wav"), 0, 1)
	global.save_game_data(global.current_save)
	$AnimationPlayer.play("quit")
	yield($AnimationPlayer, "animation_finished")
	get_tree().change_scene("res://level/GUI/SelectLevelWindow.tscn")
	pass # Replace with function body.
	
func skill_button_pressed(tree, step):
	var available_experience = global.player_experience-global.experience_used
	if not global.unlockable_skills[tree][step] and (global.unlockable_skills[tree][step+"_cost"] <= (available_experience)):
		audio_manager.play_sfx(load("res://assets/sfx/gui/BipOK.wav"), 0, 1)
		global.unlockable_skills[tree][step] = true
		global.experience_used = global.experience_used + global.unlockable_skills[tree][step+"_cost"]
		pass
		
	update_buyable_skills()
	pass

func skill_button_hover(button, tree, step):
	var button_position = button.rect_global_position
	if button_position.x < 900:
		$Tooltip.rect_global_position = button_position + Vector2(65.0, -20.0)
	else:
		$Tooltip.rect_global_position = button_position + Vector2(-225.0, -20.0)
	
	$Tooltip/Label.text = tr(global.unlockable_skills[tree][step+"_tooltip"])
	$Tooltip.visible = true
	pass

func _on_Tree1Step1_pressed():
	skill_button_pressed("tree1", "step1")
	pass # Replace with function body.


func _on_Tree1Step2_pressed():
	skill_button_pressed("tree1", "step2")
	pass # Replace with function body.


func _on_Tree1Step3_pressed():
	skill_button_pressed("tree1", "step3")
	pass # Replace with function body.


func _on_Tree1Step4_pressed():
	skill_button_pressed("tree1", "step4")
	pass # Replace with function body.





func _on_Tree1Step1_mouse_entered():
	skill_button_hover($SkillsContainer/SkillTree1/Step1, "tree1", "step1")
	pass # Replace with function body.


func _on_Tree1Step1_mouse_exited():
	$Tooltip.visible = false
	pass # Replace with function body.


func _on_Tree1Step2_mouse_entered():
	skill_button_hover($SkillsContainer/SkillTree1/Step2, "tree1", "step2")
	pass # Replace with function body.


func _on_Tree1Step2_mouse_exited():
	$Tooltip.visible = false
	pass # Replace with function body.


func _on_Tree1Step3_mouse_entered():
	skill_button_hover($SkillsContainer/SkillTree1/Step3, "tree1", "step3")
	pass # Replace with function body.


func _on_Tree1Step3_mouse_exited():
	$Tooltip.visible = false
	pass # Replace with function body.


func _on_Tree1Step4_mouse_entered():
	skill_button_hover($SkillsContainer/SkillTree1/Step4, "tree1", "step4")
	pass # Replace with function body.


func _on_Tree1Step4_mouse_exited():
	$Tooltip.visible = false
	pass # Replace with function body.


func _on_Tree2Step1_pressed():
	skill_button_pressed("tree2", "step1")
	pass # Replace with function body.


func _on_Tree2Step1_mouse_entered():
	skill_button_hover($SkillsContainer/SkillTree2/Step1, "tree2", "step1")
	pass # Replace with function body.


func _on_Tree2Step1_mouse_exited():
	$Tooltip.visible = false
	pass # Replace with function body.


func _on_Tree3Step1_pressed():
	skill_button_pressed("tree3", "step1")
	pass # Replace with function body.


func _on_Tree3Step1_mouse_entered():
	skill_button_hover($SkillsContainer/SkillTree3/Step1, "tree3", "step1")
	pass # Replace with function body.


func _on_Tree3Step1_mouse_exited():
	$Tooltip.visible = false
	pass # Replace with function body.


func _on_Tree4Step1_pressed():
	skill_button_pressed("tree4", "step1")
	pass # Replace with function body.


func _on_Tree4Step1_mouse_entered():
	skill_button_hover($SkillsContainer/SkillTree4/Step1, "tree4", "step1")
	pass # Replace with function body.


func _on_Tree4Step1_mouse_exited():
	$Tooltip.visible = false
	pass # Replace with function body.


func _on_Tree2Step2_pressed():
	skill_button_pressed("tree2", "step2")
	pass # Replace with function body.


func _on_Tree2Step3_pressed():
	skill_button_pressed("tree2", "step3")
	pass # Replace with function body.


func _on_Tree2Step4_pressed():
	skill_button_pressed("tree2", "step4")
	pass # Replace with function body.


func _on_Tree3Step2_pressed():
	skill_button_pressed("tree3", "step2")
	pass # Replace with function body.


func _on_Tree3Step3_pressed():
	skill_button_pressed("tree3", "step3")
	pass # Replace with function body.


func _on_Tree3Step4_pressed():
	skill_button_pressed("tree3", "step4")
	pass # Replace with function body.


func _on_Tree4Step2_pressed():
	skill_button_pressed("tree4", "step2")
	pass # Replace with function body.


func _on_Tree4Step3_pressed():
	skill_button_pressed("tree4", "step3")
	pass # Replace with function body.


func _on_Tree4Step4_pressed():
	skill_button_pressed("tree4", "step4")
	pass # Replace with function body.


func _on_Tree5Step1_pressed():
	skill_button_pressed("tree5", "step1")
	pass # Replace with function body.


func _on_Tree5Step2_pressed():
	skill_button_pressed("tree5", "step2")
	pass # Replace with function body.


func _on_Tree5Step3_pressed():
	skill_button_pressed("tree5", "step3")
	pass # Replace with function body.


func _on_Tree5Step4_pressed():
	skill_button_pressed("tree5", "step4")
	pass # Replace with function body.


func _on_Tree2Step2_mouse_entered():
	skill_button_hover($SkillsContainer/SkillTree2/Step2, "tree2", "step2")
	pass # Replace with function body.


func _on_Tree2Step2_mouse_exited():
	$Tooltip.visible = false
	pass # Replace with function body.


func _on_Tree2Step3_mouse_entered():
	skill_button_hover($SkillsContainer/SkillTree2/Step3, "tree2", "step3")
	pass # Replace with function body.


func _on_Tree2Step3_mouse_exited():
	$Tooltip.visible = false
	pass # Replace with function body.


func _on_Tree2Step4_mouse_entered():
	skill_button_hover($SkillsContainer/SkillTree2/Step4, "tree2", "step4")
	pass # Replace with function body.


func _on_Tree2Step4_mouse_exited():
	$Tooltip.visible = false
	pass # Replace with function body.


func _on_Tree3Step2_mouse_entered():
	skill_button_hover($SkillsContainer/SkillTree3/Step2, "tree3", "step2")
	pass # Replace with function body.


func _on_Tree3Step2_mouse_exited():
	$Tooltip.visible = false
	pass # Replace with function body.


func _on_Tree3Step3_mouse_entered():
	skill_button_hover($SkillsContainer/SkillTree3/Step3, "tree3", "step3")
	pass # Replace with function body.


func _on_Tree3Step3_mouse_exited():
	$Tooltip.visible = false
	pass # Replace with function body.


func _on_Tree3Step4_mouse_entered():
	skill_button_hover($SkillsContainer/SkillTree3/Step4, "tree3", "step4")
	pass # Replace with function body.


func _on_Tree3Step4_mouse_exited():
	$Tooltip.visible = false
	pass # Replace with function body.


func _on_Tree4Step2_mouse_entered():
	skill_button_hover($SkillsContainer/SkillTree4/Step2, "tree4", "step2")
	pass # Replace with function body.


func _on_Tree4Step2_mouse_exited():
	$Tooltip.visible = false
	pass # Replace with function body.


func _on_Tree4Step3_mouse_entered():
	skill_button_hover($SkillsContainer/SkillTree4/Step3, "tree4", "step3")
	pass # Replace with function body.


func _on_Tree4Step3_mouse_exited():
	$Tooltip.visible = false
	pass # Replace with function body.


func _on_Tree4Step4_mouse_entered():
	skill_button_hover($SkillsContainer/SkillTree4/Step4, "tree4", "step4")
	pass # Replace with function body.


func _on_Tree4Step4_mouse_exited():
	$Tooltip.visible = false
	pass # Replace with function body.


func _on_Tree5Step1_mouse_entered():
	skill_button_hover($SkillsContainer/SkillTree5/Step1, "tree5", "step1")
	pass # Replace with function body.


func _on_Tree5Step1_mouse_exited():
	$Tooltip.visible = false
	pass # Replace with function body.


func _on_Tree5Step2_mouse_entered():
	skill_button_hover($SkillsContainer/SkillTree5/Step2, "tree5", "step2")
	pass # Replace with function body.


func _on_Tree5Step2_mouse_exited():
	$Tooltip.visible = false
	pass # Replace with function body.


func _on_Tree5Step3_mouse_entered():
	skill_button_hover($SkillsContainer/SkillTree5/Step3, "tree5", "step3")
	pass # Replace with function body.


func _on_Tree5Step3_mouse_exited():
	$Tooltip.visible = false
	pass # Replace with function body.


func _on_Tree5Step4_mouse_entered():
	skill_button_hover($SkillsContainer/SkillTree5/Step4, "tree5", "step4")
	pass # Replace with function body.


func _on_Tree5Step4_mouse_exited():
	$Tooltip.visible = false
	pass # Replace with function body.
