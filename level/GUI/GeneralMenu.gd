extends Control


# Called when the node enters the scene tree for the first time.
func _ready():
	#call_deferred("test_send_leaderboard")
	#call_deferred("test_leaderboard")
	#TranslationServer.set_locale("fr")
	if global.first_start_software:
		$AnimationPlayer.play("start")
		global.first_start_software = false
	else:
		$AnimationPlayer.play("hot_start")
	pass # Replace with function body.

func test_send_leaderboard():
	randomize()
	SilentWolf.Scores.persist_score("player"+str(rand_range(0,100)), rand_range(0, 100))
	pass

func test_leaderboard():
	yield(SilentWolf.Scores.get_high_scores(0, "Level1Scorenormal"), "sw_scores_received")
	#print("Scores: " + str(SilentWolf.Scores.scores))
	print("\n\nLeaderboard: " + str(SilentWolf.Scores.leaderboards["Level1Scorenormal"]))
	pass

func _on_StartButton_pressed():
	audio_manager.play_sfx(load("res://assets/sfx/gui/BipOK.wav"), 0, 1)
	global.reset_data()
	audio_manager.reset_music()
	$AnimationPlayer.play("quit")
	yield($AnimationPlayer, "animation_finished")
	var _result = get_tree().change_scene("res://level/GUI/LoadGame.tscn")
	#get_tree().change_scene("res://level/Battlefield/BattleField.tscn")
	pass # Replace with function body.


func _on_OptionButton_pressed():
	audio_manager.play_sfx(load("res://assets/sfx/gui/BipOK.wav"), 0, 1)
	$AudioMenu.visible = true
	pass # Replace with function body.


func _on_CreditButton_pressed():
	audio_manager.play_sfx(load("res://assets/sfx/gui/BipOK.wav"), 0, 1)
	$Credit.visible = true
	pass # Replace with function body.


func _on_ExitButton_pressed():
	audio_manager.play_sfx(load("res://assets/sfx/gui/bipBack.wav"), 0, 1)
	$AnimationPlayer.play("quit")
	yield($AnimationPlayer, "animation_finished")
	get_tree().quit()
	pass # Replace with function body.


func _on_LeaderBoard_pressed():
	audio_manager.play_sfx(load("res://assets/sfx/gui/BipOK.wav"), 0, 1)
	$AnimationPlayer.play("quit")
	yield($AnimationPlayer, "animation_finished")
	#get_tree().change_scene("res://addons/silent_wolf/Scores/Leaderboard.tscn")
	var _result = get_tree().change_scene("res://level/GUI/Leaderboard.tscn")
	pass # Replace with function body.


func _on_Tutorial_pressed():
	audio_manager.play_sfx(load("res://assets/sfx/gui/BipOK.wav"), 0, 1)
	global.current_level = 0
	global.reset_data()
	global.reset_tutorial_data()
	audio_manager.reset_music()
	$AnimationPlayer.play("quit")
	yield($AnimationPlayer, "animation_finished")
	var _result = get_tree().change_scene("res://level/Battlefield/Tutorial/TutorialBattlefield.tscn")
	pass # Replace with function body.
