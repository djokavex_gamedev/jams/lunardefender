extends CanvasLayer


func set_pause():
	test_success()
	if not get_tree().paused:
		var leaderboard_name = "Level"+str(global.current_level)+"Score"+global.current_difficulty #TODO: A changer
		$Rect/ScoreLabel.text = tr("KEY_SCORE") + ": " + str(global.player_points)
		yield(SilentWolf.Scores.get_score_position(global.player_points, leaderboard_name), "sw_position_received")
		var position = SilentWolf.Scores.position
		$Rect/PositionLabel.text = tr("KEY_POSITION_IN_LEADERBOARD") + ": " + str(position)
		
	$Rect/Stats/CollectedGold/Value.text = str(global.player_stats["gold"])
	$Rect/Stats/CollectedSuperGold/Value.text = str(global.player_stats["super_gold"])
	$Rect/Stats/EnemyKill/Value.text = str(global.player_stats["enemy_kill"])
	$Rect/Stats/BossKill/Value.text = str(global.player_stats["boss_kill"])
	$Rect/Stats/DroneDestroyed/Value.text = str(global.player_stats["swarm_kill"])
	$Rect/Stats/TowerDestroyed/Value.text = str(global.player_stats["tower_kill"])
	
	var new_pause_state = not get_tree().paused
	get_tree().paused = new_pause_state
	$Rect.visible = new_pause_state


func _on_ExitButton_pressed():
	#test_success()
	set_pause()
	audio_manager.play_sfx(load("res://assets/sfx/gui/question_003.ogg"), 0, 1)
	var _result = get_tree().change_scene("res://level/GUI/SelectLevelWindow.tscn")
	pass # Replace with function body.


func _on_SubmitButton_pressed():
	#test_success()
	var leaderboard_name = "Level"+str(global.current_level)+"Score"+global.current_difficulty #TODO : A changer
	global.player_stats["wave_done"] = global.current_phase - 1
	SilentWolf.Scores.persist_score(global.player_name, global.player_points, leaderboard_name, global.player_stats)
	set_pause()
	audio_manager.play_sfx(load("res://assets/sfx/gui/question_003.ogg"), 0, 1)
	var _result = get_tree().change_scene("res://level/GUI/SelectLevelWindow.tscn")
	pass # Replace with function body.

func test_success():
	var level = "level" + str(global.current_level)
	if not global.level_done[level][global.current_difficulty]["half_done"]:
		global.level_done[level][global.current_difficulty]["half_done"] = true
		global.player_experience = global.player_experience + 1
	if not global.level_done[level][global.current_difficulty]["done"]:
		global.level_done[level][global.current_difficulty]["done"] = true
		global.player_experience = global.player_experience + 2
		$Rect/EndSkillLabel.visible = true
	
	var level2 = "level"+str(global.current_level)+"_"+global.current_difficulty+"_score_to_do"
	if not global.level_done[level][global.current_difficulty]["score_done"]:
		if global.player_points > global.level_done[level2]:
			global.level_done[level][global.current_difficulty]["score_done"] = true
			global.player_experience = global.player_experience + 2
			$Rect/PointsSkillLabel.visible = true
	
	if global.current_difficulty == "normal" and global.current_level == 6:
		# Hard mode unlocked
		$Rect/Label2.visible = true
	if global.current_difficulty != "normal" and global.current_level == 6:
		# Game finished
		$Rect/Label3.visible = true
	
	global.save_game_data(global.current_save)
	pass
