extends Control

func _ready():
	$AnimationPlayer.play("start")
	$LineEdit.grab_focus()
	pass

func _on_Button_pressed():
	if $LineEdit.text != "":
		audio_manager.play_sfx(load("res://assets/sfx/gui/BipOK.wav"), 0, 1)
		global.player_name = $LineEdit.text
		
		global.save_game_data(global.current_save)
#		var data = PersistenceNode.get_data("last_name")
#		data["name"] = global.player_name
#		PersistenceNode.save_data("last_name")
		
		$AnimationPlayer.play("quit")
		yield($AnimationPlayer, "animation_finished")
		#get_tree().change_scene("res://level/GUI/GeneralMenu.tscn")
		get_tree().change_scene("res://level/GUI/SelectLevelWindow.tscn")
	else:
		# Bad song
		pass
	pass # Replace with function body.


func _on_LineEdit_text_entered(new_text):
	if $LineEdit.text != "":
		audio_manager.play_sfx(load("res://assets/sfx/gui/BipOK.wav"), 0, 1)
		global.player_name = $LineEdit.text
		
		global.save_game_data(global.current_save)
#		var data = PersistenceNode.get_data("last_name")
#		data["name"] = global.player_name
#		PersistenceNode.save_data("last_name")
		
		$AnimationPlayer.play("quit")
		yield($AnimationPlayer, "animation_finished")
		#get_tree().change_scene("res://level/GUI/GeneralMenu.tscn")
		get_tree().change_scene("res://level/GUI/SelectLevelWindow.tscn")
	else:
		# Bad song
		pass
	pass # Replace with function body.


func _on_CancelButton_pressed():
	audio_manager.play_sfx(load("res://assets/sfx/gui/bipBack.wav"), 0, 1)
	$AnimationPlayer.play("quit")
	yield($AnimationPlayer, "animation_finished")
	get_tree().change_scene("res://level/GUI/LoadGame.tscn")
	pass # Replace with function body.
