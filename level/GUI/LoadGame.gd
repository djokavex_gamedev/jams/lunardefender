extends Control

# Called when the node enters the scene tree for the first time.
func _ready():
	
	#print("Loading saved games")
	$AnimationPlayer.play("start")
	
	var data = PersistenceNode.get_data("saved_game")
	#print(data)
	
	$VBoxContainer/Save1/Button2/Label.text = tr("KEY_REMOVE_SAVED")+" 1"
	$VBoxContainer/Save2/Button2/Label.text = tr("KEY_REMOVE_SAVED")+" 2"
	$VBoxContainer/Save3/Button2/Label.text = tr("KEY_REMOVE_SAVED")+" 3"
	
	if not data.has("Save1"):
		$VBoxContainer/Save1/Button/Label.text = tr("KEY_NEW_GAME")+" 1"
		$VBoxContainer/Save1/Button2.disabled = true
	else:
		$VBoxContainer/Save1/Button/Label.text = data["Save1"]["player_name"]
	
	if not data.has("Save2"):
		$VBoxContainer/Save2/Button/Label.text = tr("KEY_NEW_GAME")+" 2"
		$VBoxContainer/Save2/Button2.disabled = true
	else:
		$VBoxContainer/Save2/Button/Label.text = data["Save2"]["player_name"]
	
	if not data.has("Save3"):
		$VBoxContainer/Save3/Button/Label.text = tr("KEY_NEW_GAME")+" 3"
		$VBoxContainer/Save3/Button2.disabled = true
	else:
		$VBoxContainer/Save3/Button/Label.text = data["Save3"]["player_name"]
	
	global.general_reset()
	pass # Replace with function body.


func _on_Save1_pressed():
	audio_manager.play_sfx(load("res://assets/sfx/gui/BipOK.wav"), 0, 1)
	#print("Using Save 1")
	global.current_save = "Save1"
	
	var data = PersistenceNode.get_data("saved_game")
	#print(data)
	global.general_reset()
	
	if not data.has("Save1"): # Create new save
		$AnimationPlayer.play("quit")
		yield($AnimationPlayer, "animation_finished")
		get_tree().change_scene("res://level/GUI/NameWindow.tscn")
	else: # Use already saved data
		global.load_game_data("Save1")
		$AnimationPlayer.play("quit")
		yield($AnimationPlayer, "animation_finished")
		get_tree().change_scene("res://level/GUI/SelectLevelWindow.tscn")
		pass
	pass # Replace with function body.


func _on_RemoveSave1_pressed():
	audio_manager.play_sfx(load("res://assets/sfx/gui/bipBack.wav"), 0, 1)
	var data = PersistenceNode.get_data("saved_game")
	#print(data)
	data.erase("Save1")
	PersistenceNode.save_data("saved_game")
	
	$VBoxContainer/Save1/Button/Label.text = tr("KEY_NEW_GAME")+" 1"
	$VBoxContainer/Save1/Button2.disabled = true
	pass # Replace with function body.


func _on_Save2_pressed():
	audio_manager.play_sfx(load("res://assets/sfx/gui/BipOK.wav"), 0, 1)
	#print("Using Save 2")
	global.current_save = "Save2"
	
	var data = PersistenceNode.get_data("saved_game")
	#print(data)
	global.general_reset()
	
	if not data.has("Save2"): # Create new save
		$AnimationPlayer.play("quit")
		yield($AnimationPlayer, "animation_finished")
		get_tree().change_scene("res://level/GUI/NameWindow.tscn")
	else: # Use already saved data
		global.load_game_data("Save2")
		$AnimationPlayer.play("quit")
		yield($AnimationPlayer, "animation_finished")
		get_tree().change_scene("res://level/GUI/SelectLevelWindow.tscn")
		pass
	pass # Replace with function body.


func _on_RemoveSave2_pressed():
	audio_manager.play_sfx(load("res://assets/sfx/gui/bipBack.wav"), 0, 1)
	var data = PersistenceNode.get_data("saved_game")
	#print(data)
	data.erase("Save2")
	PersistenceNode.save_data("saved_game")
	
	$VBoxContainer/Save2/Button/Label.text = tr("KEY_NEW_GAME")+" 2"
	$VBoxContainer/Save2/Button2.disabled = true
	pass # Replace with function body.


func _on_Save3_pressed():
	audio_manager.play_sfx(load("res://assets/sfx/gui/BipOK.wav"), 0, 1)
	#print("Using Save 3")
	global.current_save = "Save3"
	
	var data = PersistenceNode.get_data("saved_game")
	#print(data)
	global.general_reset()
	
	if not data.has("Save3"): # Create new save
		$AnimationPlayer.play("quit")
		yield($AnimationPlayer, "animation_finished")
		get_tree().change_scene("res://level/GUI/NameWindow.tscn")
	else: # Use already saved data
		global.load_game_data("Save3")
		$AnimationPlayer.play("quit")
		yield($AnimationPlayer, "animation_finished")
		get_tree().change_scene("res://level/GUI/SelectLevelWindow.tscn")
		pass
	pass # Replace with function body.


func _on_RemoveSave3_pressed():
	audio_manager.play_sfx(load("res://assets/sfx/gui/bipBack.wav"), 0, 1)
	var data = PersistenceNode.get_data("saved_game")
	#print(data)
	data.erase("Save3")
	PersistenceNode.save_data("saved_game")
	
	$VBoxContainer/Save3/Button/Label.text = tr("KEY_NEW_GAME")+" 3"
	$VBoxContainer/Save3/Button2.disabled = true
	pass # Replace with function body.


func _on_ExitButton_pressed():
	audio_manager.play_sfx(load("res://assets/sfx/gui/bipBack.wav"), 0, 1)
	$AnimationPlayer.play("quit")
	yield($AnimationPlayer, "animation_finished")
	get_tree().change_scene("res://level/GUI/GeneralMenu.tscn")
	pass # Replace with function body.
