extends Control



# Called when the node enters the scene tree for the first time.
func _ready() -> void:
#	AudioManager.restore_volumes()
#	GraphicsManager.restore_graphics()
	
	global.change_fullscreen(global.load_fullscreen())
	load_volume()
	
	pass # Replace with function body.

func StartGame():
	var _result = get_tree().change_scene("res://level/GUI/GeneralMenu.tscn")

# Dirty stuff
func load_volume():
	#print("Loading volume")
	
	var data = PersistenceNode.get_data("audio")
	#print(data)
	
	if data.has("Music"):
		var music_bus = AudioServer.get_bus_index("Music")
		var value = data["Music"]["Volume"]
		if value < 1:
			AudioServer.set_bus_mute(music_bus, true)
			pass
		else:
			AudioServer.set_bus_mute(music_bus, false)
			AudioServer.set_bus_volume_db(music_bus, value/2.0 - 50.0)
	if data.has("Effect"):
		var sounds_bus = AudioServer.get_bus_index("SFX")
		var value = data["Effect"]["Volume"]
		if value < 1:
			AudioServer.set_bus_mute(sounds_bus, true)
			pass
		else:
			AudioServer.set_bus_mute(sounds_bus, false)
			AudioServer.set_bus_volume_db(sounds_bus, value/2.0 - 50.0)
	
	pass
