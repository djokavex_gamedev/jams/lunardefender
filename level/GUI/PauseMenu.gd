extends CanvasLayer

func set_pause():
	var new_pause_state = not get_tree().paused
	get_tree().paused = new_pause_state
	$ColorRect.visible = new_pause_state

func _on_ResumeButton_pressed():
	set_pause()
	audio_manager.play_sfx(load("res://assets/sfx/gui/BipOK.wav"), 0, 1)
	pass # Replace with function body.


func _on_OptionButton_pressed():
	audio_manager.play_sfx(load("res://assets/sfx/gui/BipOK.wav"), 0, 1)
	$AudioMenu.visible = true
	pass # Replace with function body.


func _on_ExitButton_pressed():
	set_pause()
	audio_manager.play_sfx(load("res://assets/sfx/gui/bipBack.wav"), 0, 1)
	if global.current_level == 0: # Tutorial
		get_tree().change_scene("res://level/GUI/GeneralMenu.tscn")
	else:
		get_tree().change_scene("res://level/GUI/SelectLevelWindow.tscn")
	pass # Replace with function body.


func _on_RetryButton_pressed():
	#global.current_level = 2
	if global.current_level == 0:
		global.reset_data()
		global.reset_tutorial_data()
		audio_manager.reset_music()
		set_pause()
		audio_manager.play_sfx(load("res://assets/sfx/gui/BipOK.wav"), 0, 1)
		get_tree().change_scene("res://level/Battlefield/Tutorial/TutorialBattlefield.tscn")
	else:
		if global.current_difficulty == "normal":
			match global.current_level:
				1:
					global.phase = global.phase_level1_normal
				2:
					global.phase = global.phase_level2_normal
				3:
					global.phase = global.phase_level3_normal
				4:
					global.phase = global.phase_level4_normal
				5:
					global.phase = global.phase_level5_normal
				6:
					global.phase = global.phase_level6_normal
				_:
					global.phase = global.phase_level1_normal
		else:
			match global.current_level:
				1:
					global.phase = global.phase_level1_hard
				2:
					global.phase = global.phase_level2_hard
				3:
					global.phase = global.phase_level3_hard
				4:
					global.phase = global.phase_level4_hard
				5:
					global.phase = global.phase_level5_hard
				6:
					global.phase = global.phase_level6_hard
				_:
					global.phase = global.phase_level1_hard
		
		global.reset_data()
		
		set_pause()
		audio_manager.play_sfx(load("res://assets/sfx/gui/BipOK.wav"), 0, 1)
		get_tree().change_scene("res://level/Battlefield/Level"+str(global.current_level)+"/BattleField.tscn")
	pass # Replace with function body.
