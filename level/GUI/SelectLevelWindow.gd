extends Control

var LaserClass = preload("res://actor/Swarm/Laser.tscn")

var patrol_points
var patrol_path
var patrol_index = 0
var move_speed = 200
var current_velocity = Vector2(0,0)

func _ready():
	patrol_path = get_node("Level1Path")
	if global.level_done["level1"]["normal"]["half_done"]:
		$LevelButton1/Normal/HalfLevelStar.texture = load("res://assets/img/GUI/Star_03.png")
		$LevelButton2.disabled = false
		patrol_path = get_node("Level2Path")
	else:
		get_node("LevelButton2/Normal/FullLevelStar").modulate = Color(1,1,1,0.5)
		get_node("LevelButton2/Normal/HalfLevelStar").modulate = Color(1,1,1,0.5)
	if global.level_done["level1"]["normal"]["done"]:
		$LevelButton1/Normal/FullLevelStar.texture = load("res://assets/img/GUI/Star_03.png")
	if global.level_done["level1"]["normal"]["score_done"]:
		$LevelButton1/Normal/PointsStar.texture = load("res://assets/img/GUI/Star_03.png")
	if global.level_done["level1"]["hard"]["half_done"]:
		$LevelButton1Hard/Hard/HalfLevelStar.texture = load("res://assets/img/GUI/Star_03.png")
		$LevelButton2Hard.disabled = false
	else:
		get_node("LevelButton2Hard/Hard/FullLevelStar").modulate = Color(1,1,1,0.5)
		get_node("LevelButton2Hard/Hard/HalfLevelStar").modulate = Color(1,1,1,0.5)
	if global.level_done["level1"]["hard"]["done"]:
		$LevelButton1Hard/Hard/FullLevelStar.texture = load("res://assets/img/GUI/Star_03.png")
	if global.level_done["level1"]["hard"]["score_done"]:
		$LevelButton1Hard/Hard/PointsStar.texture = load("res://assets/img/GUI/Star_03.png")
		
	if global.level_done["level2"]["normal"]["half_done"]:
		$LevelButton2/Normal/HalfLevelStar.texture = load("res://assets/img/GUI/Star_03.png")
		$LevelButton3.disabled = false
		patrol_path = get_node("Level3Path")
	else:
		get_node("LevelButton3/Normal/FullLevelStar").modulate = Color(1,1,1,0.5)
		get_node("LevelButton3/Normal/HalfLevelStar").modulate = Color(1,1,1,0.5)
	if global.level_done["level2"]["normal"]["done"]:
		$LevelButton2/Normal/FullLevelStar.texture = load("res://assets/img/GUI/Star_03.png")
	if global.level_done["level2"]["normal"]["score_done"]:
		$LevelButton2/Normal/PointsStar.texture = load("res://assets/img/GUI/Star_03.png")
	if global.level_done["level2"]["hard"]["half_done"]:
		$LevelButton2Hard/Hard/HalfLevelStar.texture = load("res://assets/img/GUI/Star_03.png")
		$LevelButton3Hard.disabled = false
	else:
		get_node("LevelButton3Hard/Hard/FullLevelStar").modulate = Color(1,1,1,0.5)
		get_node("LevelButton3Hard/Hard/HalfLevelStar").modulate = Color(1,1,1,0.5)
	if global.level_done["level2"]["hard"]["done"]:
		$LevelButton2Hard/Hard/FullLevelStar.texture = load("res://assets/img/GUI/Star_03.png")
	if global.level_done["level2"]["hard"]["score_done"]:
		$LevelButton2Hard/Hard/PointsStar.texture = load("res://assets/img/GUI/Star_03.png")
		
	if global.level_done["level3"]["normal"]["half_done"]:
		$LevelButton3/Normal/HalfLevelStar.texture = load("res://assets/img/GUI/Star_03.png")
		$LevelButton4.disabled = false
		patrol_path = get_node("Level4Path")
	else:
		get_node("LevelButton4/Normal/FullLevelStar").modulate = Color(1,1,1,0.5)
		get_node("LevelButton4/Normal/HalfLevelStar").modulate = Color(1,1,1,0.5)
	if global.level_done["level3"]["normal"]["done"]:
		$LevelButton3/Normal/FullLevelStar.texture = load("res://assets/img/GUI/Star_03.png")
	if global.level_done["level3"]["normal"]["score_done"]:
		$LevelButton3/Normal/PointsStar.texture = load("res://assets/img/GUI/Star_03.png")
	if global.level_done["level3"]["hard"]["half_done"]:
		$LevelButton3Hard/Hard/HalfLevelStar.texture = load("res://assets/img/GUI/Star_03.png")
		$LevelButton4Hard.disabled = false
	else:
		get_node("LevelButton4Hard/Hard/FullLevelStar").modulate = Color(1,1,1,0.5)
		get_node("LevelButton4Hard/Hard/HalfLevelStar").modulate = Color(1,1,1,0.5)
	if global.level_done["level3"]["hard"]["done"]:
		$LevelButton3Hard/Hard/FullLevelStar.texture = load("res://assets/img/GUI/Star_03.png")
	if global.level_done["level3"]["hard"]["score_done"]:
		$LevelButton3Hard/Hard/PointsStar.texture = load("res://assets/img/GUI/Star_03.png")
		
	if global.level_done["level4"]["normal"]["half_done"]:
		$LevelButton4/Normal/HalfLevelStar.texture = load("res://assets/img/GUI/Star_03.png")
		$LevelButton5.disabled = false
		patrol_path = get_node("Level5Path")
	else:
		get_node("LevelButton5/Normal/FullLevelStar").modulate = Color(1,1,1,0.5)
		get_node("LevelButton5/Normal/HalfLevelStar").modulate = Color(1,1,1,0.5)
	if global.level_done["level4"]["normal"]["done"]:
		$LevelButton4/Normal/FullLevelStar.texture = load("res://assets/img/GUI/Star_03.png")
	if global.level_done["level4"]["normal"]["score_done"]:
		$LevelButton4/Normal/PointsStar.texture = load("res://assets/img/GUI/Star_03.png")
	if global.level_done["level4"]["hard"]["half_done"]:
		$LevelButton4Hard/Hard/HalfLevelStar.texture = load("res://assets/img/GUI/Star_03.png")
		$LevelButton5Hard.disabled = false
	else:
		get_node("LevelButton5Hard/Hard/FullLevelStar").modulate = Color(1,1,1,0.5)
		get_node("LevelButton5Hard/Hard/HalfLevelStar").modulate = Color(1,1,1,0.5)
	if global.level_done["level4"]["hard"]["done"]:
		$LevelButton4Hard/Hard/FullLevelStar.texture = load("res://assets/img/GUI/Star_03.png")
	if global.level_done["level4"]["hard"]["score_done"]:
		$LevelButton4Hard/Hard/PointsStar.texture = load("res://assets/img/GUI/Star_03.png")
		
	if global.level_done["level5"]["normal"]["half_done"]:
		$LevelButton5/Normal/HalfLevelStar.texture = load("res://assets/img/GUI/Star_03.png")
		$LevelButton6.disabled = false
		patrol_path = get_node("Level6Path")
	else:
		get_node("LevelButton6/Normal/FullLevelStar").modulate = Color(1,1,1,0.5)
		get_node("LevelButton6/Normal/HalfLevelStar").modulate = Color(1,1,1,0.5)
	if global.level_done["level5"]["normal"]["done"]:
		$LevelButton5/Normal/FullLevelStar.texture = load("res://assets/img/GUI/Star_03.png")
	if global.level_done["level5"]["normal"]["score_done"]:
		$LevelButton5/Normal/PointsStar.texture = load("res://assets/img/GUI/Star_03.png")
	if global.level_done["level5"]["hard"]["half_done"]:
		$LevelButton5Hard/Hard/HalfLevelStar.texture = load("res://assets/img/GUI/Star_03.png")
		$LevelButton6Hard.disabled = false
	else:
		get_node("LevelButton6Hard/Hard/FullLevelStar").modulate = Color(1,1,1,0.5)
		get_node("LevelButton6Hard/Hard/HalfLevelStar").modulate = Color(1,1,1,0.5)
	if global.level_done["level5"]["hard"]["done"]:
		$LevelButton5Hard/Hard/FullLevelStar.texture = load("res://assets/img/GUI/Star_03.png")
	if global.level_done["level5"]["hard"]["score_done"]:
		$LevelButton5Hard/Hard/PointsStar.texture = load("res://assets/img/GUI/Star_03.png")
		
	if global.level_done["level6"]["normal"]["half_done"]:
		$LevelButton6/Normal/HalfLevelStar.texture = load("res://assets/img/GUI/Star_03.png")
		$LevelButton1Hard.disabled = false
	else:
		get_node("LevelButton1Hard/Hard/FullLevelStar").modulate = Color(1,1,1,0.5)
		get_node("LevelButton1Hard/Hard/HalfLevelStar").modulate = Color(1,1,1,0.5)
	if global.level_done["level6"]["normal"]["done"]:
		$LevelButton6/Normal/FullLevelStar.texture = load("res://assets/img/GUI/Star_03.png")
	if global.level_done["level6"]["normal"]["score_done"]:
		$LevelButton6/Normal/PointsStar.texture = load("res://assets/img/GUI/Star_03.png")
	if global.level_done["level6"]["hard"]["half_done"]:
		$LevelButton6Hard/Hard/HalfLevelStar.texture = load("res://assets/img/GUI/Star_03.png")
	if global.level_done["level6"]["hard"]["done"]:
		$LevelButton6Hard/Hard/FullLevelStar.texture = load("res://assets/img/GUI/Star_03.png")
	if global.level_done["level6"]["hard"]["score_done"]:
		$LevelButton6Hard/Hard/PointsStar.texture = load("res://assets/img/GUI/Star_03.png")
	
	var available_experience = global.player_experience-global.experience_used
	get_node("SkillTreeButton/Label").text = tr("KEY_SKILL_TREE") + " (" + str(available_experience) + ")"
	
	
	patrol_points = patrol_path.curve.get_baked_points()
	patrol_index = 0
	get_node("ShipSprite").position = patrol_points[patrol_index]
	
	pass

func _physics_process(delta: float) -> void:
	var target = patrol_points[patrol_index]
	if get_node("ShipSprite").position.distance_to(target) < 3:
		patrol_index = wrapi(patrol_index + 1, 0, patrol_points.size())
		target = patrol_points[patrol_index]
		if patrol_index == (patrol_points.size() - 1):
			patrol_index = 0
	var velocity = (target - get_node("ShipSprite").position).normalized() * move_speed
	get_node("ShipSprite").global_rotation = velocity.angle() + 1.56
	current_velocity = lerp(current_velocity, velocity * delta, 0.9) # 
	get_node("ShipSprite").global_position = get_node("ShipSprite").global_position + current_velocity
	pass

func _on_LevelButton1_pressed():
	global.current_level = 1
	global.current_difficulty = "normal"
	global.phase = global.phase_level1_normal
	global.reset_data()
	audio_manager.play_sfx(load("res://assets/sfx/gui/BipOK.wav"), 0, 1)
	get_tree().change_scene("res://level/Battlefield/Level1/PreStartLevel1Normal.tscn")
	pass # Replace with function body.

func _on_LevelButton1Hard_pressed() -> void:
	global.current_level = 1
	global.current_difficulty = "hard"
	global.phase = global.phase_level1_hard
	global.reset_data()
	audio_manager.play_sfx(load("res://assets/sfx/gui/BipOK.wav"), 0, 1)
	get_tree().change_scene("res://level/Battlefield/Level1/PreStartLevel1Hard.tscn")
	pass # Replace with function body.

func _on_LevelButton2_pressed():
	global.current_level = 2
	global.current_difficulty = "normal"
	global.phase = global.phase_level2_normal
	global.reset_data()
	audio_manager.play_sfx(load("res://assets/sfx/gui/BipOK.wav"), 0, 1)
	get_tree().change_scene("res://level/Battlefield/Level2/PreStartLevel2Normal.tscn")
	pass # Replace with function body.

func _on_LevelButton2Hard_pressed() -> void:
	global.current_level = 2
	global.current_difficulty = "hard"
	global.phase = global.phase_level2_hard
	global.reset_data()
	audio_manager.play_sfx(load("res://assets/sfx/gui/BipOK.wav"), 0, 1)
	get_tree().change_scene("res://level/Battlefield/Level2/PreStartLevel2Hard.tscn")
	pass # Replace with function body.

func _on_LevelButton3_pressed():
	global.current_level = 3
	global.current_difficulty = "normal"
	global.phase = global.phase_level3_normal
	global.reset_data()
	audio_manager.play_sfx(load("res://assets/sfx/gui/BipOK.wav"), 0, 1)
	get_tree().change_scene("res://level/Battlefield/Level3/PreStartLevel3Normal.tscn")
	pass # Replace with function body.

func _on_LevelButton3Hard_pressed() -> void:
	global.current_level = 3
	global.current_difficulty = "hard"
	global.phase = global.phase_level3_hard
	global.reset_data()
	audio_manager.play_sfx(load("res://assets/sfx/gui/BipOK.wav"), 0, 1)
	get_tree().change_scene("res://level/Battlefield/Level3/PreStartLevel3Hard.tscn")
	pass # Replace with function body.

func _on_LevelButton4_pressed():
	global.current_level = 4
	global.current_difficulty = "normal"
	global.phase = global.phase_level4_normal
	global.reset_data()
	audio_manager.play_sfx(load("res://assets/sfx/gui/BipOK.wav"), 0, 1)
	get_tree().change_scene("res://level/Battlefield/Level4/PreStartLevel4Normal.tscn")
	pass # Replace with function body.

func _on_LevelButton4Hard_pressed() -> void:
	global.current_level = 4
	global.current_difficulty = "hard"
	global.phase = global.phase_level4_hard
	global.reset_data()
	audio_manager.play_sfx(load("res://assets/sfx/gui/BipOK.wav"), 0, 1)
	get_tree().change_scene("res://level/Battlefield/Level4/PreStartLevel4Hard.tscn")
	pass # Replace with function body.

func _on_LevelButton5_pressed():
	global.current_level = 5
	global.current_difficulty = "normal"
	global.phase = global.phase_level5_normal
	global.reset_data()
	audio_manager.play_sfx(load("res://assets/sfx/gui/BipOK.wav"), 0, 1)
	get_tree().change_scene("res://level/Battlefield/Level5/PreStartLevel5Normal.tscn")
	pass # Replace with function body.

func _on_LevelButton5Hard_pressed() -> void:
	global.current_level = 5
	global.current_difficulty = "hard"
	global.phase = global.phase_level5_hard
	global.reset_data()
	audio_manager.play_sfx(load("res://assets/sfx/gui/BipOK.wav"), 0, 1)
	get_tree().change_scene("res://level/Battlefield/Level5/PreStartLevel5Hard.tscn")
	pass # Replace with function body.

func _on_LevelButton6_pressed():
#	global.current_level = 6
#	global.current_difficulty = "normal"
#	audio_manager.play_sfx(load("res://assets/sfx/gui/BipOK.wav"), 0, 1)
#
#	# TODO : CHANGE
#	global.phase = global.phase_test
#	global.reset_data()
#	get_tree().change_scene("res://level/Battlefield/BattleField.tscn")
	
	global.current_level = 6
	global.current_difficulty = "normal"
	global.phase = global.phase_level6_normal
	global.reset_data()
	audio_manager.play_sfx(load("res://assets/sfx/gui/BipOK.wav"), 0, 1)
	get_tree().change_scene("res://level/Battlefield/Level6/PreStartLevel6Normal.tscn")
	pass # Replace with function body.

func _on_LevelButton6Hard_pressed() -> void:
	global.current_level = 6
	global.current_difficulty = "hard"
	global.phase = global.phase_level6_hard
	global.reset_data()
	audio_manager.play_sfx(load("res://assets/sfx/gui/BipOK.wav"), 0, 1)
	get_tree().change_scene("res://level/Battlefield/Level6/PreStartLevel6Hard.tscn")
	pass # Replace with function body.
	

func _on_MainMenuButton_pressed():
	audio_manager.play_sfx(load("res://assets/sfx/gui/bipBack.wav"), 0, 1)
	global.save_game_data(global.current_save)
	get_tree().change_scene("res://level/GUI/GeneralMenu.tscn")
	pass # Replace with function body.


func _on_SkillTreeButton_pressed():
	audio_manager.play_sfx(load("res://assets/sfx/gui/BipOK.wav"), 0, 1)
	get_tree().change_scene("res://level/GUI/BuySkillsWindow.tscn")
	pass # Replace with function body.


func _on_CodexButton_pressed() -> void:
	audio_manager.play_sfx(load("res://assets/sfx/gui/BipOK.wav"), 0, 1)
	get_tree().change_scene("res://level/GUI/Codex.tscn")
	pass # Replace with function body.















func _on_ShotTimer_timeout() -> void:
	var laser_instance = LaserClass.instance()
	var number = randi() % 7
	for i in number:
		laser_instance = LaserClass.instance()
		laser_instance.global_position = get_node("ShipSprite").global_position
		laser_instance.global_rotation = get_node("ShipSprite").global_rotation - 1.57
		add_child(laser_instance)
		yield(get_tree().create_timer(0.1),"timeout")
	pass # Replace with function body.
