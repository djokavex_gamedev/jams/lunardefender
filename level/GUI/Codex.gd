extends Control

onready var item_name = $Data/Name
onready var item_description = $Data/Description
onready var item_stats = $Data/Stats
onready var item_movie = $Data/MoviePlayerFrame/VideoPlayer

var index_key = 0
var current_key_list = []
var type = "tower"

func _ready() -> void:
	current_key_list = global.towers.keys()
	index_key = 0
	type = "tower"
	get_node("Menu/Particles2D").position = get_node("Menu/TowerButton").rect_position + get_node("Menu/TowerButton").rect_size / 2.0
	
	load_and_print_data()
	
	$AnimationPlayer.play("start")
	pass

#	"small": {
#		"unlocked": true,
#		"price": 75,
#		"health": 250,
#		"swarm_number": 2,
#		"swarm_price": 20,
#		"swarm_health": 50,
#		"swarm_damage": 6,
#		"swarm_fire_rate": 3.0,
		
func load_and_print_data():
	match type:
		"enemy":
			item_name.text = global.enemies[current_key_list[index_key]]["name"]
			item_description.text = global.enemies[current_key_list[index_key]]["tooltip"]
			item_stats.text = "- "+tr("KEY_HEALTH")+": "+str(global.enemies[current_key_list[index_key]]["health"])+"\n- "+tr("KEY_DAMAGE")+": "+str(global.enemies[current_key_list[index_key]]["damage"])+"\n- "+tr("KEY_FIRE_RATE")+": "+str(global.enemies[current_key_list[index_key]]["fire_rate"])+"\n- "+tr("KEY_MOVE_SPEED")+": "+str(global.enemies[current_key_list[index_key]]["move_speed"]) 
			item_movie.stop()
			item_movie.stream = load(global.enemies[current_key_list[index_key]]["movie"])
			item_movie.play()
			pass
		"tower":
			item_name.text = global.towers[current_key_list[index_key]]["name"]
			item_description.text = global.towers[current_key_list[index_key]]["description"]
			item_stats.text = "- "+tr("KEY_TOWER_PRICE")+": "+str(global.towers[current_key_list[index_key]]["price"])+"\n- "+tr("KEY_TOWER_HEALTH")+": "+str(global.towers[current_key_list[index_key]]["health"])+"\n- "+tr("KEY_DRONE_PRICE")+": "+str(global.towers[current_key_list[index_key]]["swarm_price"])+"\n- "+tr("KEY_DRONE_NUMBER")+": "+str(global.towers[current_key_list[index_key]]["swarm_number"])+"\n- "+tr("KEY_DRONE_HEALTH")+": "+str(global.towers[current_key_list[index_key]]["swarm_health"])+"\n- "+tr("KEY_DRONE_DAMAGE")+": "+str(global.towers[current_key_list[index_key]]["swarm_damage"])+"\n- "+tr("KEY_DRONE_FIRE_RATE")+": "+str(global.towers[current_key_list[index_key]]["swarm_fire_rate"])
			item_movie.stop()
			item_movie.stream = load(global.towers[current_key_list[index_key]]["movie"])
			item_movie.play()
			pass
		"usable":
			item_name.text = global.bonus_description[current_key_list[index_key]]["name"]
			item_description.text = global.bonus_description[current_key_list[index_key]]["description"]
			item_stats.text = global.bonus_description[current_key_list[index_key]]["stats"]
			item_movie.stop()
			item_movie.stream = load(global.bonus_description[current_key_list[index_key]]["movie"])
			item_movie.play()
			pass
	pass

func _on_VideoPlayer_finished() -> void:
	item_movie.play()
	pass # Replace with function body.


func _on_EnemyButton_pressed() -> void:
	if type != "enemy":
		audio_manager.play_sfx(load("res://assets/sfx/gui/BipOK.wav"), 0, 1)
		current_key_list = global.enemies.keys()
		index_key = 0
		type = "enemy"
		load_and_print_data()
		get_node("Menu/Particles2D").position = get_node("Menu/EnemyButton").rect_position + get_node("Menu/EnemyButton").rect_size / 2.0
	pass # Replace with function body.


func _on_TowerButton_pressed() -> void:
	if type != "tower":
		audio_manager.play_sfx(load("res://assets/sfx/gui/BipOK.wav"), 0, 1)
		current_key_list = global.towers.keys()
		current_key_list.remove(current_key_list.size()-1)
		print(current_key_list)
		index_key = 0
		type = "tower"
		load_and_print_data()
		get_node("Menu/Particles2D").position = get_node("Menu/TowerButton").rect_position + get_node("Menu/TowerButton").rect_size / 2.0
	pass # Replace with function body.


func _on_BonusButton_pressed() -> void:
	if type != "usable":
		audio_manager.play_sfx(load("res://assets/sfx/gui/BipOK.wav"), 0, 1)
		current_key_list = global.bonus_description.keys()
		index_key = 0
		type = "usable"
		load_and_print_data()
		get_node("Menu/Particles2D").position = get_node("Menu/BonusButton").rect_position + get_node("Menu/BonusButton").rect_size / 2.0
	pass # Replace with function body.


func _on_ExitButton_pressed() -> void:
	audio_manager.play_sfx(load("res://assets/sfx/gui/bipBack.wav"), 0, 1)
	$AnimationPlayer.play("quit")
	yield($AnimationPlayer, "animation_finished")
	get_tree().change_scene("res://level/GUI/SelectLevelWindow.tscn")
	pass # Replace with function body.


func _on_NextButton_pressed() -> void:
	audio_manager.play_sfx(load("res://assets/sfx/gui/BipOK.wav"), 0, 1)
	index_key = index_key + 1
	if index_key >= current_key_list.size():
		index_key = 0
	
	load_and_print_data()
	pass # Replace with function body.


func _on_PreviousButton_pressed() -> void:
	audio_manager.play_sfx(load("res://assets/sfx/gui/bipBack.wav"), 0, 1)
	index_key = index_key - 1
	if index_key < 0:
		index_key = current_key_list.size() - 1
	
	load_and_print_data()
	pass # Replace with function body.
