extends Button

# Called when the node enters the scene tree for the first time.
func _ready():
	var _result = connect("mouse_entered", self, "mouse_entered")
	_result = connect("mouse_exited", self, "mouse_exited")
	pass # Replace with function body.

func mouse_entered():
	modulate = Color(1.0, 1.0, 1.2)
	pass

func mouse_exited():
	modulate = Color(1.0, 1.0, 1.0)
	pass
