extends Control

var list_index = 1
var difficulty_index = "normal"
var ld_name = "Level1Scorenormal"
var current_increment = 0

func _ready():
	$AnimationPlayer.play("start")
	yield(SilentWolf.Scores.get_high_scores(10, ld_name), "sw_scores_received")
	#print("SilentWolf.Scores.leaderboards: " + str(SilentWolf.Scores.leaderboards))
	#print("SilentWolf.Scores.ldboard_config: " + str(SilentWolf.Scores.ldboard_config))
	#var scores = SilentWolf.Scores.scores
	var scores = []
	if ld_name in SilentWolf.Scores.leaderboards:
		scores = SilentWolf.Scores.leaderboards[ld_name]
	var local_scores = SilentWolf.Scores.local_scores
	
	if len(scores) > 0: 
		render_board(scores, local_scores)
	else:
		# use a signal to notify when the high scores have been returned, and show a "loading" animation until it's the case...
#		add_loading_scores_message()
#		yield(SilentWolf.Scores.get_high_scores(), "sw_scores_received")
#		hide_message()
#		render_board(SilentWolf.Scores.scores, local_scores)
		print("")

func render_board(scores, local_scores):
	var all_scores = scores
#	if ld_name in SilentWolf.Scores.ldboard_config and is_default_leaderboard(SilentWolf.Scores.ldboard_config[ld_name]):
#		all_scores = merge_scores_with_local_scores(scores, local_scores)
#		if !scores and !local_scores:
#			add_no_scores_message()
#	else:
#		if !scores:
#			add_no_scores_message()
	var position = 1
	print(scores)
	for score in scores:
		#print(score.player_name)
		var line = $VBoxContainer/HBoxTitle.duplicate()
		line.get_node("NameLabel").text = score.player_name
		line.get_node("PositionLabel").text = str(position)
		line.get_node("ScoreLabel").text = str(score.score)
		line.get_node("WaveLabel").text = str(score.metadata.wave_done)
		line.get_node("GoldLabel").text = str(score.metadata.gold)
		line.get_node("SuperGoldLabel").text = str(score.metadata.super_gold)
		line.get_node("EnemyLabel").text = str(score.metadata.enemy_kill)
		line.get_node("BossLabel").text = str(score.metadata.boss_kill)
		line.get_node("DroneLabel").text = str(score.metadata.swarm_kill)
		line.get_node("TowerLabel").text = str(score.metadata.tower_kill)
		$VBoxContainer.add_child(line)
		position += 1
		#add_item(score.player_name, str(int(score.score)))

func _on_ExitButton_pressed():
	audio_manager.play_sfx(load("res://assets/sfx/gui/bipBack.wav"), 0, 1)
	var scene_name = SilentWolf.scores_config.open_scene_on_close
	$AnimationPlayer.play("quit")
	yield($AnimationPlayer, "animation_finished")
	get_tree().change_scene(scene_name)
	pass # Replace with function body.


func _on_LeftButton_pressed():
	current_increment = current_increment + 1
	var function_increment = current_increment
	audio_manager.play_sfx(load("res://assets/sfx/gui/BipOK.wav"), 0, 1)
	list_index = list_index - 1
	if list_index < 1:
		list_index = 6
		if difficulty_index == "normal":
			difficulty_index = "hard"
			$Level/Difficulty.text = tr("KEY_HARD")
		else:
			difficulty_index = "normal"
			$Level/Difficulty.text = tr("KEY_NORMAL")
	ld_name = "Level"+str(list_index)+"Score"+difficulty_index
	$Level.text = tr("KEY_LEVEL"+ str(list_index))
	for node in $VBoxContainer.get_children():
		if node.name != "HBoxTitle":
			node.queue_free()
	yield(SilentWolf.Scores.get_high_scores(10, ld_name), "sw_scores_received")
	
	if function_increment == current_increment:
		# Print only if no button has been pressed
		var scores = []
		if ld_name in SilentWolf.Scores.leaderboards:
			scores = SilentWolf.Scores.leaderboards[ld_name]
		var local_scores = SilentWolf.Scores.local_scores
		
		#print(ld_name)
		
		if len(scores) > 0: 
			render_board(scores, local_scores)
	pass # Replace with function body.


func _on_RightButton_pressed():
	current_increment = current_increment + 1
	var function_increment = current_increment
	audio_manager.play_sfx(load("res://assets/sfx/gui/BipOK.wav"), 0, 1)
	#TODO : Mettre un mutex
	list_index = list_index + 1
	if list_index > 6:
		list_index = 1
		if difficulty_index == "normal":
			difficulty_index = "hard"
			$Level/Difficulty.text = tr("KEY_HARD")
		else:
			difficulty_index = "normal"
			$Level/Difficulty.text = tr("KEY_NORMAL")
	ld_name = "Level"+str(list_index)+"Score"+difficulty_index
	$Level.text = tr("KEY_LEVEL"+ str(list_index))
	for node in $VBoxContainer.get_children():
		if node.name != "HBoxTitle":
			node.queue_free()
	yield(SilentWolf.Scores.get_high_scores(10, ld_name), "sw_scores_received")
	
	if function_increment == current_increment:
		# Print only if no button has been pressed
		var scores = []
		if ld_name in SilentWolf.Scores.leaderboards:
			scores = SilentWolf.Scores.leaderboards[ld_name]
		var local_scores = SilentWolf.Scores.local_scores
		
		#print(ld_name)
		
		if len(scores) > 0: 
			render_board(scores, local_scores)
	pass # Replace with function body.
