extends CanvasLayer

var boost_green_selected = 1.3

var TowerSmallLaserClass = preload("res://actor/Tower/TowerSmallLaser/TowerSmallLaser.tscn")
var TowerBomberClass = preload("res://actor/Tower/TowerBomber/TowerBomber.tscn")
var TowerThunderClass = preload("res://actor/Tower/TowerThunder/TowerThunder.tscn")
var TowerHealerClass = preload("res://actor/Tower/TowerHealing/TowerHealing.tscn")
var TowerSniperClass = preload("res://actor/Tower/TowerSniper/TowerSniper.tscn")
var TowerFocusClass = preload("res://actor/Tower/TowerFocusLaser/TowerFocusLaser.tscn")

var BonusRegenLifeClass = preload("res://actor/Bonus/RegenLife/LaunchedRegenLife.tscn")
var BonusMeteorClass = preload("res://actor/Bonus/Meteor/LaunchedMeteor.tscn")
var BonusLargeMeteorClass = preload("res://actor/Bonus/LargeMeteor/LaunchedLargeMeteor.tscn")
var BonusIncDamageClass = preload("res://actor/Bonus/IncDamage/LaunchedIncDamage.tscn")
var BonusIncDefenseClass = preload("res://actor/Bonus/IncDefence/LaunchedIncDefence.tscn")

# Called when the node enters the scene tree for the first time.
func _ready():
	$Gold/Label.text = str(global.gold_ressource)
	$Control/ScrollContainer/GridContainer/TestButton/Label.text = str(global.towers["small"]["price"])
	$Control/ScrollContainer/GridContainer/ThunderButton/Label.text = str(global.towers["thunder"]["price"])
	$Control/ScrollContainer/GridContainer/BomberButton/Label.text = str(global.towers["bomber"]["price"])
	$Control/ScrollContainer/GridContainer/HealerButton/Label.text = str(global.towers["healer"]["price"])
	$Control/BuyCollectorButton/Label.text = str(global.collector_price)
	
	if not global.towers["small"]["unlocked"]:
		$Control/ScrollContainer/GridContainer/TestButton.disabled = true
		$Control/ScrollContainer/GridContainer/TestButton/Sprite.modulate = Color(1,1,1,0.3)
		$Control/ScrollContainer/GridContainer/TestButton/Label.visible = false
		$Control/ScrollContainer/GridContainer/TestButton/SpriteGold.visible = false
	if not global.towers["bomber"]["unlocked"]:
		$Control/ScrollContainer/GridContainer/BomberButton.disabled = true
		$Control/ScrollContainer/GridContainer/BomberButton/Sprite.modulate = Color(1,1,1,0.3)
		$Control/ScrollContainer/GridContainer/BomberButton/Label.visible = false
		$Control/ScrollContainer/GridContainer/BomberButton/SpriteGold.visible = false
	if not global.towers["thunder"]["unlocked"]:
		$Control/ScrollContainer/GridContainer/ThunderButton.disabled = true
		$Control/ScrollContainer/GridContainer/ThunderButton/Sprite.modulate = Color(1,1,1,0.3)
		$Control/ScrollContainer/GridContainer/ThunderButton/Label.visible = false
		$Control/ScrollContainer/GridContainer/ThunderButton/SpriteGold.visible = false
	if not global.towers["healer"]["unlocked"]:
		$Control/ScrollContainer/GridContainer/HealerButton.disabled = true
		$Control/ScrollContainer/GridContainer/HealerButton/Sprite.modulate = Color(1,1,1,0.3)
		$Control/ScrollContainer/GridContainer/HealerButton/Label.visible = false
		$Control/ScrollContainer/GridContainer/HealerButton/SpriteGold.visible = false
	if not global.towers["sniper"]["unlocked"]:
		$Control/ScrollContainer/GridContainer/SniperButton.disabled = true
		$Control/ScrollContainer/GridContainer/SniperButton/Sprite.modulate = Color(1,1,1,0.3)
		$Control/ScrollContainer/GridContainer/SniperButton/Label.visible = false
		$Control/ScrollContainer/GridContainer/SniperButton/SpriteGold.visible = false
	if not global.towers["focus_laser"]["unlocked"]:
		$Control/ScrollContainer/GridContainer/FocusButton.disabled = true
		$Control/ScrollContainer/GridContainer/FocusButton/Sprite.modulate = Color(1,1,1,0.3)
		$Control/ScrollContainer/GridContainer/FocusButton/Label.visible = false
		$Control/ScrollContainer/GridContainer/FocusButton/SpriteGold.visible = false
	
	if not global.collector_unlocked:
		$Control/BuyCollectorButton.disabled = true
		$Control/BuyCollectorButton/Sprite.modulate = Color(1,1,1,0.3)
		
	if not global.miner_unlocked:
		$Control/MinerButton.disabled = true
		$Control/MinerButton/Sprite.modulate = Color(1,1,1,0.3)
	
	var _result = EventBus.connect("gold_amount_changed", self, "updated_gold_amount")
	_result = EventBus.connect("unholding_tower", self, "unholding_tower_button")
	_result = EventBus.connect("update_collector_number", self, "update_collector_number")
	_result = EventBus.connect("collector_selected", self, "collector_selected")
	_result = EventBus.connect("miner_selected", self, "miner_selected")
	_result = EventBus.connect("ground_gold_amount_changed", self, "ground_gold_amount_changed")
	_result = EventBus.connect("super_gold_amount_changed", self, "updated_gold_amount")
	_result = EventBus.connect("unholding_bonus", self, "unholding_bonus_button")
	_result = EventBus.connect("update_bonus", self, "update_bonus")
	
	update_bonus()
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	$WaveLabel/WaveValue.text = str(global.current_phase)
#	pass

func updated_gold_amount():
	$Gold/Label.text = str(global.gold_ressource)
	$SuperGold/Label.text = str(global.super_gold_ressource)

	if global.gold_ressource >= global.towers["small"]["price"] and global.towers["small"]["unlocked"]:
		if $Control/ScrollContainer/GridContainer/TestButton.disabled:
			$Control/ScrollContainer/GridContainer/TestButton.disabled = false
			$Control/ScrollContainer/GridContainer/TestButton/Sprite.modulate = Color(1,1,1,1)
	else:
		if not $Control/ScrollContainer/GridContainer/TestButton.disabled:
			$Control/ScrollContainer/GridContainer/TestButton.disabled = true
			$Control/ScrollContainer/GridContainer/TestButton/Sprite.modulate = Color(1,1,1,0.3)

	if global.gold_ressource >= global.towers["bomber"]["price"] and global.towers["bomber"]["unlocked"]:
		if $Control/ScrollContainer/GridContainer/BomberButton.disabled:
			$Control/ScrollContainer/GridContainer/BomberButton.disabled = false
			$Control/ScrollContainer/GridContainer/BomberButton/Sprite.modulate = Color(1,1,1,1)
	else:
		if not $Control/ScrollContainer/GridContainer/BomberButton.disabled:
			$Control/ScrollContainer/GridContainer/BomberButton.disabled = true
			$Control/ScrollContainer/GridContainer/BomberButton/Sprite.modulate = Color(1,1,1,0.3)
		
	if global.gold_ressource >= global.towers["thunder"]["price"] and global.towers["thunder"]["unlocked"]:
		if $Control/ScrollContainer/GridContainer/ThunderButton.disabled:
			$Control/ScrollContainer/GridContainer/ThunderButton.disabled = false
			$Control/ScrollContainer/GridContainer/ThunderButton/Sprite.modulate = Color(1,1,1,1)
	else:
		if not $Control/ScrollContainer/GridContainer/ThunderButton.disabled:
			$Control/ScrollContainer/GridContainer/ThunderButton.disabled = true
			$Control/ScrollContainer/GridContainer/ThunderButton/Sprite.modulate = Color(1,1,1,0.3)
		
	if global.gold_ressource >= global.towers["healer"]["price"] and global.towers["healer"]["unlocked"]:
		if $Control/ScrollContainer/GridContainer/HealerButton.disabled:
			$Control/ScrollContainer/GridContainer/HealerButton.disabled = false
			$Control/ScrollContainer/GridContainer/HealerButton/Sprite.modulate = Color(1,1,1,1)
	else:
		if not $Control/ScrollContainer/GridContainer/HealerButton.disabled:
			$Control/ScrollContainer/GridContainer/HealerButton.disabled = true
			$Control/ScrollContainer/GridContainer/HealerButton/Sprite.modulate = Color(1,1,1,0.3)
	
	if global.gold_ressource >= global.towers["sniper"]["price"] and global.towers["sniper"]["unlocked"]:
		if $Control/ScrollContainer/GridContainer/SniperButton.disabled:
			$Control/ScrollContainer/GridContainer/SniperButton.disabled = false
			$Control/ScrollContainer/GridContainer/SniperButton/Sprite.modulate = Color(1,1,1,1)
	else:
		if not $Control/ScrollContainer/GridContainer/SniperButton.disabled:
			$Control/ScrollContainer/GridContainer/SniperButton.disabled = true
			$Control/ScrollContainer/GridContainer/SniperButton/Sprite.modulate = Color(1,1,1,0.3)
			
	if global.gold_ressource >= global.towers["focus_laser"]["price"] and global.towers["focus_laser"]["unlocked"]:
		if $Control/ScrollContainer/GridContainer/FocusButton.disabled:
			$Control/ScrollContainer/GridContainer/FocusButton.disabled = false
			$Control/ScrollContainer/GridContainer/FocusButton/Sprite.modulate = Color(1,1,1,1)
	else:
		if not $Control/ScrollContainer/GridContainer/FocusButton.disabled:
			$Control/ScrollContainer/GridContainer/FocusButton.disabled = true
			$Control/ScrollContainer/GridContainer/FocusButton/Sprite.modulate = Color(1,1,1,0.3)
			
	if global.gold_ressource >= global.miner_price and global.miner_unlocked:
		if $Control/MinerButton.disabled:
			$Control/MinerButton.disabled = false
			$Control/MinerButton/Sprite.modulate = Color(1,1,1,1)
	else:
		if not $Control/MinerButton.disabled:
			$Control/MinerButton.disabled = true
			$Control/MinerButton/Sprite.modulate = Color(1,1,1,0.3)
			
	if (global.gold_ressource >= global.collector_price) and global.collector_unlocked:
		$Control/BuyCollectorButton.disabled = false
		$Control/BuyCollectorButton/Sprite.modulate = Color(1,1,1,1)
	else:
		$Control/BuyCollectorButton.disabled = true
		$Control/BuyCollectorButton/Sprite.modulate = Color(1,1,1,0.3)
		
	
	pass

func ground_gold_amount_changed():
	$OnGround/Label.text = str(global.on_ground_gold_ressource)
	pass
	
func unholding_tower_button():
	if not $Control/ScrollContainer/GridContainer/TestButton.disabled:
		$Control/ScrollContainer/GridContainer/TestButton/Sprite.modulate = Color(1,1,1)
	if not $Control/ScrollContainer/GridContainer/ThunderButton.disabled:
		$Control/ScrollContainer/GridContainer/ThunderButton/Sprite.modulate = Color(1,1,1)
	if not $Control/ScrollContainer/GridContainer/BomberButton.disabled:
		$Control/ScrollContainer/GridContainer/BomberButton/Sprite.modulate = Color(1,1,1)
	if not $Control/ScrollContainer/GridContainer/HealerButton.disabled:
		$Control/ScrollContainer/GridContainer/HealerButton/Sprite.modulate = Color(1,1,1)
	if not $Control/ScrollContainer/GridContainer/SniperButton.disabled:
		$Control/ScrollContainer/GridContainer/SniperButton/Sprite.modulate = Color(1,1,1)
	if not $Control/ScrollContainer/GridContainer/FocusButton.disabled:
		$Control/ScrollContainer/GridContainer/FocusButton/Sprite.modulate = Color(1,1,1)
	pass

func unholding_bonus_button():
	if not $Control/RegenButton.disabled:
		$Control/RegenButton/Sprite.modulate = Color(1,1,1)
	if not $Control/MeteorButton.disabled:
		$Control/MeteorButton/Sprite.modulate = Color(1,1,1)
	if not $Control/LargeMeteorButton.disabled:
		$Control/LargeMeteorButton/Sprite.modulate = Color(1,1,1)
	if not $Control/IncDamageButton.disabled:
		$Control/IncDamageButton/Sprite.modulate = Color(1,1,1)
	if not $Control/IncDefenseButton.disabled:
		$Control/IncDefenseButton/Sprite.modulate = Color(1,1,1)
	pass

func update_bonus():
	$Control/RegenButton/Label.text = str(global.current_bonus[global.ressource_type.RESSOURCE_REGEN_LIFE])
	if global.current_bonus[global.ressource_type.RESSOURCE_REGEN_LIFE] > 0:
		if $Control/RegenButton.disabled:
			$Control/RegenButton.disabled = false
			$Control/RegenButton/Sprite.modulate = Color(1,1,1,1)
	else:
		if not $Control/RegenButton.disabled:
			$Control/RegenButton.disabled = true
			$Control/RegenButton/Sprite.modulate = Color(1,1,1,0.3)
			
	$Control/MeteorButton/Label.text = str(global.current_bonus[global.ressource_type.RESSOURCE_METEOR])
	if global.current_bonus[global.ressource_type.RESSOURCE_METEOR] > 0:
		if $Control/MeteorButton.disabled:
			$Control/MeteorButton.disabled = false
			$Control/MeteorButton/Sprite.modulate = Color(1,1,1,1)
	else:
		if not $Control/MeteorButton.disabled:
			$Control/MeteorButton.disabled = true
			$Control/MeteorButton/Sprite.modulate = Color(1,1,1,0.3)
	
	$Control/LargeMeteorButton/Label.text = str(global.current_bonus[global.ressource_type.RESSOURCE_LARGE_METEOR])
	if global.current_bonus[global.ressource_type.RESSOURCE_LARGE_METEOR] > 0:
		if $Control/LargeMeteorButton.disabled:
			$Control/LargeMeteorButton.disabled = false
			$Control/LargeMeteorButton/Sprite.modulate = Color(1,1,1,1)
	else:
		if not $Control/LargeMeteorButton.disabled:
			$Control/LargeMeteorButton.disabled = true
			$Control/LargeMeteorButton/Sprite.modulate = Color(1,1,1,0.3)
	pass
	
	$Control/IncDamageButton/Label.text = str(global.current_bonus[global.ressource_type.RESSOURCE_INC_DAMAGE])
	if global.current_bonus[global.ressource_type.RESSOURCE_INC_DAMAGE] > 0:
		if $Control/IncDamageButton.disabled:
			$Control/IncDamageButton.disabled = false
			$Control/IncDamageButton/Sprite.modulate = Color(1,1,1,1)
	else:
		if not $Control/IncDamageButton.disabled:
			$Control/IncDamageButton.disabled = true
			$Control/IncDamageButton/Sprite.modulate = Color(1,1,1,0.3)
	pass
	
	$Control/IncDefenseButton/Label.text = str(global.current_bonus[global.ressource_type.RESSOURCE_INC_DEFENCE])
	if global.current_bonus[global.ressource_type.RESSOURCE_INC_DEFENCE] > 0:
		if $Control/IncDefenseButton.disabled:
			$Control/IncDefenseButton.disabled = false
			$Control/IncDefenseButton/Sprite.modulate = Color(1,1,1,1)
	else:
		if not $Control/IncDefenseButton.disabled:
			$Control/IncDefenseButton.disabled = true
			$Control/IncDefenseButton/Sprite.modulate = Color(1,1,1,0.3)
	pass

func collector_selected(selected):
	if not selected:
		if not $Control/CollectorButton.disabled:
			$Control/CollectorButton/Sprite.modulate = Color(1,1,1)
	pass
	
func miner_selected(selected):
	if not selected:
		if not $Control/MinerButton.disabled:
			$Control/MinerButton/Sprite.modulate = Color(1,1,1)
	pass

func reset_selected_button():
	if not $Control/ScrollContainer/GridContainer/TestButton.disabled:
		$Control/ScrollContainer/GridContainer/TestButton/Sprite.modulate = Color(1,1,1)
	if not $Control/ScrollContainer/GridContainer/ThunderButton.disabled:
		$Control/ScrollContainer/GridContainer/ThunderButton/Sprite.modulate = Color(1,1,1)
	if not $Control/ScrollContainer/GridContainer/BomberButton.disabled:
		$Control/ScrollContainer/GridContainer/BomberButton/Sprite.modulate = Color(1,1,1)
	if not $Control/ScrollContainer/GridContainer/HealerButton.disabled:
		$Control/ScrollContainer/GridContainer/HealerButton/Sprite.modulate = Color(1,1,1)
	if not $Control/ScrollContainer/GridContainer/SniperButton.disabled:
		$Control/ScrollContainer/GridContainer/SniperButton/Sprite.modulate = Color(1,1,1)
	if not $Control/ScrollContainer/GridContainer/FocusButton.disabled:
		$Control/ScrollContainer/GridContainer/FocusButton/Sprite.modulate = Color(1,1,1)
	
	if not $Control/RegenButton.disabled:
		$Control/RegenButton/Sprite.modulate = Color(1,1,1)
	if not $Control/MeteorButton.disabled:
		$Control/MeteorButton/Sprite.modulate = Color(1,1,1)
	if not $Control/LargeMeteorButton.disabled:
		$Control/LargeMeteorButton/Sprite.modulate = Color(1,1,1)
	if not $Control/IncDamageButton.disabled:
		$Control/IncDamageButton/Sprite.modulate = Color(1,1,1)
	if not $Control/IncDefenseButton.disabled:
		$Control/IncDefenseButton/Sprite.modulate = Color(1,1,1)
		
func _on_TestButton_pressed():
	audio_manager.play_sfx(load("res://assets/sfx/gui/BipOK.wav"), 0, 1)
	if tower_select.holding_tower:
		tower_select.holding_tower.queue_free()
		tower_select.holding_tower = null
	var tower_actor = TowerSmallLaserClass.instance() # TowerSniperClass.instance() #
	tower_select.holding_tower = tower_actor
	tower_actor.picked = true
	get_parent().get_node("GroundObject").call_deferred("add_child", tower_actor)
	
	reset_selected_button()
	$Control/ScrollContainer/GridContainer/TestButton/Sprite.modulate = Color(1,boost_green_selected,1)
	pass # Replace with function body.

func _on_BomberButton_pressed():
	audio_manager.play_sfx(load("res://assets/sfx/gui/BipOK.wav"), 0, 1)
	if tower_select.holding_tower:
		tower_select.holding_tower.queue_free()
		tower_select.holding_tower = null
	var tower_actor = TowerBomberClass.instance()
	tower_select.holding_tower = tower_actor
	tower_actor.picked = true
	get_parent().get_node("GroundObject").call_deferred("add_child", tower_actor)
	
	reset_selected_button()
	$Control/ScrollContainer/GridContainer/BomberButton/Sprite.modulate = Color(1,boost_green_selected,1)
	pass # Replace with function body.


func _on_ThunderButton_pressed():
	audio_manager.play_sfx(load("res://assets/sfx/gui/BipOK.wav"), 0, 1)
	if tower_select.holding_tower:
		tower_select.holding_tower.queue_free()
		tower_select.holding_tower = null
	var tower_actor = TowerThunderClass.instance()
	tower_select.holding_tower = tower_actor
	tower_actor.picked = true
	get_parent().get_node("GroundObject").call_deferred("add_child", tower_actor)
	
	reset_selected_button()
	$Control/ScrollContainer/GridContainer/ThunderButton/Sprite.modulate = Color(1,boost_green_selected,1)
	pass # Replace with function body.

func _on_HealerButton_pressed():
	audio_manager.play_sfx(load("res://assets/sfx/gui/BipOK.wav"), 0, 1)
	if tower_select.holding_tower:
		tower_select.holding_tower.queue_free()
		tower_select.holding_tower = null
	var tower_actor = TowerHealerClass.instance()
	tower_select.holding_tower = tower_actor
	tower_actor.picked = true
	get_parent().get_node("GroundObject").call_deferred("add_child", tower_actor)
	
	reset_selected_button()
	$Control/ScrollContainer/GridContainer/HealerButton/Sprite.modulate = Color(1,boost_green_selected,1)
	pass # Replace with function body.

func _on_SniperButton_pressed():
	audio_manager.play_sfx(load("res://assets/sfx/gui/BipOK.wav"), 0, 1)
	if tower_select.holding_tower:
		tower_select.holding_tower.queue_free()
		tower_select.holding_tower = null
	var tower_actor = TowerSniperClass.instance()
	tower_select.holding_tower = tower_actor
	tower_actor.picked = true
	get_parent().get_node("GroundObject").call_deferred("add_child", tower_actor)
	
	reset_selected_button()
	$Control/ScrollContainer/GridContainer/SniperButton/Sprite.modulate = Color(1,boost_green_selected,1)
	pass # Replace with function body.
	
func _on_MinerButton_pressed():
	audio_manager.play_sfx(load("res://assets/sfx/gui/BipOK.wav"), 0, 1)
	EventBus.emit_signal("miner_selected", true)
	if not $Control/MinerButton.disabled:
		$Control/MinerButton/Sprite.modulate = Color(1,boost_green_selected,1)
	pass # Replace with function body.

func _on_CollectorButton_pressed():
	audio_manager.play_sfx(load("res://assets/sfx/gui/BipOK.wav"), 0, 1)
	EventBus.emit_signal("collector_selected", true)
	if not $Control/CollectorButton.disabled:
		$Control/CollectorButton/Sprite.modulate = Color(1,boost_green_selected,1)
	pass # Replace with function body.
	
func update_collector_number(number_out, total_number):
#	$GeneralGUI/Control/CollectButton/AvailableLabel.text = str(total_number - number_out)
#	$GeneralGUI/Control/CollectButton/MaxLabel.text = "/" + str(total_number)

	$Control/CollectorButton/Label.text = str(total_number - number_out) + "/" + str(total_number)

	if number_out < total_number:
		if $Control/CollectorButton.disabled:
			$Control/CollectorButton.disabled = false
			$Control/CollectorButton/Sprite.modulate = Color(1,1,1,1)
	else:
		if not $Control/CollectorButton.disabled:
			$Control/CollectorButton.disabled = true
			$Control/CollectorButton/Sprite.modulate = Color(1,1,1,0.3)
	pass

func _on_BuyCollectorButton_pressed():
	if (global.gold_ressource >= global.collector_price):
		audio_manager.play_sfx(load("res://assets/sfx/gui/BipOK.wav"), 0, 1)
		EventBus.emit_signal("create_collector")
	pass # Replace with function body.

func _on_TestButton_mouse_entered():
	$Tooltip.visible = true
	$Tooltip.rect_position = $Control/ScrollContainer/GridContainer/TestButton.rect_global_position + Vector2(-185, -150)
	$Tooltip/Label.text = tr(global.towers["small"]["tooltip"]) % [global.towers["small"]["swarm_health"], global.towers["small"]["swarm_damage"], global.towers["small"]["swarm_fire_rate"]]
	if not $Control/ScrollContainer/GridContainer/TestButton.disabled:
		$Control/ScrollContainer/GridContainer/TestButton.modulate = Color(1.3,1.3,1.3)
	pass # Replace with function body.


func _on_TestButton_mouse_exited():
	$Tooltip.visible = false
	if not $Control/ScrollContainer/GridContainer/TestButton.disabled:
		$Control/ScrollContainer/GridContainer/TestButton.modulate = Color(1,1,1)
	pass # Replace with function body.


func _on_ThunderButton_mouse_entered():
	$Tooltip.visible = true
	$Tooltip.rect_position = $Control/ScrollContainer/GridContainer/ThunderButton.rect_global_position + Vector2(-185, -150)
	$Tooltip/Label.text = tr(global.towers["thunder"]["tooltip"]) % [global.towers["thunder"]["swarm_health"], global.towers["thunder"]["swarm_damage"], global.towers["thunder"]["swarm_fire_rate"]]
	if not $Control/ScrollContainer/GridContainer/ThunderButton.disabled:
		$Control/ScrollContainer/GridContainer/ThunderButton.modulate = Color(1.3,1.3,1.3)
	pass # Replace with function body.


func _on_ThunderButton_mouse_exited():
	$Tooltip.visible = false
	if not $Control/ScrollContainer/GridContainer/ThunderButton.disabled:
		$Control/ScrollContainer/GridContainer/ThunderButton.modulate = Color(1,1,1)
	pass # Replace with function body.


func _on_BomberButton_mouse_entered():
	$Tooltip.visible = true
	$Tooltip.rect_position = $Control/ScrollContainer/GridContainer/BomberButton.rect_global_position + Vector2(-185, -150)
	$Tooltip/Label.text = tr(global.towers["bomber"]["tooltip"]) % [global.towers["bomber"]["swarm_health"], global.towers["bomber"]["swarm_damage"], global.towers["bomber"]["swarm_fire_rate"]]
	if not $Control/ScrollContainer/GridContainer/BomberButton.disabled:
		$Control/ScrollContainer/GridContainer/BomberButton.modulate = Color(1.3,1.3,1.3)
	pass # Replace with function body.


func _on_BomberButton_mouse_exited():
	$Tooltip.visible = false
	if not $Control/ScrollContainer/GridContainer/BomberButton.disabled:
		$Control/ScrollContainer/GridContainer/BomberButton.modulate = Color(1,1,1)
	pass # Replace with function body.


func _on_HealerButton_mouse_entered():
	$Tooltip.visible = true
	$Tooltip.rect_position = $Control/ScrollContainer/GridContainer/HealerButton.rect_global_position + Vector2(-185, -150)
	$Tooltip/Label.text = tr(global.towers["healer"]["tooltip"]) % [global.towers["healer"]["swarm_health"], global.towers["healer"]["swarm_damage"], global.towers["healer"]["swarm_fire_rate"]]
	if not $Control/ScrollContainer/GridContainer/HealerButton.disabled:
		$Control/ScrollContainer/GridContainer/HealerButton.modulate = Color(1.3,1.3,1.3)
	pass # Replace with function body.


func _on_HealerButton_mouse_exited():
	$Tooltip.visible = false
	if not $Control/ScrollContainer/GridContainer/HealerButton.disabled:
		$Control/ScrollContainer/GridContainer/HealerButton.modulate = Color(1,1,1)
	pass # Replace with function body.


func _on_MinerButton_mouse_entered():
	$Tooltip.visible = true
	$Tooltip.rect_position = $Control/MinerButton.rect_global_position + Vector2(-185, -150)
	$Tooltip/Label.text = tr("KEY_BUY_MINER")
	if not $Control/MinerButton.disabled:
		$Control/MinerButton.modulate = Color(1.3,1.3,1.3)
	pass # Replace with function body.


func _on_MinerButton_mouse_exited():
	$Tooltip.visible = false
	if not $Control/MinerButton.disabled:
		$Control/MinerButton.modulate = Color(1,1,1)
	pass # Replace with function body.


func _on_CollectorButton_mouse_entered():
	$Tooltip.visible = true
	$Tooltip.rect_position = $Control/CollectorButton.rect_global_position + Vector2(-185, -150)
	$Tooltip/Label.text = tr("KEY_USE_COLLECTOR")
	if not $Control/CollectorButton.disabled:
		$Control/CollectorButton.modulate = Color(1.3,1.3,1.3)
	pass # Replace with function body.


func _on_CollectorButton_mouse_exited():
	$Tooltip.visible = false
	if not $Control/CollectorButton.disabled:
		$Control/CollectorButton.modulate = Color(1,1,1)
	pass # Replace with function body.


func _on_BuyCollectorButton_mouse_entered():
	$Tooltip.visible = true
	$Tooltip.rect_position = $Control/BuyCollectorButton.rect_global_position + Vector2(-185, -150)
	$Tooltip/Label.text = tr("KEY_BUY_COLLECTOR")
	if not $Control/BuyCollectorButton.disabled:
		$Control/BuyCollectorButton.modulate = Color(1.3,1.3,1.3)
	pass # Replace with function body.


func _on_BuyCollectorButton_mouse_exited():
	$Tooltip.visible = false
	if not $Control/BuyCollectorButton.disabled:
		$Control/BuyCollectorButton.modulate = Color(1,1,1)
	pass # Replace with function body.


func _on_SniperButton_mouse_entered():
	$Tooltip.visible = true
	$Tooltip.rect_position = $Control/ScrollContainer/GridContainer/SniperButton.rect_global_position + Vector2(-185, -150)
	$Tooltip/Label.text = tr(global.towers["sniper"]["tooltip"]) % [global.towers["sniper"]["swarm_health"], global.towers["sniper"]["swarm_damage"], global.towers["sniper"]["swarm_fire_rate"]]
	if not $Control/ScrollContainer/GridContainer/SniperButton.disabled:
		$Control/ScrollContainer/GridContainer/SniperButton.modulate = Color(1.3,1.3,1.3)
	pass # Replace with function body.

func _on_SniperButton_mouse_exited():
	$Tooltip.visible = false
	if not $Control/ScrollContainer/GridContainer/SniperButton.disabled:
		$Control/ScrollContainer/GridContainer/SniperButton.modulate = Color(1,1,1)
	pass # Replace with function body.


func _on_FocusButton_pressed():
	audio_manager.play_sfx(load("res://assets/sfx/gui/BipOK.wav"), 0, 1)
	if tower_select.holding_tower:
		tower_select.holding_tower.queue_free()
		tower_select.holding_tower = null
	var tower_actor = TowerFocusClass.instance()
	tower_select.holding_tower = tower_actor
	tower_actor.picked = true
	get_parent().get_node("GroundObject").call_deferred("add_child", tower_actor)
	
	reset_selected_button()
	$Control/ScrollContainer/GridContainer/FocusButton/Sprite.modulate = Color(1,boost_green_selected,1)
	pass # Replace with function body.


func _on_FocusButton_mouse_entered():
	$Tooltip.visible = true
	$Tooltip.rect_position = $Control/ScrollContainer/GridContainer/FocusButton.rect_global_position + Vector2(-185, -150)
	$Tooltip/Label.text = tr(global.towers["focus_laser"]["tooltip"]) % [global.towers["focus_laser"]["swarm_health"], global.towers["focus_laser"]["swarm_damage"], global.towers["focus_laser"]["swarm_fire_rate"]]
	if not $Control/ScrollContainer/GridContainer/FocusButton.disabled:
		$Control/ScrollContainer/GridContainer/FocusButton.modulate = Color(1.3,1.3,1.3)
	pass # Replace with function body.


func _on_FocusButton_mouse_exited():
	$Tooltip.visible = false
	if not $Control/ScrollContainer/GridContainer/FocusButton.disabled:
		$Control/ScrollContainer/GridContainer/FocusButton.modulate = Color(1,1,1)
	pass # Replace with function body.


func _on_RegenButton_pressed():
	audio_manager.play_sfx(load("res://assets/sfx/gui/BipOK.wav"), 0, 1)
	if tower_select.holding_bonus:
		tower_select.holding_bonus.queue_free()
		tower_select.holding_bonus = null
	var bonus_actor = BonusRegenLifeClass.instance()
	tower_select.holding_bonus = bonus_actor
	bonus_actor.picked = true
	get_parent().get_node("GroundObject").call_deferred("add_child", bonus_actor)
	
	reset_selected_button()
	$Control/RegenButton/Sprite.modulate = Color(1,boost_green_selected,1)
	pass # Replace with function body.


func _on_RegenButton_mouse_entered():
	$Tooltip.visible = true
	$Tooltip.rect_position = $Control/RegenButton.rect_global_position + Vector2(-185, -150)
	$Tooltip/Label.text = tr(global.bonus_tooltips[global.ressource_type.RESSOURCE_REGEN_LIFE])
	if not $Control/RegenButton.disabled:
		$Control/RegenButton.modulate = Color(1.3,1.3,1.3)
	pass # Replace with function body.


func _on_RegenButton_mouse_exited():
	$Tooltip.visible = false
	if not $Control/RegenButton.disabled:
		$Control/RegenButton.modulate = Color(1,1,1)
	pass # Replace with function body.


func _on_GoldTooltip_mouse_entered():
	#print("test")
	$Tooltip.visible = true
	$Tooltip.rect_position = $Gold/Tooltip.rect_global_position + Vector2(0, -150)
	$Tooltip/Label.text = tr("KEY_GOLD_TOOLTIP")
	pass # Replace with function body.


func _on_GoldTooltip_mouse_exited():
	$Tooltip.visible = false
	pass # Replace with function body.


func _on_SuperGoldTooltip_mouse_entered():
	$Tooltip.visible = true
	$Tooltip.rect_position = $SuperGold/Tooltip.rect_global_position + Vector2(0, -150)
	$Tooltip/Label.text = tr("KEY_SUPER_GOLD_TOOLTIP")
	pass # Replace with function body.


func _on_SuperGoldTooltip_mouse_exited():
	$Tooltip.visible = false
	pass # Replace with function body.


func _on_MeteorButton_pressed():
	audio_manager.play_sfx(load("res://assets/sfx/gui/BipOK.wav"), 0, 1)
	if tower_select.holding_bonus:
		tower_select.holding_bonus.queue_free()
		tower_select.holding_bonus = null
	var bonus_actor = BonusMeteorClass.instance()
	tower_select.holding_bonus = bonus_actor
	bonus_actor.picked = true
	get_parent().get_node("GroundObject").call_deferred("add_child", bonus_actor)
	
	reset_selected_button()
	$Control/MeteorButton/Sprite.modulate = Color(1,boost_green_selected,1)
	pass # Replace with function body.


func _on_MeteorButton_mouse_entered():
	$Tooltip.visible = true
	$Tooltip.rect_position = $Control/MeteorButton.rect_global_position + Vector2(-185, -150)
	$Tooltip/Label.text = tr(global.bonus_tooltips[global.ressource_type.RESSOURCE_METEOR])
	if not $Control/MeteorButton.disabled:
		$Control/MeteorButton.modulate = Color(1.3,1.3,1.3)
	pass # Replace with function body.


func _on_MeteorButton_mouse_exited():
	$Tooltip.visible = false
	if not $Control/MeteorButton.disabled:
		$Control/MeteorButton.modulate = Color(1,1,1)
	pass # Replace with function body.


func _on_LargeMeteorButton_pressed():
	audio_manager.play_sfx(load("res://assets/sfx/gui/BipOK.wav"), 0, 1)
	if tower_select.holding_bonus:
		tower_select.holding_bonus.queue_free()
		tower_select.holding_bonus = null
	var bonus_actor = BonusLargeMeteorClass.instance()
	tower_select.holding_bonus = bonus_actor
	bonus_actor.picked = true
	get_parent().get_node("GroundObject").call_deferred("add_child", bonus_actor)
	
	reset_selected_button()
	$Control/LargeMeteorButton/Sprite.modulate = Color(1,boost_green_selected,1)
	pass # Replace with function body.


func _on_LargeMeteorButton_mouse_entered():
	$Tooltip.visible = true
	$Tooltip.rect_position = $Control/LargeMeteorButton.rect_global_position + Vector2(-185, -150)
	$Tooltip/Label.text = tr(global.bonus_tooltips[global.ressource_type.RESSOURCE_LARGE_METEOR])
	if not $Control/LargeMeteorButton.disabled:
		$Control/LargeMeteorButton.modulate = Color(1.3,1.3,1.3)
	pass # Replace with function body.


func _on_LargeMeteorButton_mouse_exited():
	$Tooltip.visible = false
	if not $Control/LargeMeteorButton.disabled:
		$Control/LargeMeteorButton.modulate = Color(1,1,1)
	pass # Replace with function body.


func _on_IncDamageButton_pressed():
	audio_manager.play_sfx(load("res://assets/sfx/gui/BipOK.wav"), 0, 1)
	if tower_select.holding_bonus:
		tower_select.holding_bonus.queue_free()
		tower_select.holding_bonus = null
	var bonus_actor = BonusIncDamageClass.instance()
	tower_select.holding_bonus = bonus_actor
	bonus_actor.picked = true
	get_parent().get_node("GroundObject").call_deferred("add_child", bonus_actor)
	
	reset_selected_button()
	$Control/IncDamageButton/Sprite.modulate = Color(1,boost_green_selected,1)
	pass # Replace with function body.


func _on_IncDamageButton_mouse_entered():
	$Tooltip.visible = true
	$Tooltip.rect_position = $Control/IncDamageButton.rect_global_position + Vector2(-185, -150)
	$Tooltip/Label.text = tr(global.bonus_tooltips[global.ressource_type.RESSOURCE_INC_DAMAGE])
	if not $Control/IncDamageButton.disabled:
		$Control/IncDamageButton.modulate = Color(1.3,1.3,1.3)
	pass # Replace with function body.


func _on_IncDamageButton_mouse_exited():
	$Tooltip.visible = false
	if not $Control/IncDamageButton.disabled:
		$Control/IncDamageButton.modulate = Color(1,1,1)
	pass # Replace with function body.


func _on_IncDefenseButton_pressed():
	audio_manager.play_sfx(load("res://assets/sfx/gui/BipOK.wav"), 0, 1)
	if tower_select.holding_bonus:
		tower_select.holding_bonus.queue_free()
		tower_select.holding_bonus = null
	var bonus_actor = BonusIncDefenseClass.instance()
	tower_select.holding_bonus = bonus_actor
	bonus_actor.picked = true
	get_parent().get_node("GroundObject").call_deferred("add_child", bonus_actor)
	
	reset_selected_button()
	$Control/IncDefenseButton/Sprite.modulate = Color(1,boost_green_selected,1)
	pass # Replace with function body.


func _on_IncDefenseButton_mouse_entered():
	$Tooltip.visible = true
	$Tooltip.rect_position = $Control/IncDefenseButton.rect_global_position + Vector2(-185, -150)
	$Tooltip/Label.text = tr(global.bonus_tooltips[global.ressource_type.RESSOURCE_INC_DEFENCE])
	if not $Control/IncDefenseButton.disabled:
		$Control/IncDefenseButton.modulate = Color(1.3,1.3,1.3)
	pass # Replace with function body.


func _on_IncDefenseButton_mouse_exited():
	$Tooltip.visible = false
	if not $Control/IncDefenseButton.disabled:
		$Control/IncDefenseButton.modulate = Color(1,1,1)
	pass # Replace with function body.


func _on_LaunchPhase_pressed():
	EventBus.emit_signal("force_start_phase")
	pass # Replace with function body.


func _on_UpdateSlowTimer_timeout():
	$WaveLabel/WaveValue.text = str(global.current_phase)
	$PhaseAnnouncement2/RemainTime.text = str(floor(get_parent().get_node("WaitStartTimer").time_left)) + " sec"
	pass # Replace with function body.


func _on_TextureButton_pressed() -> void:
	get_parent().get_node("PauseMenu").set_pause()
	pass # Replace with function body.
