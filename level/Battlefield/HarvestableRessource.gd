extends Area2D

export var type = global.ressource_type.RESSOURCE_GOLD
export var value = 10

var collected = false
var collector = null
var collected_speed = 5

# Called when the node enters the scene tree for the first time.
func _ready():
	EventBus.connect("collector_selected", self, "collector_selected")
	EventBus.emit_signal("add_ground_gold", value)
	if tower_select.selecting_collect_target:
		z_index = 30
		$SelectableParticles.emitting = true
		$SelectableParticles.restart()
		$SelectableParticles.visible = true
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	if collected:
		var distance = global_position.distance_to(collector.global_position)
		var speed = max(2, min(distance * 0.5, collected_speed))
		var direction = (collector.global_position - global_position).normalized() * speed
		global_position = global_position + direction
		
		if distance < 15:
			queue_free()
		pass
	pass

func collector_selected(selected):
	if selected:
		z_index = 30
		$SelectableParticles.emitting = true
		$SelectableParticles.restart()
		$SelectableParticles.visible = true
	else:
		z_index = 0
		$SelectableParticles.emitting = false
		$SelectableParticles.visible = false
	pass

func collect(col):
	if not collected:
		$CollisionShape2D.set_deferred("disabled", true)
		collector = col
		collected = true
	pass
