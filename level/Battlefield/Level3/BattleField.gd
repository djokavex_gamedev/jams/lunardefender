extends "res://level/Battlefield/BattlefieldTemplate.gd"

var current_animation = "stop"

func rand_fog_time():
	$FogTimer.stop()
	$FogTimer.wait_time = rand_range(15, 30)
	$FogTimer.start()
	pass

func _on_FogTimer_timeout():
	if current_animation == "start":
		$FogAnimationPlayer.play("stop")
		current_animation = "stop"
	else:
		$FogAnimationPlayer.play("start")
		current_animation = "start"
	pass # Replace with function body.
