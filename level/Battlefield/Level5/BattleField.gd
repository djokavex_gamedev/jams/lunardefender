extends "res://level/Battlefield/BattlefieldTemplate.gd"

func AnimationPhase1():
	var phase_offset = 0
	#$Ground/Background.tile_set.tile_set_region(13, Rect2(16+phase_offset, 160-16, 80, 48))
	#$Ground/Background.tile_set.tile_set_region(15, Rect2(16+phase_offset, 288-16, 80, 48))
	$Ground/Lava.tile_set.tile_set_region(2, Rect2(16+phase_offset, 224-16, 80, 48))

func AnimationPhase2():
	var phase_offset = 80
	#$Ground/Background.tile_set.tile_set_region(13, Rect2(16+phase_offset, 160-16, 80, 48))
	#$Ground/Background.tile_set.tile_set_region(15, Rect2(16+phase_offset, 288-16, 80, 48))
	$Ground/Lava.tile_set.tile_set_region(2, Rect2(16+phase_offset, 224-16, 80, 48))

func AnimationPhase3():
	var phase_offset = 160
	#$Ground/Background.tile_set.tile_set_region(13, Rect2(16+phase_offset, 160-16, 80, 48))
	#$Ground/Background.tile_set.tile_set_region(15, Rect2(16+phase_offset, 288-16, 80, 48))
	$Ground/Lava.tile_set.tile_set_region(2, Rect2(16+phase_offset, 224-16, 80, 48))

func AnimationPhase4():
	var phase_offset = 80
	#$Ground/Background.tile_set.tile_set_region(13, Rect2(16+phase_offset, 160-16, 80, 48))
	#$Ground/Background.tile_set.tile_set_region(15, Rect2(16+phase_offset, 288-16, 80, 48))
	$Ground/Lava.tile_set.tile_set_region(2, Rect2(16+phase_offset, 224-16, 80, 48))
