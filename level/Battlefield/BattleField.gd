extends Node2D

#var TowerActorClass = preload("res://actor/Tower/TowerActor.tscn")
#var TowerSmallLaserClass = preload("res://actor/Tower/TowerSmallLaser/TowerSmallLaser.tscn")
#var TowerBomberClass = preload("res://actor/Tower/TowerBomber/TowerBomber.tscn")
#var TowerThunderClass = preload("res://actor/Tower/TowerThunder/TowerThunder.tscn")
#var TowerHealerClass = preload("res://actor/Tower/TowerHealing/TowerHealing.tscn")
var Tank1Class = preload("res://actor/Enemy/tank1/Tank1.tscn")
var Tank2Class = preload("res://actor/Enemy/tank2/Tank2.tscn")
var TankHealClass = preload("res://actor/Enemy/tankheal/TankHeal.tscn")
var Tank4Class = preload("res://actor/Enemy/bigtank/BigTank.tscn")
var Ship1Class = preload("res://actor/Enemy/ship1/EnemyShip1bis.tscn")
var Ship2Class = preload("res://actor/Enemy/ship2/EnemyShip2.tscn")
var Boss1Class = preload("res://actor/Enemy/tank1/Tank1Boss.tscn")
var MeteorClass = preload("res://effects/Meteor/Meteor.tscn")
var EnemyDirectionClass = preload("res://actor/Enemy/EnemyDirection.tscn")

var wave_tank1_counter = 0
var wave_tank2_counter = 0
var wave_tankheal_counter = 0
var wave_tank4_counter = 0
var wave_tank5_counter = 0
var wave_ship1_counter = 0
var wave_ship2_counter = 0
var wave_boss1_counter = 0
var meteor_spawn_phase = 30
var meteor_spawned = 0
var meteor_time = 0.2
var meteor_phase_time = 30

var rect_viewport = get_viewport_rect()
var counter_mouse = 0

var _previousPosition: Vector2 = Vector2(0, 0);
var _moveCamera: bool = false;

var current_direction_enemy = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	$Particles2D.restart()
	$Particles2D.emitting = true
	
	$GroundObject/PlayerCore.connect("died", self, "defeat")
	
	rect_viewport = get_viewport_rect()
	pass # Replace with function body.


func _physics_process(delta: float) -> void:
	#print(str($Camera2D.position.x) + " " + str(_previousPosition))
	if Input.is_action_pressed("ui_right"):
		#print("right")
		$Camera2D.position.x = ($Camera2D.position.x + 1000 * delta);
		
		#print("before" + str($Camera2D.position.x) + " " + str(rect_viewport.size.x / 2.0) + " " + str($Camera2D.limit_right))
		if ($Camera2D.position.x + rect_viewport.size.x * $Camera2D.zoom.x / 2.0) > $Camera2D.limit_right:
			$Camera2D.position.x = $Camera2D.limit_right - rect_viewport.size.x * $Camera2D.zoom.x / 2.0
		if ($Camera2D.position.x - rect_viewport.size.x * $Camera2D.zoom.x / 2.0) < $Camera2D.limit_left:
			$Camera2D.position.x = $Camera2D.limit_left + rect_viewport.size.x * $Camera2D.zoom.x / 2.0
		if ($Camera2D.position.y + rect_viewport.size.y * $Camera2D.zoom.x / 2.0) > $Camera2D.limit_bottom:
			$Camera2D.position.y = $Camera2D.limit_bottom - rect_viewport.size.y * $Camera2D.zoom.x / 2.0
		if ($Camera2D.position.y - rect_viewport.size.y * $Camera2D.zoom.x / 2.0) < $Camera2D.limit_top:
			$Camera2D.position.y = $Camera2D.limit_top + rect_viewport.size.y * $Camera2D.zoom.x / 2.0
		pass
	if Input.is_action_pressed("ui_left"):
		#print("left")
		$Camera2D.position.x = ($Camera2D.position.x - 1000 * delta);
		
		#print("before" + str($Camera2D.position.x) + " " + str(rect_viewport.size.x / 2.0) + " " + str($Camera2D.limit_right))
		if ($Camera2D.position.x + rect_viewport.size.x * $Camera2D.zoom.x / 2.0) > $Camera2D.limit_right:
			$Camera2D.position.x = $Camera2D.limit_right - rect_viewport.size.x * $Camera2D.zoom.x / 2.0
		if ($Camera2D.position.x - rect_viewport.size.x * $Camera2D.zoom.x / 2.0) < $Camera2D.limit_left:
			$Camera2D.position.x = $Camera2D.limit_left + rect_viewport.size.x * $Camera2D.zoom.x / 2.0
		if ($Camera2D.position.y + rect_viewport.size.y * $Camera2D.zoom.x / 2.0) > $Camera2D.limit_bottom:
			$Camera2D.position.y = $Camera2D.limit_bottom - rect_viewport.size.y * $Camera2D.zoom.x / 2.0
		if ($Camera2D.position.y - rect_viewport.size.y * $Camera2D.zoom.x / 2.0) < $Camera2D.limit_top:
			$Camera2D.position.y = $Camera2D.limit_top + rect_viewport.size.y * $Camera2D.zoom.x / 2.0
		pass
	if Input.is_action_pressed("ui_up"):
		$Camera2D.position.y = ($Camera2D.position.y - 1000 * delta);
		
		#print("before" + str($Camera2D.position.x) + " " + str(rect_viewport.size.x / 2.0) + " " + str($Camera2D.limit_right))
		if ($Camera2D.position.x + rect_viewport.size.x * $Camera2D.zoom.x / 2.0) > $Camera2D.limit_right:
			$Camera2D.position.x = $Camera2D.limit_right - rect_viewport.size.x * $Camera2D.zoom.x / 2.0
		if ($Camera2D.position.x - rect_viewport.size.x * $Camera2D.zoom.x / 2.0) < $Camera2D.limit_left:
			$Camera2D.position.x = $Camera2D.limit_left + rect_viewport.size.x * $Camera2D.zoom.x / 2.0
		if ($Camera2D.position.y + rect_viewport.size.y * $Camera2D.zoom.x / 2.0) > $Camera2D.limit_bottom:
			$Camera2D.position.y = $Camera2D.limit_bottom - rect_viewport.size.y * $Camera2D.zoom.x / 2.0
		if ($Camera2D.position.y - rect_viewport.size.y * $Camera2D.zoom.x / 2.0) < $Camera2D.limit_top:
			$Camera2D.position.y = $Camera2D.limit_top + rect_viewport.size.y * $Camera2D.zoom.x / 2.0
		pass
	if Input.is_action_pressed("ui_down"):
		$Camera2D.position.y = ($Camera2D.position.y + 1000 * delta);
		
		#print("before" + str($Camera2D.position.x) + " " + str(rect_viewport.size.x / 2.0) + " " + str($Camera2D.limit_right))
		if ($Camera2D.position.x + rect_viewport.size.x * $Camera2D.zoom.x / 2.0) > $Camera2D.limit_right:
			$Camera2D.position.x = $Camera2D.limit_right - rect_viewport.size.x * $Camera2D.zoom.x / 2.0
		if ($Camera2D.position.x - rect_viewport.size.x * $Camera2D.zoom.x / 2.0) < $Camera2D.limit_left:
			$Camera2D.position.x = $Camera2D.limit_left + rect_viewport.size.x * $Camera2D.zoom.x / 2.0
		if ($Camera2D.position.y + rect_viewport.size.y * $Camera2D.zoom.x / 2.0) > $Camera2D.limit_bottom:
			$Camera2D.position.y = $Camera2D.limit_bottom - rect_viewport.size.y * $Camera2D.zoom.x / 2.0
		if ($Camera2D.position.y - rect_viewport.size.y * $Camera2D.zoom.x / 2.0) < $Camera2D.limit_top:
			$Camera2D.position.y = $Camera2D.limit_top + rect_viewport.size.y * $Camera2D.zoom.x / 2.0
		pass
	pass

func _unhandled_input(event):
	if event is InputEventMouseMotion:
		if _moveCamera:
			$Camera2D.position += (_previousPosition - event.position);
			_previousPosition = event.position;
			
			#print("before" + str($Camera2D.position.x) + " " + str(rect_viewport.size.x / 2.0) + " " + str($Camera2D.limit_right))
			if ($Camera2D.position.x + rect_viewport.size.x / 2.0) > $Camera2D.limit_right:
				$Camera2D.position.x = $Camera2D.limit_right - rect_viewport.size.x / 2.0
			if ($Camera2D.position.x - rect_viewport.size.x / 2.0) < $Camera2D.limit_left:
				$Camera2D.position.x = $Camera2D.limit_left + rect_viewport.size.x / 2.0
			if ($Camera2D.position.y + rect_viewport.size.y / 2.0) > $Camera2D.limit_bottom:
				$Camera2D.position.y = $Camera2D.limit_bottom - rect_viewport.size.y / 2.0
			if ($Camera2D.position.y - rect_viewport.size.y / 2.0) < $Camera2D.limit_top:
				$Camera2D.position.y = $Camera2D.limit_top + rect_viewport.size.y / 2.0
			#print("after" + str($Camera2D.position.x))
			pass
		pass
	if event is InputEventMouseButton:
		if event.is_pressed():
			# zoom in
			if event.button_index == BUTTON_WHEEL_UP:
				if $Camera2D.zoom.x > 0.5:
					$Camera2D.zoom = $Camera2D.zoom - Vector2(0.1,0.1)
					# call the zoom function
			# zoom out
			if event.button_index == BUTTON_WHEEL_DOWN:
				if $Camera2D.zoom.x < 2.0:
					$Camera2D.zoom = $Camera2D.zoom + Vector2(0.1,0.1)
					# call the zoom function
			if event.button_index == BUTTON_MIDDLE:
				_previousPosition = event.position;
				_moveCamera = true;
				pass
		else:
			_moveCamera = false;
	if event is InputEventKey:
		if event.is_pressed():
			if event.scancode == KEY_ESCAPE:
				$PauseMenu.set_pause()
				pass
			if event.scancode == KEY_F:
				$GUI/Control.visible = not $GUI/Control.visible
				$GUI/Gold.visible = not $GUI/Gold.visible
				$GUI/WaveLabel.visible = not $GUI/WaveLabel.visible
				$GUI/BackgroundBar.visible = not $GUI/BackgroundBar.visible
				#$GUI/PhaseAnnouncement.visible = not $GUI/PhaseAnnouncement.visible
				pass
			
	pass
	
func defeat(_target):
	print("You LOST the Moon control !")
	audio_manager.reset_music()
	$LoseGUI.set_pause()
	pass

func win():
	print("You WIN the Moon control !")
	audio_manager.reset_music()
	$WinGUI.set_pause()
	pass

func _on_TankSpawner_timeout():
	var counter = 0
	var line_to_spawn = false
	for tank_to_spawn in global.phase[global.current_phase]["tank1_number"]:
		if wave_tank1_counter < tank_to_spawn:
			# Spawn tank on this line
			line_to_spawn = true
			var tank = Tank1Class.instance()
			tank.patrol_path = global.phase[global.current_phase]["tank1_path"][counter]
			tank.position = Vector2(1850,1200)
			$GroundObject.add_child(tank)
			pass
		counter = counter + 1
		pass
	
	wave_tank1_counter = wave_tank1_counter + 1
	
	if $TankSpawner.wait_time != global.phase[global.current_phase]["tank1_timer"]:
		if global.phase[global.current_phase]["tank1_timer"] != 0:
			$TankSpawner.wait_time = global.phase[global.current_phase]["tank1_timer"]
			$TankSpawner.stop()
			$TankSpawner.start()
		else:
			$TankSpawner.stop()
		
	# AUTRE CONDITION D ARRET
	if not line_to_spawn:
		$TankSpawner.stop()
		wave_tank1_counter = 0
	pass # Replace with function body.


func _on_MeteorSpawner_timeout():
	if meteor_spawned == 0:
		$MeteorSpawner.wait_time = meteor_time

	var meteor = MeteorClass.instance()
	meteor.global_position = Vector2(rand_range(-1500,1500), rand_range(-1500,0))
	$GroundObject.add_child(meteor)
	meteor = MeteorClass.instance()
	
	meteor_spawned = meteor_spawned + 1
	if meteor_spawned == meteor_spawn_phase:
		$MeteorSpawner.wait_time = meteor_phase_time
		meteor_spawned = 0
	pass # Replace with function body.


func _on_GoldTimer_timeout():
	#global.gold_ressource = global.gold_ressource + 5
	EventBus.emit_signal("add_gold", 5)
	pass # Replace with function body.

func _on_PhaseTimer_timeout():
	
	
	if global.current_phase + 1 in global.phase:
		#print("Start next phase !")
		global.current_phase = global.current_phase + 1
		#print(global.phase[global.current_phase])
		
		if global.current_phase == (global.phase["last_wave"]+1):
			win()
			return
		
		$GUI/PhaseAnnouncement.text = global.phase[global.current_phase]["message"]
		$GUI/PhaseAnnouncement/AnimationPlayer.play("ShowNewPhase")
		
#		$GUI/PhaseAnnouncement2/TextInterfaceEngine.clear_text()
#		#$GUI/PhaseAnnouncement2/TextInterfaceEngine.buff_text(global.phase[global.current_phase]["message"], 0.04)
#		$GUI/PhaseAnnouncement2/TextInterfaceEngine.buff_text(tr(global.phase[global.current_phase]["message"]), 0.04)
#		$GUI/PhaseAnnouncement2/TextInterfaceEngine.set_state($GUI/PhaseAnnouncement2/TextInterfaceEngine.STATE_OUTPUT)
#		$GUI/PhaseAnnouncement2/Timer.start()
#		$GUI/PhaseAnnouncement2.visible = true
		
		
		if global.phase[global.current_phase]["enemy_direction_path"].size() != 0:
			#$EnemyDirectionTimer.wait_time = global.phase[global.current_phase]["enemy_direction_path"]
			current_direction_enemy = 0
			$EnemyDirectionTimer.start()
		
		$PhaseTimer.wait_time = global.phase[global.current_phase]["next_phase_wait"]
		$PhaseTimer.stop()
		
		$WaitStartTimer.wait_time = global.phase[global.current_phase]["start_wait_timer"]
		$WaitStartTimer.stop()
		$WaitStartTimer.start()
	else:
		print("Phase not found, stop")
	
	
	
	
#	if global.current_phase + 1 in global.phase:
#		#print("Start next phase !")
#		global.current_phase = global.current_phase + 1
#		#print(global.phase[global.current_phase])
#
#		if global.current_phase == 13:
#			win()
#			return
#
#		$GUI/PhaseAnnouncement.text = global.phase[global.current_phase]["message"]
#		$GUI/PhaseAnnouncement/AnimationPlayer.play("ShowNewPhase")
#
#		if global.phase[global.current_phase]["tank1_wait_time"] != 0:
#			$TankSpawner.wait_time = global.phase[global.current_phase]["tank1_wait_time"]
#			$TankSpawner.start()
#		else:
#			if global.phase[global.current_phase]["tank1_timer"] != 0:
#				$TankSpawner.wait_time = global.phase[global.current_phase]["tank1_timer"]
#				$TankSpawner.start()
#
#		if global.phase[global.current_phase]["tank2_wait_time"] != 0:
#			$Tank2Spawner.wait_time = global.phase[global.current_phase]["tank2_wait_time"]
#			$Tank2Spawner.start()
#		else:
#			if global.phase[global.current_phase]["tank2_timer"] != 0:
#				$Tank2Spawner.wait_time = global.phase[global.current_phase]["tank2_timer"]
#				$Tank2Spawner.start()
#
#		if global.phase[global.current_phase]["tank3_wait_time"] != 0:
#			$TankHealSpawner.wait_time = global.phase[global.current_phase]["tank3_wait_time"]
#			$TankHealSpawner.start()
#		else:
#			if global.phase[global.current_phase]["tank3_timer"] != 0:
#				$TankHealSpawner.wait_time = global.phase[global.current_phase]["tank3_timer"]
#				$TankHealSpawner.start()
#
#		if global.phase[global.current_phase]["tank4_wait_time"] != 0:
#			$Tank4Spawner.wait_time = global.phase[global.current_phase]["tank4_wait_time"]
#			$Tank4Spawner.start()
#		else:
#			if global.phase[global.current_phase]["tank4_timer"] != 0:
#				$Tank4Spawner.wait_time = global.phase[global.current_phase]["tank4_timer"]
#				$Tank4Spawner.start()
#
#		if global.phase[global.current_phase]["ship1_wait_time"] != 0:
#			$Ship1Spawner.wait_time = global.phase[global.current_phase]["ship1_wait_time"]
#			$Ship1Spawner.start()
#		else:
#			if global.phase[global.current_phase]["ship1_timer"] != 0:
#				$Ship1Spawner.wait_time = global.phase[global.current_phase]["ship1_timer"]
#				$Ship1Spawner.start()
#
#		if global.phase[global.current_phase]["boss1_wait_time"] != 0:
#			$Boss1Spawner.wait_time = global.phase[global.current_phase]["boss1_wait_time"]
#			$Boss1Spawner.start()
#		else:
#			if global.phase[global.current_phase]["boss1_timer"] != 0:
#				$Boss1Spawner.wait_time = global.phase[global.current_phase]["boss1_timer"]
#				$Boss1Spawner.start()
#
#		if global.phase[global.current_phase]["enemy_direction_path"].size() != 0:
#			#$EnemyDirectionTimer.wait_time = global.phase[global.current_phase]["enemy_direction_path"]
#			current_direction_enemy = 0
#			$EnemyDirectionTimer.start()
#
#		$PhaseTimer.wait_time = global.phase[global.current_phase]["next_phase_wait"]
#		$PhaseTimer.stop()
#		$PhaseTimer.start()
#	else:
#		print("Phase not found, stop")
	pass # Replace with function body.




func _on_Ship1Spawner_timeout():
	var counter = 0
	var line_to_spawn = false
	for ship_to_spawn in global.phase[global.current_phase]["ship1_number"]:
		if wave_ship1_counter < ship_to_spawn:
			# Spawn ship on this line
			line_to_spawn = true
			var ship = Ship1Class.instance()
			ship.global_position = global.phase[global.current_phase]["ship1_position"][counter]
			$FlyingObject.add_child(ship)
			pass
		counter = counter + 1
		pass
	
	wave_ship1_counter = wave_ship1_counter + 1
	
	if $Ship1Spawner.wait_time != global.phase[global.current_phase]["ship1_timer"]:
		$Ship1Spawner.wait_time = global.phase[global.current_phase]["ship1_timer"]
		$Ship1Spawner.stop()
		$Ship1Spawner.start()
	
	# AUTRE CONDITION D ARRET
	if not line_to_spawn:
		$Ship1Spawner.stop()
		wave_ship1_counter = 0
	pass # Replace with function body.



func _on_Tank2Spawner_timeout():
	var counter = 0
	var line_to_spawn = false
	for tank_to_spawn in global.phase[global.current_phase]["tank2_number"]:
		if wave_tank2_counter < tank_to_spawn:
			# Spawn tank on this line
			line_to_spawn = true
			var tank = Tank2Class.instance()
			tank.patrol_path = global.phase[global.current_phase]["tank2_path"][counter]
			tank.position = Vector2(1850,1200)
			$GroundObject.add_child(tank)
			pass
		counter = counter + 1
		pass
	
	wave_tank2_counter = wave_tank2_counter + 1
	
	if $Tank2Spawner.wait_time != global.phase[global.current_phase]["tank2_timer"]:
		$Tank2Spawner.wait_time = global.phase[global.current_phase]["tank2_timer"]
		$Tank2Spawner.stop()
		$Tank2Spawner.start()
		
	# AUTRE CONDITION D ARRET
	if not line_to_spawn:
		$Tank2Spawner.stop()
		wave_tank2_counter = 0
	pass # Replace with function body.


func _on_TankHealSpawner_timeout():
	var counter = 0
	var line_to_spawn = false
	for tank_to_spawn in global.phase[global.current_phase]["tank3_number"]:
		if wave_tankheal_counter < tank_to_spawn:
			# Spawn tank on this line
			line_to_spawn = true
			var tank = TankHealClass.instance()
			tank.patrol_path = global.phase[global.current_phase]["tank3_path"][counter]
			tank.position = Vector2(1850,1200)
			$GroundObject.add_child(tank)
			pass
		counter = counter + 1
		pass
	
	wave_tankheal_counter = wave_tankheal_counter + 1
	
	if $TankHealSpawner.wait_time != global.phase[global.current_phase]["tank3_timer"]:
		$TankHealSpawner.wait_time = global.phase[global.current_phase]["tank3_timer"]
		$TankHealSpawner.stop()
		$TankHealSpawner.start()
		
	# AUTRE CONDITION D ARRET
	if not line_to_spawn:
		$TankHealSpawner.stop()
		wave_tankheal_counter = 0
	pass # Replace with function body.

func _on_Tank4Spawner_timeout():
	var counter = 0
	var line_to_spawn = false
	for tank_to_spawn in global.phase[global.current_phase]["tank4_number"]:
		if wave_tank4_counter < tank_to_spawn:
			# Spawn tank on this line
			line_to_spawn = true
			var tank = Tank4Class.instance()
			tank.patrol_path = global.phase[global.current_phase]["tank4_path"][counter]
			tank.position = Vector2(1850,1200)
			$GroundObject.add_child(tank)
			pass
		counter = counter + 1
		pass
	
	wave_tank4_counter = wave_tank4_counter + 1
	
	if $Tank4Spawner.wait_time != global.phase[global.current_phase]["tank4_timer"]:
		$Tank4Spawner.wait_time = global.phase[global.current_phase]["tank4_timer"]
		$Tank4Spawner.stop()
		$Tank4Spawner.start()
		
	# AUTRE CONDITION D ARRET
	if not line_to_spawn:
		$Tank4Spawner.stop()
		wave_tank4_counter = 0
	pass # Replace with function body.


func _on_Boss1Spawner_timeout():
	var counter = 0
	var line_to_spawn = false
	for tank_to_spawn in global.phase[global.current_phase]["boss1_number"]:
		if wave_boss1_counter < tank_to_spawn:
			# Spawn tank on this line
			line_to_spawn = true
			var tank = Boss1Class.instance()
			tank.patrol_path = global.phase[global.current_phase]["boss1_path"][counter]
			tank.position = Vector2(1850,1200)
			$GroundObject.add_child(tank)
			pass
		counter = counter + 1
		pass
	
	wave_boss1_counter = wave_boss1_counter + 1
	
	if $Boss1Spawner.wait_time != global.phase[global.current_phase]["boss1_timer"]:
		$Boss1Spawner.wait_time = global.phase[global.current_phase]["boss1_timer"]
		$Boss1Spawner.stop()
		$Boss1Spawner.start()
		
	# AUTRE CONDITION D ARRET
	if not line_to_spawn:
		$Boss1Spawner.stop()
		wave_boss1_counter = 0
	pass # Replace with function body.  


func _on_TTC1_OkButton_pressed():
	$Tooltips/ToolTipCollector.visible = false
	pass # Replace with function body.


func _on_TTC2_OkButton_pressed():
	$Tooltips/ToolTipCollector2.visible = false
	pass # Replace with function body.


func _on_TTM_OkButton_pressed():
	$Tooltips/UIToolTip/ToolTipMiner.visible = false
	pass # Replace with function body.


func _on_TTUI_OkButton_pressed():
	$Tooltips/UIToolTip/ToolTipUI.visible = false
	pass # Replace with function body.


func _on_EnemyDirectionTimer_timeout():
	if current_direction_enemy < 3:
		var counter = 0
		for tank_to_spawn in global.phase[global.current_phase]["enemy_direction_path"]:
			var enemy_direction = EnemyDirectionClass.instance()
			enemy_direction.patrol_path = global.phase[global.current_phase]["enemy_direction_path"][counter]
			enemy_direction.position = Vector2(1850,1200)
			$GroundObject.add_child(enemy_direction)
			
			counter = counter + 1
		current_direction_enemy = current_direction_enemy + 1
	else:
		$EnemyDirectionTimer.stop()
	pass # Replace with function body.


func _on_WaitStartTimer_timeout() -> void:
	$WaitStartTimer.stop()
	#$GUI/PhaseAnnouncement2.visible = false
	
	if global.phase[global.current_phase]["tank1_wait_time"] != 0:
		$TankSpawner.wait_time = global.phase[global.current_phase]["tank1_wait_time"]
		$TankSpawner.start()
	else:
		if global.phase[global.current_phase]["tank1_timer"] != 0:
			$TankSpawner.wait_time = global.phase[global.current_phase]["tank1_timer"]
			$TankSpawner.start()
		
	if global.phase[global.current_phase]["tank2_wait_time"] != 0:
		$Tank2Spawner.wait_time = global.phase[global.current_phase]["tank2_wait_time"]
		$Tank2Spawner.start()
	else:
		if global.phase[global.current_phase]["tank2_timer"] != 0:
			$Tank2Spawner.wait_time = global.phase[global.current_phase]["tank2_timer"]
			$Tank2Spawner.start()
	
	if global.phase[global.current_phase]["tank3_wait_time"] != 0:
		$TankHealSpawner.wait_time = global.phase[global.current_phase]["tank3_wait_time"]
		$TankHealSpawner.start()
	else:
		if global.phase[global.current_phase]["tank3_timer"] != 0:
			$TankHealSpawner.wait_time = global.phase[global.current_phase]["tank3_timer"]
			$TankHealSpawner.start()
	
	if global.phase[global.current_phase]["tank4_wait_time"] != 0:
		$Tank4Spawner.wait_time = global.phase[global.current_phase]["tank4_wait_time"]
		$Tank4Spawner.start()
	else:
		if global.phase[global.current_phase]["tank4_timer"] != 0:
			$Tank4Spawner.wait_time = global.phase[global.current_phase]["tank4_timer"]
			$Tank4Spawner.start()
	
	if global.phase[global.current_phase]["ship1_wait_time"] != 0:
		$Ship1Spawner.wait_time = global.phase[global.current_phase]["ship1_wait_time"]
		$Ship1Spawner.start()
	else:
		if global.phase[global.current_phase]["ship1_timer"] != 0:
			$Ship1Spawner.wait_time = global.phase[global.current_phase]["ship1_timer"]
			$Ship1Spawner.start()
		
	if global.phase[global.current_phase]["ship2_wait_time"] != 0:
		$Ship2Spawner.wait_time = global.phase[global.current_phase]["ship2_wait_time"]
		$Ship2Spawner.start()
	else:
		if global.phase[global.current_phase]["ship2_timer"] != 0:
			$Ship2Spawner.wait_time = global.phase[global.current_phase]["ship2_timer"]
			$Ship2Spawner.start()
			
	if global.phase[global.current_phase]["boss1_wait_time"] != 0:
		$Boss1Spawner.wait_time = global.phase[global.current_phase]["boss1_wait_time"]
		$Boss1Spawner.start()
	else:
		if global.phase[global.current_phase]["boss1_timer"] != 0:
			$Boss1Spawner.wait_time = global.phase[global.current_phase]["boss1_timer"]
			$Boss1Spawner.start()
	
	$PhaseTimer.wait_time = global.phase[global.current_phase]["next_phase_wait"]
	$PhaseTimer.stop()
	$PhaseTimer.start()
	pass # Replace with function body.


func _on_Ship2Spawner_timeout() -> void:
	var counter = 0
	var line_to_spawn = false
	for ship_to_spawn in global.phase[global.current_phase]["ship2_number"]:
		if wave_ship2_counter < ship_to_spawn:
			# Spawn ship on this line
			line_to_spawn = true
			var ship = Ship2Class.instance()
			ship.global_position = global.phase[global.current_phase]["ship2_position"][counter]
			$FlyingObject.add_child(ship)
			pass
		counter = counter + 1
		pass
	
	wave_ship2_counter = wave_ship2_counter + 1
	
	if $Ship2Spawner.wait_time != global.phase[global.current_phase]["ship2_timer"]:
		$Ship2Spawner.wait_time = global.phase[global.current_phase]["ship2_timer"]
		$Ship2Spawner.stop()
		$Ship2Spawner.start()
	
	# AUTRE CONDITION D ARRET
	if not line_to_spawn:
		$Ship2Spawner.stop()
		wave_ship2_counter = 0
	pass # Replace with function body.
