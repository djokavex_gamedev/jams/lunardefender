extends CanvasLayer

func set_pause():
	var new_pause_state = not get_tree().paused
	get_tree().paused = new_pause_state
	$Rect.visible = new_pause_state

func _on_ExitButton_pressed():
	set_pause()
	audio_manager.play_sfx(load("res://assets/sfx/gui/question_003.ogg"), 0, 1)
	get_tree().change_scene("res://level/GUI/GeneralMenu.tscn")
	pass # Replace with function body.

