extends "res://level/Battlefield/BattlefieldTemplate.gd"

#var Tank2Class = preload("res://actor/Enemy/tank2/Tank2.tscn")
#var EnemyDirectionClass = preload("res://actor/Enemy/EnemyDirection.tscn")

var tutorial_step = 0
#var current_direction_enemy = 0
var tutorial_enemy_number = 0
var number_of_tower = 0
var enemy_dead = 0
var number_miner = 0
var number_upgrade = 0

func _ready():
	var enemy_direction = EnemyDirectionClass.instance()
	enemy_direction.patrol_path = "../../Path2D"
	enemy_direction.position = Vector2(1850,1200)
	$GroundObject.add_child(enemy_direction)
	
	EventBus.connect("dropped_tower", self, "dropped_tower")
	EventBus.connect("remove_enemy", self, "remove_enemy")
	EventBus.connect("new_miner", self, "new_miner")
	EventBus.connect("tower_upgraded", self, "tower_upgraded")
	
	$GUI_Tutorial/STEP0_1/TextInterfaceEngine.connect("buff_end", self, "enable_step0_2")
	$GUI_Tutorial/STEP0_1/TextInterfaceEngine.buff_text(tr("KEY_TUTORIAL_STEP0_1"), 0.08)
	$GUI_Tutorial/STEP0_1/TextInterfaceEngine.set_state($GUI_Tutorial/STEP0_1/TextInterfaceEngine.STATE_OUTPUT)

	pass
	
func _process(delta):
	match tutorial_step:
		0:
			if number_of_tower > 1:
				$GUI_Tutorial/STEP0_1.visible = false
				$GUI_Tutorial/STEP0_2.visible = false
				
				$GUI_Tutorial/STEP1_1.visible = true
				#$GUI_Tutorial/STEP1_1/TextInterfaceEngine.connect("buff_end", self, "enable_step1_2")
				$GUI_Tutorial/STEP1_1/TextInterfaceEngine.buff_text(tr("KEY_TUTORIAL_STEP1_1"), 0.05)
				$GUI_Tutorial/STEP1_1/TextInterfaceEngine.set_state($GUI_Tutorial/STEP1_1/TextInterfaceEngine.STATE_OUTPUT)
				
				var tank = Tank2Class.instance()
				tank.patrol_path = "../../Path2D"
				tank.position = Vector2(1850,1200)
				$GroundObject.add_child(tank)
				
				#$Area2D/ColorRect.visible = false
				$Area2D/Particles2D.visible = false
				tutorial_step = 1
			pass
		1:
			if enemy_dead > 0:
				#global.collector_unlocked = true
				$GUI_Tutorial/STEP1_1.visible = false
				
				global.gold_ressource = 50
				global.miner_unlocked = true
				EventBus.emit_signal("gold_amount_changed")
				$GUI_Tutorial/STEP2_1.visible = true
				$GUI_Tutorial/STEP2_1/TextInterfaceEngine.connect("buff_end", self, "enable_step2_2")
				$GUI_Tutorial/STEP2_1/TextInterfaceEngine.buff_text(tr("KEY_TUTORIAL_STEP2_1"), 0.07)
				$GUI_Tutorial/STEP2_1/TextInterfaceEngine.set_state($GUI_Tutorial/STEP2_1/TextInterfaceEngine.STATE_OUTPUT)
				#$GUI_Tutorial/STEP2_2.visible = true
				
				tutorial_step = 2
			pass
		2:
			if number_miner > 0:
				$FlyingObject/Miner/CollectingTimer.wait_time = 5
				$FlyingObject/Miner/CollectingTimer.stop()
				$FlyingObject/Miner/CollectingTimer.start()
				global.miner_unlocked = false
				$GUI_Tutorial/STEP2_1.visible = false
				$GUI_Tutorial/STEP2_2.visible = false
				
				$GUI_Tutorial/STEP3_1.visible = true
				$GUI_Tutorial/STEP3_1/TextInterfaceEngine.connect("buff_end", self, "enable_step3_2")
				$GUI_Tutorial/STEP3_1/TextInterfaceEngine.buff_text(tr("KEY_TUTORIAL_STEP3_1"), 0.07)
				$GUI_Tutorial/STEP3_1/TextInterfaceEngine.set_state($GUI_Tutorial/STEP3_1/TextInterfaceEngine.STATE_OUTPUT)
				
				#$GUI_Tutorial/STEP3_2.visible = true
				#$STEP3_3.visible = true
				tutorial_step = 3
			pass
		3:
			if global.gold_ressource > 70:
				
				$GUI_Tutorial/STEP3_1.visible = false
				$GUI_Tutorial/STEP3_2.visible = false
				$STEP3_3.visible = false
				
				$EnemyTimer.start()
				$STEP4_1.visible = true
				tutorial_step = 4
			pass
		4:
			if enemy_dead > 12:
				$STEP4_1.visible = false
				$GUI_Tutorial/STEP5_1.visible = true
				$GUI_Tutorial/STEP5_1/TextInterfaceEngine.connect("buff_end", self, "enable_step5_2")
				$GUI_Tutorial/STEP5_1/TextInterfaceEngine.buff_text(tr("KEY_TUTORIAL_STEP5_1"), 0.07) # 
				$GUI_Tutorial/STEP5_1/TextInterfaceEngine.set_state($GUI_Tutorial/STEP5_1/TextInterfaceEngine.STATE_OUTPUT)
				
				tutorial_step = 5
				pass
			pass
		5:
			if number_upgrade > 1:
				tutorial_enemy_number = 0
				$GUI_Tutorial/STEP5_1.visible = false
				$STEP5_2.visible = false
				$EnemyTimer.start()
				
				tutorial_step = 6
			pass
		6:
			#print(enemy_dead)
			if enemy_dead > 20:
				#print("DONE")
				tutorial_step = 7
				win()
			pass
		7:
			pass
	pass
	
func enable_step0_2():
	$GUI_Tutorial/STEP0_2.visible = true
	#$GUI_Tutorial/STEP0_2/TextInterfaceEngine.buff_silence(8)
	$GUI_Tutorial/STEP0_2/TextInterfaceEngine.buff_text(tr("KEY_TUTORIAL_STEP0_2"), 0.07)
	$GUI_Tutorial/STEP0_2/TextInterfaceEngine.set_state($GUI_Tutorial/STEP0_2/TextInterfaceEngine.STATE_OUTPUT)
	
	#$Area2D/ColorRect.visible = true
	$Area2D/Particles2D.visible = true
	pass

func enable_step2_2():
	$GUI_Tutorial/STEP2_2.visible = true
	#$GUI_Tutorial/STEP0_2/TextInterfaceEngine.buff_silence(8)
	$GUI_Tutorial/STEP2_2/TextInterfaceEngine.buff_text(tr("KEY_TUTORIAL_STEP2_2"), 0.07)
	$GUI_Tutorial/STEP2_2/TextInterfaceEngine.set_state($GUI_Tutorial/STEP2_2/TextInterfaceEngine.STATE_OUTPUT)
	pass

func enable_step3_2():
	$GUI_Tutorial/STEP3_2.visible = true
	#$GUI_Tutorial/STEP0_2/TextInterfaceEngine.buff_silence(8)
	$GUI_Tutorial/STEP3_2/TextInterfaceEngine.connect("buff_end", self, "enable_step3_3")
	$GUI_Tutorial/STEP3_2/TextInterfaceEngine.buff_text(tr("KEY_TUTORIAL_STEP3_2"), 0.07)
	$GUI_Tutorial/STEP3_2/TextInterfaceEngine.set_state($GUI_Tutorial/STEP3_2/TextInterfaceEngine.STATE_OUTPUT)
	pass
	
func enable_step3_3():
	$STEP3_3.visible = true
	#$GUI_Tutorial/STEP0_2/TextInterfaceEngine.buff_silence(8)
	$STEP3_3/TextInterfaceEngine.buff_text(tr("KEY_TUTORIAL_STEP3_3"), 0.07)
	$STEP3_3/TextInterfaceEngine.set_state($STEP3_3/TextInterfaceEngine.STATE_OUTPUT)
	pass
	
func enable_step5_2():
	$STEP5_2.visible = true
	$STEP5_2/TextInterfaceEngine.buff_text(tr("KEY_TUTORIAL_STEP5_2"), 0.07) #
	$STEP5_2/TextInterfaceEngine.set_state($STEP5_2/TextInterfaceEngine.STATE_OUTPUT)
	pass
	
func dropped_tower():
	number_of_tower = number_of_tower + 1
	pass

func remove_enemy(enemy):
	enemy_dead = enemy_dead + 1
	pass
	
func new_miner():
	number_miner = number_miner + 1
	pass

func tower_upgraded():
	number_upgrade = number_upgrade + 1
	pass

func win():
	#print("You WIN the Moon control !")
	audio_manager.reset_music()
	$WinGUI.set_pause()
	pass

func _on_EnemyDirectionTimer_timeout():
	if current_direction_enemy < 2:
		var enemy_direction = EnemyDirectionClass.instance()
		enemy_direction.patrol_path = "../../Path2D"
		enemy_direction.position = Vector2(1850,1200)
		enemy_direction.duration = 10000
		$GroundObject.add_child(enemy_direction)
		current_direction_enemy = current_direction_enemy + 1
	else:
		$EnemyDirectionTimer.stop()
	
	pass # Replace with function body.


func _on_EnemyTimer_timeout():
	if tutorial_enemy_number < 12 and tutorial_step == 4:
		var tank = Tank2Class.instance()
		tank.patrol_path = "../../Path2D"
		tank.position = Vector2(1850,1200)
		$GroundObject.add_child(tank)
		tutorial_enemy_number = tutorial_enemy_number + 1
	elif tutorial_enemy_number < 8 and tutorial_step == 6:
		var tank = Tank2Class.instance()
		tank.patrol_path = "../../Path2D"
		tank.position = Vector2(1850,1200)
		$GroundObject.add_child(tank)
		tutorial_enemy_number = tutorial_enemy_number + 1
		pass
	else:
		$EnemyTimer.stop()
	pass # Replace with function body.
