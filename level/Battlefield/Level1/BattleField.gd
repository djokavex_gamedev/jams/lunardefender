extends "res://level/Battlefield/BattlefieldTemplate.gd"

var current_animation = "stop"

func rand_wind_time():
	$WindTimer.stop()
	$WindTimer.wait_time = rand_range(15, 30)
	$WindTimer.start()
	pass

func _on_WindTimer_timeout():
	if current_animation == "start":
		$WindAnimationPlayer.play("stop")
		current_animation = "stop"
	else:
		$WindAnimationPlayer.play("start")
		current_animation = "start"
	pass # Replace with function body.
