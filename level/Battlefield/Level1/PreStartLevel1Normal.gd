extends Control

func _ready() -> void:
	$TextInterfaceEngine.buff_text(tr("KEY_LEVEL1_NORMAL_DESCRIPTION"), 0.03)
	$TextInterfaceEngine.set_state($TextInterfaceEngine.STATE_OUTPUT)
	pass


func _on_ContinueButton_pressed() -> void:
	audio_manager.play_sfx(load("res://assets/sfx/gui/BipOK.wav"), 0, 1)
	get_tree().change_scene("res://level/Battlefield/Level1/BattleField.tscn")
	pass # Replace with function body.


func _on_BackButton_pressed() -> void:
	audio_manager.play_sfx(load("res://assets/sfx/gui/bipBack.wav"), 0, 1)
	get_tree().change_scene("res://level/GUI/SelectLevelWindow.tscn")
	pass # Replace with function body.
