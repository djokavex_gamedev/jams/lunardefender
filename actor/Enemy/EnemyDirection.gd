extends "res://actor/Enemy/EnemyActor.gd"

# Export
export (NodePath) var patrol_path = ""

# Signal

# Variables
var LaserClass = preload("res://actor/Enemy/tank1/Laser.tscn")
#var TrackClass = preload("res://effects/TrackParticle/TrackParticle.tscn")
#var TrackClass = preload("res://effects/TrackParticle/TrackParticleSprite.tscn")
var TrackClass = preload("res://effects/TrackParticle/TrackLine.tscn")
var move_speed = 250
var patrol_points
var patrol_index = 0
var current_patrol_path = 0

var duration = 2
var counter = 0

# Called when the node enters the scene tree for the first time.
func specific_ready():
	if patrol_path:
		patrol_points = get_node(patrol_path).curve.get_baked_points()
		position = patrol_points[0]
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func specific_process(delta):
#	counter = counter + 1 
#	if counter > 12:
#		counter = 0
	if health > 0:
		# Moving
		if !patrol_path:
			return
		var target = patrol_points[patrol_index]
		if position.distance_to(target) < 50:
			patrol_index = wrapi(patrol_index + 1, 0, patrol_points.size())
			target = patrol_points[patrol_index]
			if current_patrol_path == 0 and patrol_index == (patrol_points.size() - 1):
				patrol_index = 0
				#patrol_points = get_node("../../CircularPath").curve.get_baked_points()
				#current_patrol_path = 1
				position = patrol_points[0]
				counter = counter + 1
				if counter > duration:
					queue_free()
		var velocity = (target - position).normalized() * move_speed
		global_rotation = velocity.angle()
		#current_velocity = velocity * delta
		current_velocity = lerp(current_velocity, velocity * delta, 0.7) # 
		global_position = global_position + current_velocity
		#current_velocity = move_and_slide(current_velocity)
		

		
	else:
		pass
	pass
