extends KinematicBody2D

# Exports

export(global.enemy_type) var type = global.enemy_type.ENEMY_TANK1
export var enemy_name = "tank1"

# Signals
signal died(enemy)

# Variables
var explosion = preload("res://effects/SmallExplosion/SmallExplosion.tscn")
var harvestable = preload("res://level/Battlefield/HarvestableRessource.tscn")
var floating_text = preload("res://assets/other/FloatingText.tscn")
var bonus_regen = preload("res://actor/Bonus/RegenLife/RegenLife.tscn")
var bonus_meteor = preload("res://actor/Bonus/Meteor/Meteor.tscn")
var bonus_large_meteor = preload("res://actor/Bonus/LargeMeteor/LargeMeteor.tscn")
var bonus_inc_damage = preload("res://actor/Bonus/IncDamage/IncDamage.tscn")
var bonus_inc_defense = preload("res://actor/Bonus/IncDefence/IncDefence.tscn")
var starting_health = 100
var health
var tank_damage = 5
var rate = 1.0
var current_velocity = Vector2(0,0)
var target_list = []

# Called when the node enters the scene tree for the first time.
func _ready():
	$TrackParticles1.process_material = $TrackParticles1.process_material.duplicate()
	$TrackParticles2.process_material = $TrackParticles2.process_material.duplicate()
	EventBus.emit_signal("add_enemy", type)
	set_physics_process(false)
	#set_process(false)
	add_to_group("enemy")
	var boosted_hp = 1.0 if global.current_difficulty == "normal" else global.difficulty_boost[global.boost_stat_difficulty.HARD_BOOST_ENEMY_HP]
	var boosted_damage = 1.0 if global.current_difficulty == "normal" else global.difficulty_boost[global.boost_stat_difficulty.HARD_BOOST_ENEMY_DAMAGE]
	starting_health = global.enemies[enemy_name]["health"] * boosted_hp
	tank_damage = global.enemies[enemy_name]["damage"] * boosted_damage
	rate = global.enemies[enemy_name]["fire_rate"]
	health = starting_health
	$GUI/LifeBar.max_value = starting_health
	$GUI/LifeBar.value = health
	$ShotTimer.wait_time = 1.0 / rate
	
	yield(get_tree().create_timer(rand_range(0.05,0.4)), "timeout")
	$ShotTimer.start()
	specific_ready()
	set_physics_process(true)
	#set_process(true)
	pass # Replace with function body.

func specific_ready():
	pass
	
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
func _physics_process(delta):
	specific_process(delta)
	$GUI.global_rotation = 0
	#$TrackParticles1.process_material.angle = - global_rotation_degrees - 90
	#$TrackParticles2.process_material.angle = - global_rotation_degrees - 90
	pass
	
func specific_process(_delta):
	pass

func modify_health(value):
	if health > 0:
		health = health + value
		if health >= (starting_health - 5):
			$GUI.visible = false
		else:
			$GUI.visible = true
		if health > starting_health:
			health = starting_health
		$GUI/LifeBar.value = health
		if value < 0:
			# For debug
#			var ftext = floating_text.instance()
#			ftext.value = -value
#			ftext.global_position = global_position
#			#ftext.color = Color("ffffff")
#			$World.add_child(ftext)
		
			take_hit_visibility()
			pass
		if health < (starting_health / 3.0):
			$InFire.emitting = true
		else:
			$InFire.emitting = false
		if health <= 0:
			$InFire.emitting = false
			emit_signal("died", self)
			EventBus.emit_signal("shake_camera", 0.1)
			destroy_instance()
	pass
	
func take_hit_visibility():
	$Tween.interpolate_property(self, "modulate", Color(1, 1, 1, 1), Color(0, 0, 0, 1), 0.05, Tween.TRANS_LINEAR, Tween.EASE_IN)
	$Tween.interpolate_property(self, "modulate", Color(0, 0, 0, 1), Color(1, 1, 1, 1), 0.05, Tween.TRANS_LINEAR, Tween.EASE_IN, 0.05)
	$Tween.start()
	pass

func destroy_instance():
	EventBus.emit_signal("remove_enemy", type)
	EventBus.emit_signal("add_points", global.action_points["enemy_kill"])
	global.player_stats["enemy_kill"] += 1
	
	$ShotTimer.stop()
	collision_layer = 0
	#$CollisionShape2D.disabled = true
	var explo = explosion.instance()
	explo.global_position = global_position
	get_parent().add_child(explo)
	explo.get_node("AnimationPlayer").play("explosion")
	
	randomize()
	if rand_range(0,100) <= 5:
		call_deferred("drop_bonus")
	
	randomize()
	if rand_range(0,100) <= 10:
		call_deferred("drop_gold")
	
	$AnimationPlayer.play("destroy")
	yield($AnimationPlayer,"animation_finished")
	queue_free()
	pass

func drop_gold():
	var new_gold = harvestable.instance()
	new_gold.global_position = global_position
	new_gold.value = 20
	get_tree().get_root().get_node_or_null("BattleField/GroundObject").add_child_below_node(get_tree().get_root().get_node_or_null("BattleField/GroundObject"), new_gold)

func drop_bonus():
	var new_bonus
	var random_number = randi() % 5
	match random_number:
		0:
			new_bonus = bonus_regen.instance()
			pass
		1:
			new_bonus = bonus_meteor.instance()
			pass
		2:
			new_bonus = bonus_large_meteor.instance()
			pass
		3:
			new_bonus = bonus_inc_damage.instance()
			pass
		4:
			new_bonus = bonus_inc_defense.instance()
			pass
	new_bonus.global_position = global_position
	get_tree().get_root().get_node_or_null("BattleField/GroundObject").add_child(new_bonus)
	pass
	
func _on_EnemyDetection_area_entered(area):
	if area.is_in_group("tower_building") and area.get_parent().actor_enabled:
		var is_present = target_list.find(area)
		if is_present != -1:
			pass
		else:
			target_list.push_back(area)
			area.get_parent().connect("died", self, "on_enemy_died")
	pass # Replace with function body.

func _on_EnemyDetection_area_exited(area):
	#print(area)
	if area.is_in_group("tower_building") and area.get_parent().actor_enabled:
		var is_present = target_list.find(area)
		if is_present != -1:
			area.get_parent().disconnect("died", self, "on_enemy_died")
			target_list.remove(is_present)
#			if target_list.size() == 0:
#				$AnimationPlayer.play("idle")
	pass # Replace with function body.



func _on_EnemyDetection_body_entered(body):
	if body.is_in_group("swarm"):
		var is_present = target_list.find(body)
		if is_present != -1:
			pass
		else:
			target_list.push_back(body)
			body.connect("died", self, "on_enemy_died")
	pass # Replace with function body.

func _on_EnemyDetection_body_exited(body):
	if body.is_in_group("swarm"):
		var is_present = target_list.find(body)
		if is_present != -1:
			body.disconnect("died", self, "on_enemy_died")
			target_list.remove(is_present)
	pass # Replace with function body.

func on_enemy_died(enemy):
	var is_present = target_list.find(enemy)
	if is_present != -1:
		target_list.remove(is_present)
#		if target_list.size() == 0:
#			$AnimationPlayer.play("idle")
	pass


func _on_ShotTimer_timeout():
	pass # Replace with function body.
