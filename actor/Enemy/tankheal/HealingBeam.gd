extends Line2D

var healing_amount = 4
var target = null
var boost_damage = false
var boost_defense = false

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	global_rotation = 0
	#if target != null:
	if is_instance_valid(target):
		points[1] = to_local(target.global_position)
		$Particles2D.position = points[1]
		
		$ColorRect.rect_rotation = 180 + Vector2(0,0).angle_to_point(points[1]) * 180 / 3.14159
		$ColorRect.rect_size = Vector2(Vector2(0, 0).distance_to(points[1]), 32)
	pass


func _on_HealingTimer_timeout():
	#if target != null:
	if is_instance_valid(target):
		target.modify_health(healing_amount)
		if boost_damage:
			target.on_bonus_received(global.ressource_type.RESSOURCE_INC_DAMAGE)
		if boost_defense:
			target.on_bonus_received(global.ressource_type.RESSOURCE_INC_DEFENCE)
	pass # Replace with function body.
