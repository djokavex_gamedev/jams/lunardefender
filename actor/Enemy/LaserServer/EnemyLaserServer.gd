class_name EnemyLaserServer
extends Node2D

export (float) var max_lifetime = 0.1

onready var shared_area := get_node("SharedArea") as Area2D

var bullets : Array = []

# ================================ Lifecycle ================================ #
func _ready() -> void:
	#EventBus.connect("spawn_laser", self, "spawn_bullet")
	pass

func _exit_tree() -> void:
	for bullet in bullets:
		Physics2DServer.free_rid((bullet as LaserStats).shape_id)
		pass
	bullets.clear()

func _physics_process(delta: float) -> void:
	var start = OS.get_ticks_msec()
	var used_transform = Transform2D()
	var bullets_queued_for_destruction = []
	
	#print(str(OS.get_datetime()) + str(bullets.size()))
	for i in range(0, bullets.size()):
		
		var bullet = bullets[i] as LaserStats
		
		if bullet.lifetime >= max_lifetime:
			bullets_queued_for_destruction.append(bullet)
			continue

		var offset : Vector2 = (
			bullet.movement_vector * bullet.speed * delta
		)
		
		# Move the bullet and the collision
		bullet.current_position += offset
		used_transform = Transform2D(bullet.angle, bullet.current_position)
		#used_transform.origin = bullet.current_position
		Physics2DServer.area_set_shape_transform(
			shared_area.get_rid(), i, used_transform
		)
		
		bullet.lifetime += delta
	
	#print("a "+str(OS.get_ticks_msec() - start)+" "+str(delta) )
	
	#print(str(bullets_queued_for_destruction.size()))
	for bullet in bullets_queued_for_destruction:
		Physics2DServer.free_rid((bullet as LaserStats).shape_id)
		bullets.erase(bullet)
	
	update()
	
	#print("b "+str(OS.get_ticks_msec() - start))

func _draw() -> void:
	for i in range(0, bullets.size()):
		var bullet = bullets[i]
		draw_line(bullet.current_position-bullet.movement_vector*bullet.size.x, bullet.current_position+bullet.movement_vector*bullet.size.x, bullet.color, bullet.size.y, true)

# ================================= Public ================================== #


# Register a new bullet in the array with the optimization logic
func spawn_bullet(i_movement: Vector2, start_position: Vector2, i_size: Vector2, i_damage: float, i_color: Color, speed: float) -> void:
	#print("spawn new laser")
	var bullet : LaserStats = LaserStats.new()
	bullet.movement_vector = i_movement
	bullet.speed = speed
	bullet.current_position  = start_position
	bullet.size = i_size
	bullet.color = i_color
	bullet.damage = i_damage
	
	_configure_collision_for_bullet(bullet)
	
	bullets.append(bullet)
	
# Adds the collision data to the bullet
func _configure_collision_for_bullet(bullet: LaserStats) -> void:
	# Define the shape's position
	var used_transform := Transform2D(bullet.angle, position) # TODO: SET CORRECT ORIENTATION
	used_transform.origin = bullet.current_position
	  
	# Create the shape
	var _rectangle_shape = Physics2DServer.rectangle_shape_create()
	Physics2DServer.shape_set_data(_rectangle_shape, bullet.size)

	# Add the shape to the shared area
	Physics2DServer.area_add_shape(
		shared_area.get_rid(), _rectangle_shape, used_transform
	)
	
	# Register the generated id to the bullet
	bullet.shape_id = _rectangle_shape


#func _on_SharedArea_area_shape_entered(area_id, area, area_shape, self_shape):
#	if area.is_in_group("tower_building"):
#		area.get_parent().modify_health(-bullets[self_shape].damage)
#		Physics2DServer.area_remove_shape(shared_area.get_rid(), self_shape)
#		bullets.remove(self_shape)
#	pass # Replace with function body.
#
#
#func _on_SharedArea_body_shape_entered(body_id, body, body_shape, area_shape):
#	print("test1 "+str(body_id))
#	print("test2 "+str(body))
#	print("test3 "+str(body_shape))
#	print("test4 "+str(area_shape))
##	if body.is_in_group("swarm"):
##		body.modify_health(-bullets[area_shape].damage)
##		Physics2DServer.area_remove_shape(shared_area.get_rid(), self_shape)
##		bullets.remove(self_shape)
#	pass # Replace with function body.
