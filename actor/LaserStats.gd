class_name LaserStats
extends Reference

var shape_id : RID
var movement_vector : Vector2
var angle : float
var current_position : Vector2
var lifetime : float = 0.0
var speed : float
var size : Vector2
var color : Color
var damage : float
