extends "res://actor/Tower/TowerActor.gd"

# Called when the node enters the scene tree for the first time.
func _ready():
	SwarmActorClass = preload("res://actor/Swarm/SmallLaserSwarm/SmallLaserSwarm.tscn")

	if global.unlockable_skills["tree1"]["step2"]:
		swarm_damage = swarm_damage + global.unlockable_skills["tree1"]["step2_value"]
		pass
	if global.unlockable_skills["tree1"]["step3"]:
		swarm_fire_rate = swarm_fire_rate + global.unlockable_skills["tree1"]["step3_value"]
		pass
	if global.unlockable_skills["tree1"]["step4"]:
		max_swarm_number = max_swarm_number + global.unlockable_skills["tree1"]["step4_value"]
		swarm_inside = max_swarm_number
		$GUI/SwarmBar.max_value = max_swarm_number
		$GUI/SwarmBar.value = swarm_outside + swarm_inside
		update_swarm_bar()
		pass
	pass # Replace with function body.
