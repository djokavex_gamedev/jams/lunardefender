extends "res://actor/Tower/TowerActor.gd"

# Called when the node enters the scene tree for the first time.
func _ready():
	SwarmActorClass = preload("res://actor/Swarm/ThunderSwarm/ThunderSwarm.tscn")
	
	if global.unlockable_skills["tree3"]["step2"]:
		swarm_damage = swarm_damage + global.unlockable_skills["tree3"]["step2_value"]
		pass
	if global.unlockable_skills["tree3"]["step3"]:
		bouncing_damage_reduction = bouncing_damage_reduction + global.unlockable_skills["tree3"]["step3_value"]
		pass
	if global.unlockable_skills["tree3"]["step4"]:
		swarm_fire_rate = swarm_fire_rate + global.unlockable_skills["tree3"]["step4_value"]
		pass
	pass # Replace with function body.

func summon_swarm():
	#$NextSwarmTimer.stop()
	if tower_state == TOWER_OPENED and swarm_inside > 0 and enemy_collision_list.size() > 0:
		var swarm = SwarmActorClass.instance()
		swarm.global_position = global_position
		swarm.parent_tower = self
		swarm.target = enemy_collision_list[0]
		swarm.starting_health = swarm_health
		swarm.starting_rate = swarm_fire_rate
		swarm.starting_damage = swarm_damage
		swarm.bouncing_number = bouncing_number
		swarm.bouncing_damage_reduction = bouncing_damage_reduction
		swarm.id = swarm_outside % 2
		var _result = connect("change_target", swarm, "change_target")
		_result = swarm.connect("landed", self, "swarm_landed")
		_result = connect("died", swarm, "parent_died")
		_result = swarm.connect("died", self, "on_swarm_died")
		get_node("../../FlyingObject").add_child(swarm)
		swarm_outside = swarm_outside + 1
		swarm_inside = swarm_inside - 1
		if enemy_collision_list.size() == 0:
			emit_signal("change_target", null)
			#$AnimationPlayer.play("idle")
			if swarm_outside <= 0:
				close_tower()
		else:
			emit_signal("change_target", enemy_collision_list[0])
		if swarm_inside > 0:
			$NextSwarmTimer.start()
	else:
		if swarm_outside <= 0:
			close_tower()
		pass
	update_buttons()
	update_swarm_bar()
	pass
