extends Area2D

# Export
export var tower_size = 100
export(global.tower_type) var type = global.tower_type.TOWER_LASER
export var tower_name = "small"
export(global.tower_strategy) var strategy_type = global.tower_strategy.STRATEGY_FIRST_SEEN

# Signals 
signal died(target)
signal change_target(target)

# Variables
var starting_health = 100
var health = 0.0
var max_swarm_number = 2
var swarm_outside = 0
var swarm_inside = 0
var picked = false
var actor_enabled = false 
var tower_collision_list = []
var enemy_collision_list =  []
var SwarmActorClass = preload("res://actor/Swarm/SwarmActor.tscn")
#var SwarmActorClass = preload("res://actor/Swarm/BomberSwarm/BomberSwarm.tscn")
var explosion = preload("res://effects/BigExplosion/BigExplosion.tscn")
var harvestable = preload("res://level/Battlefield/HarvestableRessource.tscn")
var floating_text = preload("res://assets/other/FloatingText.tscn")
var tower_price = 0

var swarm_price = 0

class UpgradeClass:
	var type = 0
	var number = 0
	var price = 0
	var max_number = 0
	pass

var upgrade1 = UpgradeClass.new()
var upgrade2 = UpgradeClass.new()
var upgrade3 = UpgradeClass.new()
var mega_upgrade = UpgradeClass.new()


var swarm_health = 75
var swarm_damage = 5
var swarm_fire_rate = 1.0
var bouncing_number = 2
var bouncing_damage_reduction = 6.0 / 10.0
var increase_target_number = 0
var speed_boost = 1.0
var turn_speed_boost = 1.0
var pass_through = false
var boost_damage = false
var boost_defense = false
var ground_in_fire = false

var increased_defense = false

enum {
	TOWER_CLOSED,
	TOWER_OPENING,
	TOWER_OPENED,
	TOWER_CLOSING
}
var tower_state = TOWER_CLOSED


# Called when the node enters the scene tree for the first time.
func _ready():
	health = global.towers[tower_name]["health"]
	starting_health = global.towers[tower_name]["health"]
	max_swarm_number = global.towers[tower_name]["swarm_number"]
	swarm_inside = max_swarm_number
	$GUI/LifeBar.max_value = starting_health
	$GUI/LifeBar.value = health
	#$GUI/SwarmBar.max_value = max_swarm_number
	#$GUI/SwarmBar.value = swarm_outside + swarm_inside
	update_swarm_bar()
	add_to_group("tower")
	$AnimationPlayer.play("idle")
	var _result = EventBus.connect("gold_amount_changed", self, "updated_gold_amount")
	_result = EventBus.connect("super_gold_amount_changed", self, "updated_gold_amount")
	tower_price = global.towers[tower_name]["price"]
	swarm_price = global.towers[tower_name]["swarm_price"]
	
	swarm_health = global.towers[tower_name]["swarm_health"]
	swarm_damage = global.towers[tower_name]["swarm_damage"]
	swarm_fire_rate = global.towers[tower_name]["swarm_fire_rate"]
	
	initialize_upgrade()
	check_strategy_button()
	
	pass # Replace with function body.

func initialize_upgrade():
	upgrade1.price = global.towers[tower_name]["upgrade1_price"]
	upgrade1.max_number = global.towers[tower_name]["upgrade1_number"]
	upgrade2.price = global.towers[tower_name]["upgrade2_price"]
	upgrade2.max_number = global.towers[tower_name]["upgrade2_number"]
	upgrade3.price = global.towers[tower_name]["upgrade3_price"]
	upgrade3.max_number = global.towers[tower_name]["upgrade3_number"]
	mega_upgrade.price = 1
	mega_upgrade.max_number = 1
	mega_upgrade.type = 1
	
	$GeneralGUI/Control/AddSwarmButton/Label.text = str(swarm_price)
	
	$GeneralGUI/Control/Upgrade1Button/Label.text = str(upgrade1.price)
	$GeneralGUI/Control/Upgrade1Button.icon = load(global.towers[tower_name]["upgrade1_img_path"])
	match global.towers[tower_name]["upgrade1_type"]:
		global.upgrade_type.UPGRADE_INC_DRONE_NUMBER:
			$GeneralGUI/Control/Upgrade1Button.connect("pressed", self, "_on_IncreaseMaxSwarmButton_pressed", [global.towers[tower_name]["upgrade1_value"], upgrade1])
			pass
		global.upgrade_type.UPGRADE_INC_HEALTH:
			$GeneralGUI/Control/Upgrade1Button.connect("pressed", self, "_on_IncreaseHealthButton_pressed", [global.towers[tower_name]["upgrade1_value"], upgrade1])
			pass
		global.upgrade_type.UPGRADE_INC_BOUNCE:
			$GeneralGUI/Control/Upgrade1Button.connect("pressed", self, "_on_IncreaseBounceButton_pressed", [global.towers[tower_name]["upgrade1_value"], upgrade1])
			pass
		global.upgrade_type.UPGRADE_INC_SPEED:
			$GeneralGUI/Control/Upgrade1Button.connect("pressed", self, "_on_IncreaseSpeedButton_pressed", [global.towers[tower_name]["upgrade1_value"], upgrade1])
			pass
		global.upgrade_type.UPGRADE_INC_TARGET:
			$GeneralGUI/Control/Upgrade1Button.connect("pressed", self, "_on_IncreaseTargetButton_pressed", [global.towers[tower_name]["upgrade1_value"], upgrade1])
			pass
		global.upgrade_type.UPGRADE_INC_ATK_AREA:
			$GeneralGUI/Control/Upgrade1Button.connect("pressed", self, "_on_IncreaseATKAreaButton_pressed", [global.towers[tower_name]["upgrade1_value"], upgrade1])
			pass
		global.upgrade_type.UPGRADE_INC_AREA:
			$GeneralGUI/Control/Upgrade1Button.connect("pressed", self, "_on_IncreaseAreaButton_pressed", [global.towers[tower_name]["upgrade1_value"], upgrade1])
			pass
		global.upgrade_type.UPGRADE_INC_DMG:
			$GeneralGUI/Control/Upgrade1Button.connect("pressed", self, "_on_IncreaseDamageButton_pressed", [global.towers[tower_name]["upgrade1_value"], upgrade1])
			pass
		global.upgrade_type.UPGRADE_INC_BOUNCE_DMG:
			$GeneralGUI/Control/Upgrade1Button.connect("pressed", self, "_on_IncreaseBounceDamageButton_pressed", [global.towers[tower_name]["upgrade1_value"], upgrade1])
			pass
		global.upgrade_type.UPGRADE_INC_BOUNCE_AND_DMG:
			$GeneralGUI/Control/Upgrade1Button.connect("pressed", self, "_on_IncreaseBounceAndDamageButton_pressed", [global.towers[tower_name]["upgrade1_value"], upgrade1])
			pass
		global.upgrade_type.UPGRADE_GROUND_FIRE:
			$GeneralGUI/Control/Upgrade1Button.connect("pressed", self, "_on_AddGroundFireButton_pressed", [global.towers[tower_name]["upgrade1_value"], upgrade1])
			pass
		global.upgrade_type.UPGRADE_ADD_BOOST_DMG:
			$GeneralGUI/Control/Upgrade1Button.connect("pressed", self, "_on_AddBoostDamageButton_pressed", [global.towers[tower_name]["upgrade1_value"], upgrade1])
			pass
		global.upgrade_type.UPGRADE_PASS_THROUGH:
			$GeneralGUI/Control/Upgrade1Button.connect("pressed", self, "_on_PassThroughButton_pressed", [global.towers[tower_name]["upgrade1_value"], upgrade1])
			pass
		_:
			pass
	
	$GeneralGUI/Control/Upgrade2Button/Label.text = str(upgrade2.price)
	$GeneralGUI/Control/Upgrade2Button.icon = load(global.towers[tower_name]["upgrade2_img_path"])
	match global.towers[tower_name]["upgrade2_type"]:
		global.upgrade_type.UPGRADE_INC_DRONE_NUMBER:
			$GeneralGUI/Control/Upgrade2Button.connect("pressed", self, "_on_IncreaseMaxSwarmButton_pressed", [global.towers[tower_name]["upgrade2_value"], upgrade2])
			pass
		global.upgrade_type.UPGRADE_INC_HEALTH:
			$GeneralGUI/Control/Upgrade2Button.connect("pressed", self, "_on_IncreaseHealthButton_pressed", [global.towers[tower_name]["upgrade2_value"], upgrade2])
			pass
		global.upgrade_type.UPGRADE_INC_BOUNCE:
			$GeneralGUI/Control/Upgrade2Button.connect("pressed", self, "_on_IncreaseBounceButton_pressed", [global.towers[tower_name]["upgrade2_value"], upgrade2])
			pass
		global.upgrade_type.UPGRADE_INC_SPEED:
			$GeneralGUI/Control/Upgrade2Button.connect("pressed", self, "_on_IncreaseSpeedButton_pressed", [global.towers[tower_name]["upgrade2_value"], upgrade2])
			pass
		global.upgrade_type.UPGRADE_INC_TARGET:
			$GeneralGUI/Control/Upgrade2Button.connect("pressed", self, "_on_IncreaseTargetButton_pressed", [global.towers[tower_name]["upgrade2_value"], upgrade2])
			pass
		global.upgrade_type.UPGRADE_INC_ATK_AREA:
			$GeneralGUI/Control/Upgrade2Button.connect("pressed", self, "_on_IncreaseATKAreaButton_pressed", [global.towers[tower_name]["upgrade2_value"], upgrade2])
			pass
		global.upgrade_type.UPGRADE_INC_AREA:
			$GeneralGUI/Control/Upgrade2Button.connect("pressed", self, "_on_IncreaseAreaButton_pressed", [global.towers[tower_name]["upgrade2_value"], upgrade2])
			pass
		global.upgrade_type.UPGRADE_INC_DMG:
			$GeneralGUI/Control/Upgrade2Button.connect("pressed", self, "_on_IncreaseDamageButton_pressed", [global.towers[tower_name]["upgrade2_value"], upgrade2])
			pass
		global.upgrade_type.UPGRADE_INC_BOUNCE_DMG:
			$GeneralGUI/Control/Upgrade2Button.connect("pressed", self, "_on_IncreaseBounceDamageButton_pressed", [global.towers[tower_name]["upgrade2_value"], upgrade2])
			pass
		global.upgrade_type.UPGRADE_INC_BOUNCE_AND_DMG:
			$GeneralGUI/Control/Upgrade2Button.connect("pressed", self, "_on_IncreaseBounceAndDamageButton_pressed", [global.towers[tower_name]["upgrade2_value"], upgrade2])
			pass
		global.upgrade_type.UPGRADE_GROUND_FIRE:
			$GeneralGUI/Control/Upgrade2Button.connect("pressed", self, "_on_AddGroundFireButton_pressed", [global.towers[tower_name]["upgrade2_value"], upgrade2])
			pass
		global.upgrade_type.UPGRADE_ADD_BOOST_DMG:
			$GeneralGUI/Control/Upgrade2Button.connect("pressed", self, "_on_AddBoostDamageButton_pressed", [global.towers[tower_name]["upgrade2_value"], upgrade2])
			pass
		global.upgrade_type.UPGRADE_PASS_THROUGH:
			$GeneralGUI/Control/Upgrade2Button.connect("pressed", self, "_on_PassThroughButton_pressed", [global.towers[tower_name]["upgrade2_value"], upgrade2])
			pass
		_:
			pass
			
	$GeneralGUI/Control/Upgrade3Button/Label.text = str(upgrade3.price)
	$GeneralGUI/Control/Upgrade3Button.icon = load(global.towers[tower_name]["upgrade3_img_path"])
	match global.towers[tower_name]["upgrade3_type"]:
		global.upgrade_type.UPGRADE_INC_DRONE_NUMBER:
			$GeneralGUI/Control/Upgrade3Button.connect("pressed", self, "_on_IncreaseMaxSwarmButton_pressed", [global.towers[tower_name]["upgrade3_value"], upgrade3])
			pass
		global.upgrade_type.UPGRADE_INC_HEALTH:
			$GeneralGUI/Control/Upgrade3Button.connect("pressed", self, "_on_IncreaseHealthButton_pressed", [global.towers[tower_name]["upgrade3_value"], upgrade3])
			pass
		global.upgrade_type.UPGRADE_INC_BOUNCE:
			$GeneralGUI/Control/Upgrade3Button.connect("pressed", self, "_on_IncreaseBounceButton_pressed", [global.towers[tower_name]["upgrade3_value"], upgrade3])
			pass
		global.upgrade_type.UPGRADE_INC_SPEED:
			$GeneralGUI/Control/Upgrade3Button.connect("pressed", self, "_on_IncreaseSpeedButton_pressed", [global.towers[tower_name]["upgrade3_value"], upgrade3])
			pass
		global.upgrade_type.UPGRADE_INC_TARGET:
			$GeneralGUI/Control/Upgrade3Button.connect("pressed", self, "_on_IncreaseTargetButton_pressed", [global.towers[tower_name]["upgrade3_value"], upgrade3])
			pass
		global.upgrade_type.UPGRADE_INC_ATK_AREA:
			$GeneralGUI/Control/Upgrade3Button.connect("pressed", self, "_on_IncreaseATKAreaButton_pressed", [global.towers[tower_name]["upgrade3_value"], upgrade3])
			pass
		global.upgrade_type.UPGRADE_INC_AREA:
			$GeneralGUI/Control/Upgrade3Button.connect("pressed", self, "_on_IncreaseAreaButton_pressed", [global.towers[tower_name]["upgrade3_value"], upgrade3])
			pass
		global.upgrade_type.UPGRADE_INC_DMG:
			$GeneralGUI/Control/Upgrade3Button.connect("pressed", self, "_on_IncreaseDamageButton_pressed", [global.towers[tower_name]["upgrade3_value"], upgrade3])
			pass
		global.upgrade_type.UPGRADE_INC_BOUNCE_DMG:
			$GeneralGUI/Control/Upgrade3Button.connect("pressed", self, "_on_IncreaseBounceDamageButton_pressed", [global.towers[tower_name]["upgrade3_value"], upgrade3])
			pass
		global.upgrade_type.UPGRADE_INC_BOUNCE_AND_DMG:
			$GeneralGUI/Control/Upgrade3Button.connect("pressed", self, "_on_IncreaseBounceAndDamageButton_pressed", [global.towers[tower_name]["upgrade3_value"], upgrade3])
			pass
		global.upgrade_type.UPGRADE_GROUND_FIRE:
			$GeneralGUI/Control/Upgrade3Button.connect("pressed", self, "_on_AddGroundFireButton_pressed", [global.towers[tower_name]["upgrade3_value"], upgrade3])
			pass
		global.upgrade_type.UPGRADE_ADD_BOOST_DMG:
			$GeneralGUI/Control/Upgrade3Button.connect("pressed", self, "_on_AddBoostDamageButton_pressed", [global.towers[tower_name]["upgrade3_value"], upgrade3])
			pass
		global.upgrade_type.UPGRADE_PASS_THROUGH:
			$GeneralGUI/Control/Upgrade3Button.connect("pressed", self, "_on_PassThroughButton_pressed", [global.towers[tower_name]["upgrade3_value"], upgrade3])
			pass
		_:
			pass
			
	$GeneralGUI/Control/MegaUpgradeButton/Label.text = str(mega_upgrade.price)
	$GeneralGUI/Control/MegaUpgradeButton.icon = load(global.towers[tower_name]["mega_upgrade_img_path"])
	match global.towers[tower_name]["mega_upgrade_type"]:
		global.upgrade_type.UPGRADE_INC_DRONE_NUMBER:
			$GeneralGUI/Control/MegaUpgradeButton.connect("pressed", self, "_on_IncreaseMaxSwarmButton_pressed", [global.towers[tower_name]["mega_upgrade_value"], mega_upgrade])
			pass
		global.upgrade_type.UPGRADE_INC_HEALTH:
			$GeneralGUI/Control/MegaUpgradeButton.connect("pressed", self, "_on_IncreaseHealthButton_pressed", [global.towers[tower_name]["mega_upgrade_value"], mega_upgrade])
			pass
		global.upgrade_type.UPGRADE_INC_BOUNCE:
			$GeneralGUI/Control/MegaUpgradeButton.connect("pressed", self, "_on_IncreaseBounceButton_pressed", [global.towers[tower_name]["mega_upgrade_value"], mega_upgrade])
			pass
		global.upgrade_type.UPGRADE_INC_SPEED:
			$GeneralGUI/Control/MegaUpgradeButton.connect("pressed", self, "_on_IncreaseSpeedButton_pressed", [global.towers[tower_name]["mega_upgrade_value"], mega_upgrade])
			pass
		global.upgrade_type.UPGRADE_INC_TARGET:
			$GeneralGUI/Control/MegaUpgradeButton.connect("pressed", self, "_on_IncreaseTargetButton_pressed", [global.towers[tower_name]["mega_upgrade_value"], mega_upgrade])
			pass
		global.upgrade_type.UPGRADE_INC_ATK_AREA:
			$GeneralGUI/Control/MegaUpgradeButton.connect("pressed", self, "_on_IncreaseATKAreaButton_pressed", [global.towers[tower_name]["mega_upgrade_value"], mega_upgrade])
			pass
		global.upgrade_type.UPGRADE_INC_AREA:
			$GeneralGUI/Control/MegaUpgradeButton.connect("pressed", self, "_on_IncreaseAreaButton_pressed", [global.towers[tower_name]["mega_upgrade_value"], mega_upgrade])
			pass
		global.upgrade_type.UPGRADE_INC_DMG:
			$GeneralGUI/Control/MegaUpgradeButton.connect("pressed", self, "_on_IncreaseDamageButton_pressed", [global.towers[tower_name]["mega_upgrade_value"], mega_upgrade])
			pass
		global.upgrade_type.UPGRADE_INC_BOUNCE_DMG:
			$GeneralGUI/Control/MegaUpgradeButton.connect("pressed", self, "_on_IncreaseBounceDamageButton_pressed", [global.towers[tower_name]["mega_upgrade_value"], mega_upgrade])
			pass
		global.upgrade_type.UPGRADE_INC_BOUNCE_AND_DMG:
			$GeneralGUI/Control/MegaUpgradeButton.connect("pressed", self, "_on_IncreaseBounceAndDamageButton_pressed", [global.towers[tower_name]["mega_upgrade_value"], mega_upgrade])
			pass
		global.upgrade_type.UPGRADE_GROUND_FIRE:
			$GeneralGUI/Control/MegaUpgradeButton.connect("pressed", self, "_on_AddGroundFireButton_pressed", [global.towers[tower_name]["mega_upgrade_value"], mega_upgrade])
			pass
		global.upgrade_type.UPGRADE_ADD_BOOST_DMG:
			$GeneralGUI/Control/MegaUpgradeButton.connect("pressed", self, "_on_AddBoostDamageButton_pressed", [global.towers[tower_name]["mega_upgrade_value"], mega_upgrade])
			pass
		global.upgrade_type.UPGRADE_PASS_THROUGH:
			$GeneralGUI/Control/MegaUpgradeButton.connect("pressed", self, "_on_PassThroughButton_pressed", [global.towers[tower_name]["mega_upgrade_value"], mega_upgrade])
			pass
		_:
			pass
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	#update_swarm_bar()

	specific_process(delta)

	pass

func specific_process(_delta):
	if tower_state == TOWER_CLOSED and swarm_inside > 0:
		if enemy_collision_list.size() > 0:
			open_tower()
	if tower_state == TOWER_OPENED:
		if swarm_outside <= 0:
			close_tower()
	pass

func _unhandled_input(event: InputEvent) -> void:
#func _input(event : InputEvent):
		#print("input process de merde")
#		if event is InputEventMouseMotion:
#			pass
		if event is InputEventMouseButton and event.is_pressed():
			if not picked:
				match event.button_index:
					BUTTON_LEFT:
						if tower_select.selecting_collect_target:
							pass
						else:
							if get_viewport().canvas_transform.affine_inverse().xform(event.position).distance_to(global_position) < 30:
								# left button clicked
								#print("button")
								tower_select.emit_signal("select_tower", self)
								#print("not picked")
#					BUTTON_RIGHT:
#						pass
						# right button clicked
#		else:
#				pass

func updated_gold_amount():
	update_buttons()
	pass

func update_buttons():
	# Button: Add new drone
	if ((swarm_outside + swarm_inside) < max_swarm_number):
		$GeneralGUI/Control/AddSwarmButton.modulate = Color(1.0, 1.0, 1.0)
		if (swarm_price <= global.gold_ressource):
			$GeneralGUI/Control/AddSwarmButton.disabled = false
		else:
			$GeneralGUI/Control/AddSwarmButton.disabled = true
	else:
		$GeneralGUI/Control/AddSwarmButton.disabled = true
		$GeneralGUI/Control/AddSwarmButton.modulate = Color(0.0, 1.3, 0.0)

	# Button: Upgrade 1
	if (upgrade1.max_number <= upgrade1.number):
		$GeneralGUI/Control/Upgrade1Button.disabled = true
		$GeneralGUI/Control/Upgrade1Button.modulate = Color(0.0, 1.3, 0.0)
		$GeneralGUI/Control/Upgrade1Button/Label.visible = false
		$GeneralGUI/Control/Upgrade1Button/Sprite.visible = false
	else:
		if (upgrade1.price <= global.gold_ressource):
			$GeneralGUI/Control/Upgrade1Button.disabled = false
		else:
			$GeneralGUI/Control/Upgrade1Button.disabled = true
		
		
	# Button: Upgrade 2
	if (upgrade2.max_number <= upgrade2.number):
		$GeneralGUI/Control/Upgrade2Button.disabled = true
		$GeneralGUI/Control/Upgrade2Button.modulate = Color(0.0, 1.3, 0.0)
		$GeneralGUI/Control/Upgrade2Button/Label.visible = false
		$GeneralGUI/Control/Upgrade2Button/Sprite.visible = false
	else:
		if (upgrade2.price <= global.gold_ressource):
			$GeneralGUI/Control/Upgrade2Button.disabled = false
		else:
			$GeneralGUI/Control/Upgrade2Button.disabled = true
		
	# Button: Upgrade 3
	if (upgrade3.max_number <= upgrade3.number):
		$GeneralGUI/Control/Upgrade3Button.disabled = true
		$GeneralGUI/Control/Upgrade3Button.modulate = Color(0.0, 1.3, 0.0)
		$GeneralGUI/Control/Upgrade3Button/Label.visible = false
		$GeneralGUI/Control/Upgrade3Button/Sprite.visible = false
	else:
		if (upgrade3.price <= global.gold_ressource):
			$GeneralGUI/Control/Upgrade3Button.disabled = false
		else:
			$GeneralGUI/Control/Upgrade3Button.disabled = true
		
	# Button: Mega Upgrade
	if (mega_upgrade.max_number <= mega_upgrade.number):
		$GeneralGUI/Control/MegaUpgradeButton.disabled = true
		$GeneralGUI/Control/MegaUpgradeButton.modulate = Color(0.0, 1.3, 0.0)
		$GeneralGUI/Control/MegaUpgradeButton/Label.visible = false
		$GeneralGUI/Control/MegaUpgradeButton/Sprite.visible = false
	else:
		if (mega_upgrade.price <= global.super_gold_ressource):
			$GeneralGUI/Control/MegaUpgradeButton.disabled = false
		else:
			$GeneralGUI/Control/MegaUpgradeButton.disabled = true

func update_swarm_bar():
	$GUI/SwarmBar.margin_left = -4 * max_swarm_number
	$GUI/SwarmBar.margin_right = 4 * max_swarm_number
	$GUI/SwarmBar.max_value = max_swarm_number
	$GUI/SwarmBar.value = swarm_outside + swarm_inside
	pass
	
func drop_on_ground():
	if tower_collision_list.size() == 0:
		picked = false
		$AoESprite.visible = false
		actor_enabled = true
		#$BuildingCollision.connect("area_entered", self, "_on_BuildingCollision_area_entered")
		#$BuildingCollision.connect("area_exited", self, "_on_BuildingCollision_area_exited")
		connect("body_entered", self, "_on_TowerActor_body_entered")
		connect("body_exited", self, "_on_TowerActor_body_exited")
		$TowerAreaOfEffect.set_deferred("disabled", false)
		$BuildingCollision.set_deferred("collision_layer", 1)
		$DropOnGroundParticles.emitting = true
		$DropOnGroundParticles.restart()
		$BuildAudioStreamPlayer2D.play()
		
		var ftext = floating_text.instance()
		ftext.value = "-" + str(tower_price)
		ftext.global_position = global_position
		ftext.color = Color("ffffff")
		$World.add_child(ftext)
		
		EventBus.emit_signal("dropped_tower")
		#$AnimationPlayer.play("open")
		return true
	else:
		return false
	pass

func open_tower():
	tower_state = TOWER_OPENING
	$AnimationPlayer.play("open")
	pass
	
func close_tower():
	tower_state = TOWER_CLOSING
	$NextSwarmTimer.stop()
	$AnimationPlayer.play("close")
	pass

func tower_opened():
	tower_state = TOWER_OPENED
	$NextSwarmTimer.start()
	summon_swarm()
	pass
	
func tower_closed():
	tower_state = TOWER_CLOSED
	$AnimationPlayer.play("idle")
	pass

func modify_health(value):
	if value < 0:
		if increased_defense == true:
			health = health + value * global.bonus_defense_increase_percent
			pass
		else:
			health = health + value
			pass
		pass
	else:
		health = health + value
		pass
	
	if health > starting_health:
		health = starting_health
	$GUI/LifeBar.value = health
	if health >= (starting_health - 5):
		$GUI/LifeBar.visible = false
	else:
		$GUI/LifeBar.visible = true
	if value < 0:
		$Tween.interpolate_property($Sprite, "modulate", Color(1, 1, 1, 1), Color(0, 0, 0, 1), 0.05, Tween.TRANS_LINEAR, Tween.EASE_IN)
		$Tween.interpolate_property($Sprite, "modulate", Color(0, 0, 0, 1), Color(1, 1, 1, 1), 0.05, Tween.TRANS_LINEAR, Tween.EASE_IN, 0.05)
		$Tween.start()
	if health <= 0:
		emit_signal("died", self)
		destroy_instance()
	pass
	
func destroy_instance():
	var explo = explosion.instance()
	explo.global_position = global_position
	get_parent().add_child(explo)
	explo.get_node("AnimationPlayer").play("explosion")
	EventBus.emit_signal("shake_camera", 0.5)
	EventBus.emit_signal("add_points", global.action_points["tower_kill"])
	global.player_stats["tower_kill"] += 1
	
	call_deferred("drop_gold")
	
	queue_free()
	pass

func drop_gold():
	var new_gold = harvestable.instance()
	new_gold.global_position = global_position
	new_gold.value = 10
	get_tree().get_root().get_node_or_null("BattleField/GroundObject").add_child(new_gold)

func summon_swarm():
	#$NextSwarmTimer.stop()
	if tower_state == TOWER_OPENED and swarm_inside > 0 and enemy_collision_list.size() > 0:
		var swarm = SwarmActorClass.instance()
		swarm.global_position = global_position
		swarm.parent_tower = self
		swarm.target = enemy_collision_list[0]
		swarm.starting_health = swarm_health
		swarm.starting_rate = swarm_fire_rate
		swarm.starting_damage = swarm_damage
		swarm.speed = swarm.speed * speed_boost
		swarm.turning_speed = swarm.turning_speed * turn_speed_boost
		swarm.pass_through = pass_through
		swarm.boost_damage = boost_damage
		swarm.boost_defense = boost_defense
		swarm.ground_in_fire = ground_in_fire
		var _result = connect("change_target", swarm, "change_target")
		_result = swarm.connect("landed", self, "swarm_landed")
		_result = connect("died", swarm, "parent_died")
		_result = swarm.connect("died", self, "on_swarm_died")
		get_node("../../FlyingObject").add_child(swarm)
		swarm_outside = swarm_outside + 1
		swarm_inside = swarm_inside - 1
		compute_target()
		if enemy_collision_list.size() == 0:
			if swarm_outside <= 0:
				close_tower()
		if swarm_inside > 0:
			$NextSwarmTimer.start()
	else:
		if swarm_outside <= 0:
			close_tower()
		pass
	update_buttons()
	update_swarm_bar()
	pass

func swarm_landed():
	swarm_outside = swarm_outside - 1
	swarm_inside = swarm_inside + 1
	#print("landed"+str(swarm_outside)+"/"+str(swarm_inside))
	if swarm_outside <= 0:
		close_tower()
	update_buttons()
	update_swarm_bar()
	pass

func tower_selected():
	$GeneralGUI/Control.visible = true
	pass

func tower_unselected():
	$GeneralGUI/Control.visible = false
	pass
	
func _on_TowerActor_area_entered(_area):
	pass # Replace with function body.

func _on_TowerActor_area_exited(_area):
	pass # Replace with function body.

func _on_BuildingCollision_area_entered(area):
	if (area.is_in_group("tower_building") or area.is_in_group("tower_area_building")) and area != self:
		var is_present = tower_collision_list.find(area)
		#print(is_present)
		if is_present != -1:
			pass
		else:
			tower_collision_list.push_back(area)
			if tower_collision_list.size() == 1:
				$Tween.interpolate_property($Sprite, "modulate", Color(1, 1, 1, 1), Color(1, 0, 0, 1), 0.05, Tween.TRANS_LINEAR, Tween.EASE_IN)
				$Tween.start()
			pass
	else:
		# Projectile
		pass
	pass # Replace with function body.


func _on_BuildingCollision_area_exited(area):
	if (area.is_in_group("tower_building") or area.is_in_group("tower_area_building")) and area != self:
		var is_present = tower_collision_list.find(area)
		if is_present != -1:
			tower_collision_list.remove(is_present)
			if tower_collision_list.size() == 0:
				$Tween.interpolate_property($Sprite, "modulate", Color(1, 0, 0, 1), Color(1, 1, 1, 1), 0.05, Tween.TRANS_LINEAR, Tween.EASE_IN)
				$Tween.start()
			pass
		else:
			pass
	else:
		# Projectile
		pass
	pass # Replace with function body.

func _on_BuildingCollision_body_entered(body):
	if (body.is_in_group("tower_building") or body.is_in_group("tower_area_building")) and body != self:
		var is_present = tower_collision_list.find(body)
		#print(is_present)
		if is_present != -1:
			pass
		else:
			tower_collision_list.push_back(body)
			if tower_collision_list.size() == 1:
				$Tween.interpolate_property($Sprite, "modulate", Color(1, 1, 1, 1), Color(1, 0, 0, 1), 0.05, Tween.TRANS_LINEAR, Tween.EASE_IN)
				$Tween.start()
			pass
	else:
		# Projectile
		pass
	pass # Replace with function body.


func _on_BuildingCollision_body_exited(body):
	if (body.is_in_group("tower_building") or body.is_in_group("tower_area_building")) and body != self:
		var is_present = tower_collision_list.find(body)
		if is_present != -1:
			tower_collision_list.remove(is_present)
			if tower_collision_list.size() == 0:
				$Tween.interpolate_property($Sprite, "modulate", Color(1, 0, 0, 1), Color(1, 1, 1, 1), 0.05, Tween.TRANS_LINEAR, Tween.EASE_IN)
				$Tween.start()
			pass
		else:
			pass
	else:
		# Projectile
		pass
	pass # Replace with function body.
	
	

func _on_TowerActor_body_entered(body):
	#print("body entering")
	if body.is_in_group("enemy"):
		var is_present = enemy_collision_list.find(body)
		#print(is_present)
		if is_present != -1:
			pass
		else:
			enemy_collision_list.push_back(body)
			body.connect("died", self, "on_enemy_died")
			compute_target()
					
	pass # Replace with function body.


func _on_TowerActor_body_exited(body):
	#print("body exiting")
	if body.is_in_group("enemy"):
		var is_present = enemy_collision_list.find(body)
		if is_present != -1:
			enemy_collision_list.remove(is_present)
			body.disconnect("died", self, "on_enemy_died")
			compute_target()
	pass # Replace with function body.

func on_enemy_died(enemy):
	var is_present = enemy_collision_list.find(enemy)
	if is_present != -1:
		enemy_collision_list.remove(is_present)
		compute_target()
	pass

func compute_target():
	if enemy_collision_list.size() == 0:
		emit_signal("change_target", null)
	else:
		# Sort list depending on strategy
		match strategy_type:
			global.tower_strategy.STRATEGY_FIRST_SEEN:
				pass
			global.tower_strategy.STRATEGY_MOST_HEALTH:
				for enemy in enemy_collision_list:
					if enemy.health > enemy_collision_list[0].health:
						enemy_collision_list.remove(enemy_collision_list.find(enemy))
						enemy_collision_list.push_front(enemy)
						pass
					pass
				pass
			global.tower_strategy.STRATEGY_LEAST_HEALTH:
				for enemy in enemy_collision_list:
					if enemy.health < enemy_collision_list[0].health:
						enemy_collision_list.remove(enemy_collision_list.find(enemy))
						enemy_collision_list.push_front(enemy)
						pass
					pass
				pass
			global.tower_strategy.STRATEGY_MOST_MAX_HEALTH:
				for enemy in enemy_collision_list:
					if enemy.starting_health > enemy_collision_list[0].starting_health:
						enemy_collision_list.remove(enemy_collision_list.find(enemy))
						enemy_collision_list.push_front(enemy)
						pass
					pass
				pass
			global.tower_strategy.STRATEGY_LEAST_MAX_HEALTH:
				for enemy in enemy_collision_list:
					if enemy.starting_health < enemy_collision_list[0].starting_health:
						enemy_collision_list.remove(enemy_collision_list.find(enemy))
						enemy_collision_list.push_front(enemy)
						pass
					pass
				pass
			_:
				pass
		emit_signal("change_target", enemy_collision_list[0])
	pass

func on_swarm_died(_swarm):
	swarm_outside = swarm_outside - 1
	if swarm_outside < 0:
		swarm_outside = 0
	update_buttons()
	update_swarm_bar()
	pass

func _on_NextSwarmTimer_timeout():
	summon_swarm()
	pass # Replace with function body.


func _on_AddSwarmButton_pressed():
	if ((swarm_outside + swarm_inside) < max_swarm_number) and (swarm_price <= global.gold_ressource):
		audio_manager.play_sfx(load("res://assets/sfx/gui/BipOK.wav"), 0, 1)
		swarm_inside = swarm_inside + 1
		#global.gold_ressource = global.gold_ressource - global.first_tower_swarm_price
		EventBus.emit_signal("add_gold", -swarm_price)
		#EventBus.emit_signal("tower_upgraded")
		
		var ftext = floating_text.instance()
		ftext.value = "-" + str(swarm_price)
		ftext.global_position = global_position
		ftext.color = Color("ffffff")
		$World.add_child(ftext)
		
	update_swarm_bar()
	pass # Replace with function body.

func use_upgrade_gold(upgrade):
	audio_manager.play_sfx(load("res://assets/sfx/gui/BipOK.wav"), 0, 1)
	if upgrade.type == 1:
		EventBus.emit_signal("add_super_gold", -upgrade.price)
		var ftext = floating_text.instance()
		ftext.value = "-" + str(upgrade.price)
		ftext.global_position = global_position
		ftext.color = Color("ff0000")
		$World.add_child(ftext)
	else:
		EventBus.emit_signal("add_gold", -upgrade.price)
		var ftext = floating_text.instance()
		ftext.value = "-" + str(upgrade.price)
		ftext.global_position = global_position
		ftext.color = Color("ffffff")
		$World.add_child(ftext)

func _on_IncreaseMaxSwarmButton_pressed(value, upgrade):
	if upgrade.max_number > upgrade.number:
		upgrade.number = upgrade.number + 1
		max_swarm_number = max_swarm_number + value
		swarm_inside = swarm_inside + value
		use_upgrade_gold(upgrade)
		EventBus.emit_signal("tower_upgraded")
		
	update_swarm_bar()
	pass # Replace with function body.

func _on_IncreaseHealthButton_pressed(value, upgrade):
	if upgrade.max_number > upgrade.number:
		upgrade.number = upgrade.number + 1
		swarm_health = swarm_health + value
		use_upgrade_gold(upgrade)
		
	update_swarm_bar()
	pass

func _on_IncreaseBounceButton_pressed(value, upgrade):
	if upgrade.max_number > upgrade.number:
		upgrade.number = upgrade.number + 1
		bouncing_number = bouncing_number + value
		use_upgrade_gold(upgrade)
		
	update_swarm_bar()
	pass

func _on_IncreaseSpeedButton_pressed(value, upgrade):
	if upgrade.max_number > upgrade.number:
		upgrade.number = upgrade.number + 1
		speed_boost = speed_boost + value
		turn_speed_boost = turn_speed_boost + value / 2.0
		use_upgrade_gold(upgrade)
		
	update_swarm_bar()
	pass

func _on_IncreaseTargetButton_pressed(value, upgrade):
	if upgrade.max_number > upgrade.number: #FOR HEALER
		upgrade.number = upgrade.number + 1
		increase_target_number = increase_target_number + 1 #Increase target number
		swarm_damage = swarm_damage + value #Decrease damage
		use_upgrade_gold(upgrade)
		
	update_swarm_bar()
	pass

func _on_IncreaseATKAreaButton_pressed(value, upgrade):
	if upgrade.max_number > upgrade.number:
		upgrade.number = upgrade.number + 1
		# TODO
		use_upgrade_gold(upgrade)
		
	update_swarm_bar()
	pass

func _on_IncreaseAreaButton_pressed(value, upgrade):
	if upgrade.max_number > upgrade.number:
		upgrade.number = upgrade.number + 1
		$TowerAreaOfEffect.shape = $TowerAreaOfEffect.shape.duplicate()
		$TowerAreaOfEffect.shape.radius = $TowerAreaOfEffect.shape.radius + value
		use_upgrade_gold(upgrade)
		
	update_swarm_bar()
	pass
	
func _on_IncreaseDamageButton_pressed(value, upgrade):
	if upgrade.max_number > upgrade.number:
		upgrade.number = upgrade.number + 1
		swarm_damage = swarm_damage + value
		use_upgrade_gold(upgrade)
		
	update_swarm_bar()
	pass
	
func _on_IncreaseBounceDamageButton_pressed(value, upgrade):
	if upgrade.max_number > upgrade.number:
		upgrade.number = upgrade.number + 1
		bouncing_damage_reduction = bouncing_damage_reduction + value
		use_upgrade_gold(upgrade)
		
	update_swarm_bar()
	pass
	
func _on_IncreaseBounceAndDamageButton_pressed(value, upgrade):
	if upgrade.max_number > upgrade.number:
		upgrade.number = upgrade.number + 1
		bouncing_number = bouncing_number + 1
		bouncing_damage_reduction = bouncing_damage_reduction + value
		use_upgrade_gold(upgrade)
		
	update_swarm_bar()
	pass
	
func _on_AddGroundFireButton_pressed(value, upgrade):
	if upgrade.max_number > upgrade.number:
		upgrade.number = upgrade.number + 1
		ground_in_fire = true
		use_upgrade_gold(upgrade)
		
	update_swarm_bar()
	pass
	
func _on_AddBoostDamageButton_pressed(value, upgrade):
	if upgrade.max_number > upgrade.number:
		upgrade.number = upgrade.number + 1
		boost_damage = true
		use_upgrade_gold(upgrade)
		
	update_swarm_bar()
	pass
	
func _on_PassThroughButton_pressed(value, upgrade):
	if upgrade.max_number > upgrade.number:
		upgrade.number = upgrade.number + 1
		pass_through = true
		use_upgrade_gold(upgrade)
		
	update_swarm_bar()
	pass



func _on_AddSwarmButton_mouse_entered():
	$GeneralGUI/Tooltip.visible = true
	$GeneralGUI/Tooltip.rect_position = $GeneralGUI/Control/AddSwarmButton.rect_global_position + Vector2(-185, -120)
	$GeneralGUI/Tooltip/Label.text = tr("KEY_TOWER_BUY_NEW_DRONE")
	if not $GeneralGUI/Control/AddSwarmButton.disabled:
		$GeneralGUI/Control/AddSwarmButton.modulate = Color(1.3,1.3,1.3)
	pass # Replace with function body.

func _on_AddSwarmButton_mouse_exited():
	$GeneralGUI/Tooltip.visible = false
	if not $GeneralGUI/Control/AddSwarmButton.disabled:
		$GeneralGUI/Control/AddSwarmButton.modulate = Color(1,1,1)
	pass # Replace with function body.


func _on_Upgrade1Button_mouse_entered():
	$GeneralGUI/Tooltip.visible = true
	$GeneralGUI/Tooltip.rect_position = $GeneralGUI/Control/Upgrade1Button.rect_global_position + Vector2(-185, -120)
	$GeneralGUI/Tooltip/Label.text = tr(global.towers[tower_name]["upgrade1_tooltip"])
	if not $GeneralGUI/Control/Upgrade1Button.disabled:
		$GeneralGUI/Control/Upgrade1Button.modulate = Color(1.3,1.3,1.3)
	pass # Replace with function body.

func _on_Upgrade1Button_mouse_exited():
	$GeneralGUI/Tooltip.visible = false
	if not $GeneralGUI/Control/Upgrade1Button.disabled:
		$GeneralGUI/Control/Upgrade1Button.modulate = Color(1,1,1)
	pass # Replace with function body.


func _on_Upgrade2Button_mouse_entered():
	$GeneralGUI/Tooltip.visible = true
	$GeneralGUI/Tooltip.rect_position = $GeneralGUI/Control/Upgrade2Button.rect_global_position + Vector2(-185, -120)
	$GeneralGUI/Tooltip/Label.text = tr(global.towers[tower_name]["upgrade2_tooltip"])
	if not $GeneralGUI/Control/Upgrade2Button.disabled:
		$GeneralGUI/Control/Upgrade2Button.modulate = Color(1.3,1.3,1.3)
	pass # Replace with function body.

func _on_Upgrade2Button_mouse_exited():
	$GeneralGUI/Tooltip.visible = false
	if not $GeneralGUI/Control/Upgrade2Button.disabled:
		$GeneralGUI/Control/Upgrade2Button.modulate = Color(1,1,1)
	pass # Replace with function body.


func _on_Upgrade3Button_mouse_entered():
	$GeneralGUI/Tooltip.visible = true
	$GeneralGUI/Tooltip.rect_position = $GeneralGUI/Control/Upgrade3Button.rect_global_position + Vector2(-185, -120)
	$GeneralGUI/Tooltip/Label.text = tr(global.towers[tower_name]["upgrade3_tooltip"])
	if not $GeneralGUI/Control/Upgrade3Button.disabled:
		$GeneralGUI/Control/Upgrade3Button.modulate = Color(1.3,1.3,1.3)
	pass # Replace with function body.

func _on_Upgrade3Button_mouse_exited():
	$GeneralGUI/Tooltip.visible = false
	if not $GeneralGUI/Control/Upgrade3Button.disabled:
		$GeneralGUI/Control/Upgrade3Button.modulate = Color(1,1,1)
	pass # Replace with function body.
	
	
func _on_MegaUpgradeButton_mouse_entered():
	$GeneralGUI/Tooltip.visible = true
	$GeneralGUI/Tooltip.rect_position = $GeneralGUI/Control/MegaUpgradeButton.rect_global_position + Vector2(-185, -120)
	$GeneralGUI/Tooltip/Label.text = tr(global.towers[tower_name]["mega_upgrade_tooltip"])
	if not $GeneralGUI/Control/MegaUpgradeButton.disabled:
		$GeneralGUI/Control/MegaUpgradeButton.modulate = Color(1.3,1.3,1.3)
	pass # Replace with function body.

func _on_MegaUpgradeButton_mouse_exited():
	$GeneralGUI/Tooltip.visible = false
	if not $GeneralGUI/Control/MegaUpgradeButton.disabled:
		$GeneralGUI/Control/MegaUpgradeButton.modulate = Color(1,1,1)
	pass # Replace with function body.



func check_strategy_button():
	$GeneralGUI/Control/StrategyFirstSeen.modulate = Color(1.0, 1.0, 1.0)
	$GeneralGUI/Control/StrategyMostHealth.modulate = Color(1.0, 1.0, 1.0)
	$GeneralGUI/Control/StrategyLeastHealth.modulate = Color(1.0, 1.0, 1.0)
	$GeneralGUI/Control/StrategyMostMaxHealth.modulate = Color(1.0, 1.0, 1.0)
	$GeneralGUI/Control/StrategyLeastMaxHealth.modulate = Color(1.0, 1.0, 1.0)
	match strategy_type:
		global.tower_strategy.STRATEGY_FIRST_SEEN:
			$GeneralGUI/Control/StrategyFirstSeen.modulate = Color(0.0, 1.0, 0.0)
			pass
		global.tower_strategy.STRATEGY_MOST_HEALTH:
			$GeneralGUI/Control/StrategyMostHealth.modulate = Color(0.0, 1.0, 0.0)
			pass
		global.tower_strategy.STRATEGY_LEAST_HEALTH:
			$GeneralGUI/Control/StrategyLeastHealth.modulate = Color(0.0, 1.0, 0.0)
			pass
		global.tower_strategy.STRATEGY_MOST_MAX_HEALTH:
			$GeneralGUI/Control/StrategyMostMaxHealth.modulate = Color(0.0, 1.0, 0.0)
			pass
		global.tower_strategy.STRATEGY_LEAST_MAX_HEALTH:
			$GeneralGUI/Control/StrategyLeastMaxHealth.modulate = Color(0.0, 1.0, 0.0)
			pass
		_:
			pass
	pass

func _on_StrategyFirstSeen_pressed():
	strategy_type = global.tower_strategy.STRATEGY_FIRST_SEEN
	check_strategy_button()
	compute_target()
	pass # Replace with function body.


func _on_StrategyMostHealth_pressed():
	strategy_type = global.tower_strategy.STRATEGY_MOST_HEALTH
	check_strategy_button()
	compute_target()
	pass # Replace with function body.


func _on_StrategyLeastHealth_pressed():
	strategy_type = global.tower_strategy.STRATEGY_LEAST_HEALTH
	check_strategy_button()
	compute_target()
	pass # Replace with function body.


func _on_StrategyMostMaxHealth_pressed():
	strategy_type = global.tower_strategy.STRATEGY_MOST_MAX_HEALTH
	check_strategy_button()
	compute_target()
	pass # Replace with function body.


func _on_StrategyLeastMaxHealth_pressed():
	strategy_type = global.tower_strategy.STRATEGY_LEAST_MAX_HEALTH
	check_strategy_button()
	compute_target()
	pass # Replace with function body.



func _on_StrategyFirstSeen_mouse_entered():
	$GeneralGUI/Tooltip.visible = true
	$GeneralGUI/Tooltip.rect_position = $GeneralGUI/Control/StrategyFirstSeen.rect_global_position + Vector2(-185, -120)
	$GeneralGUI/Tooltip/Label.text = tr(global.towers["strategy"]["first_seen_tooltip"])
	pass # Replace with function body.

func _on_StrategyFirstSeen_mouse_exited():
	$GeneralGUI/Tooltip.visible = false
	check_strategy_button()
	pass # Replace with function body.


func _on_StrategyMostHealth_mouse_entered():
	$GeneralGUI/Tooltip.visible = true
	$GeneralGUI/Tooltip.rect_position = $GeneralGUI/Control/StrategyMostHealth.rect_global_position + Vector2(-185, -120)
	$GeneralGUI/Tooltip/Label.text = tr(global.towers["strategy"]["most_health_tooltip"])
	pass # Replace with function body.


func _on_StrategyMostHealth_mouse_exited():
	$GeneralGUI/Tooltip.visible = false
	check_strategy_button()
	pass # Replace with function body.


func _on_StrategyLeastHealth_mouse_entered():
	$GeneralGUI/Tooltip.visible = true
	$GeneralGUI/Tooltip.rect_position = $GeneralGUI/Control/StrategyLeastHealth.rect_global_position + Vector2(-185, -120)
	$GeneralGUI/Tooltip/Label.text = tr(global.towers["strategy"]["least_health_tooltip"])
	pass # Replace with function body.


func _on_StrategyLeastHealth_mouse_exited():
	$GeneralGUI/Tooltip.visible = false
	check_strategy_button()
	pass # Replace with function body.


func _on_StrategyMostMaxHealth_mouse_entered():
	$GeneralGUI/Tooltip.visible = true
	$GeneralGUI/Tooltip.rect_position = $GeneralGUI/Control/StrategyMostMaxHealth.rect_global_position + Vector2(-185, -120)
	$GeneralGUI/Tooltip/Label.text = tr(global.towers["strategy"]["most_max_health_tooltip"])
	pass # Replace with function body.


func _on_StrategyMostMaxHealth_mouse_exited():
	$GeneralGUI/Tooltip.visible = false
	check_strategy_button()
	pass # Replace with function body.


func _on_StrategyLeastMaxHealth_mouse_entered():
	$GeneralGUI/Tooltip.visible = true
	$GeneralGUI/Tooltip.rect_position = $GeneralGUI/Control/StrategyLeastMaxHealth.rect_global_position + Vector2(-185, -120)
	$GeneralGUI/Tooltip/Label.text = tr(global.towers["strategy"]["least_max_health_tooltip"])
	pass # Replace with function body.


func _on_StrategyLeastMaxHealth_mouse_exited():
	$GeneralGUI/Tooltip.visible = false
	check_strategy_button()
	pass # Replace with function body.

func on_bonus_received(type):
	if type == global.ressource_type.RESSOURCE_INC_DEFENCE:
		increased_defense = true
		$DefenseTimer.stop()
		$DefenseTimer.start()
		$DefenseActivated.visible = true
		pass
	else:
		# No other possible bonus
		pass
	pass


func _on_DefenseTimer_timeout():
	increased_defense = false
	$DefenseActivated.visible = false
	pass # Replace with function body.
