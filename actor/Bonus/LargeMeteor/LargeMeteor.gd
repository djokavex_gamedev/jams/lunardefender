extends "res://actor/Bonus/BonusTemplate.gd"

var MeteorClass = preload("res://effects/Meteor/Meteor.tscn")

func activate():
	$CollisionShape2D.disabled = true
	$Sprite.visible = false
	$SelectableParticles.visible = false
	
	for i in 20:
		randomize()
		var meteor = MeteorClass.instance()
		meteor.global_position = global_position + Vector2(rand_range(-25,25), rand_range(-1000,-900))
		meteor.motion = Vector2(rand_range(-25,25), rand_range(400,500))
		meteor.damage = 50
		#$World.add_child(meteor)
		get_tree().get_root().get_node_or_null("BattleField/GroundObject").add_child(meteor)
		meteor = MeteorClass.instance()
		yield(get_tree().create_timer(0.1),"timeout")
	
	yield(get_tree().create_timer(4.0),"timeout")
	queue_free()
	pass
