extends "res://actor/Bonus/LaunchedBonusTemplate.gd"

var MeteorClass = preload("res://effects/Meteor/Meteor.tscn")

func launch_bonus():
	$Sprite.visible = false
	picked = false
	
	var meteor = MeteorClass.instance()
	meteor.global_position = global_position + Vector2(-40, -900)
	meteor.motion = Vector2(25, 450)
	meteor.damage = 300
	#$World.add_child(meteor)
	get_tree().get_root().get_node_or_null("BattleField/GroundObject").add_child(meteor)
	meteor = MeteorClass.instance()
	
	end_bonus()
	pass
	
	
