extends "res://actor/Bonus/LaunchedBonusTemplate.gd"

func launch_bonus():
	#$Sprite.visible = false
	$Particles2D.emitting = true
	$Particles2D.visible = true
	$TickTimer.start()
	$EndTimer.start()
	picked = false
	pass

func _on_TickTimer_timeout():
	var overlapping_bodies = get_overlapping_bodies()
	for body in overlapping_bodies:
		if body.is_in_group("swarm"):
			body.on_bonus_received(global.ressource_type.RESSOURCE_INC_DEFENCE)
		if body.is_in_group("enemy"):
			#body.modify_health(heal_value)
			pass
	var overlapping_areas = get_overlapping_areas()
	for area in overlapping_areas:
		if area.is_in_group("tower_building"):
			area.get_parent().on_bonus_received(global.ressource_type.RESSOURCE_INC_DEFENCE)
	pass # Replace with function body.



func _on_EndTimer_timeout():
	end_bonus()
	pass # Replace with function body.
