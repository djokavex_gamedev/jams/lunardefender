extends "res://actor/Bonus/BonusTemplate.gd"

var heal_value = 5

func activate():
	$CollisionShape2D.disabled = true
	$Sprite.visible = false
	$SelectableParticles.visible = false
	
	$RegenArea.visible = true
	$HealTimer.start()
	
	yield(get_tree().create_timer(10.0),"timeout")
	queue_free()
	pass


func _on_HealTimer_timeout():
	var overlapping_bodies = $RegenArea.get_overlapping_bodies()
	for body in overlapping_bodies:
		if body.is_in_group("swarm"):
			body.modify_health(heal_value)
		if body.is_in_group("enemy"):
			#body.modify_health(heal_value)
			pass
	var overlapping_areas = $RegenArea.get_overlapping_areas()
	for area in overlapping_areas:
		if area.is_in_group("tower_building"):
			area.get_parent().modify_health(heal_value)
	pass # Replace with function body.
