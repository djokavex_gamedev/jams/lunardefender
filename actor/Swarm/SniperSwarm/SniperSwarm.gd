extends "res://actor/Swarm/SwarmActor.gd"

func _ready():
	LaserClass = preload("res://actor/Swarm/LongLaser.tscn")
	pass

func _on_ShotTimer_timeout():
	charge_particles()
	pass # Replace with function body.

func charge_particles():
	if target != null:
		$ChargeParticles.restart()
		yield(get_tree().create_timer(1.0), "timeout")
		shot_laser()
		pass
	pass
	
func shot_laser():
	if target != null:
		# Shot laser
		var laser = LaserClass.instance()
		# Direction of canon
		#var desired_direction = (target.global_position - global_position).normalized()
		laser.global_rotation = global_rotation#desired_direction.angle()
		#laser.global_rotation = $Canon.global_rotation - 1.57
		laser.global_position = global_position
		laser.speed = 2000.0
		if increased_attack:
			laser.damage = damage * global.bonus_damage_increase_percent
		else:
			laser.damage = damage
		laser.pass_through = pass_through
		audio_shot.pitch_scale = rand_range(0.8,1.2)
		audio_shot.play()
		
		parent.call_deferred("add_child", laser) # get_node("..").add_child(laser)
	pass
