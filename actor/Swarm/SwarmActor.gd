extends KinematicBody2D

# Export
export var starting_health = 50
export var starting_rate = 1.0
export var starting_damage = 5
export var speed = 5000
export var turning_speed = 2000
export var stop_distance = 50

# Signal
signal landed
signal died(actor)

# Nodes
onready var audio_shot = $AudioStreamShot
onready var parent = get_node("..")

# Variables
var LaserClass = preload("res://actor/Swarm/Laser.tscn")
var explosion = preload("res://effects/SmallExplosion/SmallExplosion.tscn")
var harvestable = preload("res://level/Battlefield/HarvestableRessource.tscn")
var health = 0.0
var rate = 1.0
var damage = 5
var parent_tower = null
var target = null
var pass_through = false
var boost_damage = false
var boost_defense = false
var ground_in_fire = false

var increased_defense = false
var increased_attack = false

enum {
	SWARM_TAKING_OFF,
	SWARM_FLYING,
	SWARM_LANDING
}
var swarm_state = SWARM_TAKING_OFF

# Called when the node enters the scene tree for the first time.
func _ready():
	add_to_group("swarm")
	health = starting_health
	rate = starting_rate
	damage = starting_damage
	$GUI/LifeBar.max_value = starting_health
	$GUI/LifeBar.value = health
	$ShotTimer.wait_time = (1.0 / rate)
	#set_process(false)
	set_physics_process(false)
	$AnimationPlayer.play("takeoff")
	$AudioStreamTakeOff.play()
	randomize()
	
	#print(turning_speed)
	specific_ready()
	pass # Replace with function body.

func specific_ready():
	turning_speed = rand_range(turning_speed-(turning_speed*20.0/100.0), turning_speed+(turning_speed*20.0/100.0)) * (((randi() % 2) * 2) - 1)
	stop_distance = rand_range(stop_distance, stop_distance+(stop_distance*25.0/100.0))
	yield($AnimationPlayer,"animation_finished")
	#set_process(true)
	set_physics_process(true)
	$ShotTimer.start()
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
func _physics_process(delta):
	$GUI.global_rotation = 0
	specific_process(delta)
	pass
	
func specific_process(delta):
	if target != null:
		var dist = target.global_position.distance_to(global_position) - stop_distance
		var turn_around = Vector2(0,0)
		if dist < 200:
			turn_around = Vector2(0,1) * turning_speed * delta
		var current_speed = max(-speed, min(speed, dist*1))
		#if dist < 0:
			#current_speed = 1
		var desired_direction = (target.global_position - global_position).normalized() * current_speed * delta
		if dist < 0:
			global_rotation = desired_direction.angle() - PI
		else:
			global_rotation = desired_direction.angle()

		#print("start" + str(global_rotation))
		#print(turn_around)
		turn_around = turn_around.rotated(global_rotation)
		#print(turn_around)
		#move_and_slide(desired_direction+turn_around)
		global_position = global_position + desired_direction + turn_around
	else:
		var desired_direction = (parent_tower.global_position - global_position).normalized() * speed * delta
		global_rotation = desired_direction.angle()
		#laser.global_rotation = $Canon.global_rotation - 1.57
		#global_position = global_position
		#move_and_slide(desired_direction)
		global_position = global_position + desired_direction
		if parent_tower.global_position.distance_to(global_position) < 2:
			landing()
			emit_signal("landed")
			queue_free()
			pass
	pass

func flying():
	swarm_state = SWARM_FLYING
	pass
	
func landing():
	swarm_state = SWARM_LANDING
	$AnimationPlayer.play("landing")
	yield($AnimationPlayer,"animation_finished")
	pass

func modify_health(value):
	if value < 0:
		if increased_defense == true:
			health = health + value * global.bonus_defense_increase_percent
			pass
		else:
			health = health + value
			pass
		pass
	else:
		health = health + value
		pass
		
	$GUI/LifeBar.value = health
	if health >= (starting_health - 5):
		$GUI/LifeBar.visible = false
	else:
		$GUI/LifeBar.visible = true
	if value < 0:
		$Tween.interpolate_property($Sprite, "modulate", Color(1, 1, 1, 1), Color(0, 0, 0, 1), 0.05, Tween.TRANS_LINEAR, Tween.EASE_IN)
		$Tween.interpolate_property($Sprite, "modulate", Color(0, 0, 0, 1), Color(1, 1, 1, 1), 0.05, Tween.TRANS_LINEAR, Tween.EASE_IN, 0.05)
		$Tween.start()
	if health < (starting_health / 3.0):
			$InFire.emitting = true
	else:
		$InFire.emitting = false
	if health <= 0:
		emit_signal("died", self)
		destroy_instance()
	pass

func destroy_instance():
	var explo = explosion.instance()
	explo.global_position = global_position
	get_parent().get_parent().get_node("GroundObject").add_child(explo)
	explo.get_node("AnimationPlayer").play("explosion")
	EventBus.emit_signal("shake_camera", 0.1)
	EventBus.emit_signal("add_points", global.action_points["swarm_kill"])
	global.player_stats["swarm_kill"] += 1
	
	randomize()
	if rand_range(0,100) <= 25:
		call_deferred("drop_gold")
	
	queue_free()
	pass
	
func drop_gold():
	var new_gold = harvestable.instance()
	new_gold.global_position = global_position
	new_gold.value = 5
	get_tree().get_root().get_node_or_null("BattleField/GroundObject").add_child_below_node(get_tree().get_root().get_node_or_null("BattleField/GroundObject"), new_gold)

func _on_ShotTimer_timeout():
	if target != null:
		# Shot laser
		var laser = LaserClass.instance()
		# Direction of canon
		#var desired_direction = (target.global_position - global_position).normalized()
		laser.global_rotation = global_rotation#desired_direction.angle()
		#laser.global_rotation = $Canon.global_rotation - 1.57
		laser.global_position = global_position
		if increased_attack:
			laser.damage = damage * global.bonus_damage_increase_percent
		else:
			laser.damage = damage
		laser.pass_through = pass_through
		audio_shot.pitch_scale = rand_range(0.8,1.2)
		audio_shot.play()
		
		parent.call_deferred("add_child", laser) #add_child(laser)
		pass
	pass # Replace with function body.

func change_target(new_target):
	#print(new_target)
	target = new_target
	pass

func parent_died(_parent):
	queue_free()
	pass

func on_bonus_received(type):
	if type == global.ressource_type.RESSOURCE_INC_DEFENCE:
		increased_defense = true
		$IncDefenseTimer.stop()
		$IncDefenseTimer.start()
		$GUI/IncDefenseActivated.visible = true
		#print("Drone Defense increased")
		pass
	elif type == global.ressource_type.RESSOURCE_INC_DAMAGE:
		increased_attack = true
		$IncAttackTimer.stop()
		$IncAttackTimer.start()
		$GUI/IncAttackActivated.visible = true
		#print("Drone Attack increased")
		pass
	else:
		# No other possible bonus
		pass
	pass


func _on_IncDefenseTimer_timeout():
	increased_defense = false
	$GUI/IncDefenseActivated.visible = false
	#print("Drone Defense STOP increased")
	pass # Replace with function body.


func _on_IncAttackTimer_timeout():
	increased_attack = false
	$GUI/IncAttackActivated.visible = false
	#print("Drone Attack STOP increased")
	pass # Replace with function body.
