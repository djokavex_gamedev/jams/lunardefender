extends "res://actor/Swarm/SwarmActor.gd"

var BeamClass = preload("res://actor/Swarm/LaserFocusSwarm/FocusBeam.tscn")

var laser_shot = null

func _on_ShotTimer_timeout():
	if is_instance_valid(target) and (laser_shot == null or not is_instance_valid(laser_shot)):
		if global_position.distance_to(target.global_position) < (stop_distance + 75):
			# Shot laser
			laser_shot = BeamClass.instance()
			# Direction of canon
			laser_shot.target = target
			if increased_attack:
				laser_shot.start_damage = damage * global.bonus_damage_increase_percent
			else:
				laser_shot.start_damage = damage
			#$AudioStreamShot.pitch_scale = rand_range(0.8,1.2)
			#$AudioStreamShot.play()
			
			add_child(laser_shot)
		pass
	pass # Replace with function body.

func change_target(new_target):
	#print("new_target")
	if new_target == target:
		pass
	else:
		if is_instance_valid(laser_shot):
			laser_shot.queue_free()
		
		target = new_target
		if is_instance_valid(target):
			if global_position.distance_to(target.global_position) < (stop_distance + 75):
				laser_shot = BeamClass.instance()
				laser_shot.target = target
				if increased_attack:
					laser_shot.start_damage = damage * global.bonus_damage_increase_percent
				else:
					laser_shot.start_damage = damage
				add_child(laser_shot)
	
	pass
