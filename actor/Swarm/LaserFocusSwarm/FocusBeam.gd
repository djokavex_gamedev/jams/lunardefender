extends Line2D

var start_damage = 4
var increase_damage = 0.4
var current_damage = 4
var target = null
var current_size = 64

# Called when the node enters the scene tree for the first time.
func _ready():
	#print("New beam")
	current_damage = start_damage
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	#global_rotation = 0
	if not is_instance_valid(target):
		queue_free()
	else:
		var current_target_pose = to_local(target.global_position)
		#points[1]
		get_node("ColorRect").rect_size = Vector2(current_target_pose.x-8, current_size)
		get_node("ColorRect").rect_position = Vector2(8, -current_size/2.0)
	pass


func _on_DamageTimer_timeout():
	if is_instance_valid(target):
		target.modify_health(-current_damage)
	pass # Replace with function body.


func _on_IncreaseDamageTimer_timeout():
	current_damage = current_damage + start_damage * increase_damage
	#print(current_damage)
	if current_damage > 3 * start_damage:
		current_damage = 3 * start_damage
	else:
		#width = width + 0.5
		get_node("ColorRect").material.set_shader_param("energy", get_node("ColorRect").material.get_shader_param("energy") * 1.5)
		current_size = current_size * 1.1
	pass # Replace with function body.



