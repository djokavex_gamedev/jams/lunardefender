extends Area2D

var damage = 2

func _on_TickTimer_timeout() -> void:
	var overlapping_areas = get_overlapping_bodies()
	for area in overlapping_areas:
		area.modify_health(-damage)
	pass # Replace with function body.


func _on_LifeTimer_timeout() -> void:
	queue_free()
	pass # Replace with function body.
