extends Area2D

var explosion = preload("res://effects/BigExplosion/BigExplosion.tscn")
var fire = preload("res://actor/Swarm/BomberSwarm/FireArea.tscn")

var process_number = 0
var damage = 20
var ground_in_fire = false

# Called when the node enters the scene tree for the first time.
func _ready():
	if global.unlockable_skills["tree4"]["step4"]: # TODO
		get_node("CollisionShape2D").shape.radius = get_node("CollisionShape2D").shape.radius + get_node("CollisionShape2D").shape.radius * global.unlockable_skills["tree4"]["step4_value"]
	var explo = explosion.instance()
	explo.global_position = global_position
	get_tree().get_root().get_node_or_null("BattleField/GroundObject").add_child(explo)
	explo.get_node("AnimationPlayer").play("explosion")
	
	if ground_in_fire:
		var fire_instance = fire.instance()
		fire_instance.global_position = global_position
		get_tree().get_root().get_node_or_null("BattleField/GroundObject").add_child(fire_instance)
	
	
#	yield(get_tree(),"idle_frame")
#
#	var overlapping_areas = get_overlapping_bodies()
#	for area in overlapping_areas:
#		area.modify_health(-damage)
#	queue_free()
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	process_number = process_number + 1

	if process_number > 3:
		var overlapping_areas = get_overlapping_bodies()
		for area in overlapping_areas:
			area.modify_health(-damage)
		queue_free()
	pass
