extends "res://actor/Swarm/SwarmActor.gd"

# Export

# Signals 

# Variables
var floating_text = preload("res://assets/other/FloatingText.tscn")
var collected = false
var collected_gold = 0
var collected_super_gold = 0
var current_bonus = {
	global.ressource_type.RESSOURCE_REGEN_LIFE: 0,
	global.ressource_type.RESSOURCE_INC_DAMAGE: 0,
	global.ressource_type.RESSOURCE_INC_DEFENCE: 0,
	global.ressource_type.RESSOURCE_SLOW_ENEMIES: 0,
	global.ressource_type.RESSOURCE_METEOR: 0,
	global.ressource_type.RESSOURCE_LARGE_METEOR: 0,
}
var path_target = null
var path_index = 0

# Called when the node enters the scene tree for the first time.
func specific_ready():
	if global.unlockable_skills["tree5"]["step1"]:
		speed = speed + global.unlockable_skills["tree5"]["step1_value"]
		pass
	
	global_position = get_tree().get_root().get_node_or_null("BattleField/GroundObject/PlayerCore").global_position
	yield($AnimationPlayer,"animation_finished")
	#set_process(true)
	set_physics_process(true)
	$ShotTimer.start()
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func specific_process(delta):
	if target != null:
		if collected:
			var dist = parent_tower.global_position.distance_to(global_position)
			var turn_around = Vector2(0,0)
			var current_speed = min(speed, dist*3)
			var desired_direction = (parent_tower.global_position - global_position).normalized() * current_speed * delta
			if dist < 0:
				global_rotation = desired_direction.angle() - PI
			else:
				global_rotation = desired_direction.angle()
			
			turn_around = turn_around.rotated(global_rotation)
			#move_and_slide(desired_direction+turn_around)
			global_position = global_position + desired_direction+turn_around
			
			if parent_tower.global_position.distance_to(global_position) < 3:
				emit_signal("landed")
				give_gold_to_core()
				queue_free()
				pass
			
			pass
		else:
			var dist = target.distance_to(global_position) - stop_distance
			var turn_around = Vector2(0,0)
			if dist < stop_distance:
				turn_around = Vector2(0,1) * turning_speed * delta
			var current_speed = min(speed, dist*3)
			#if dist < 0:
				#current_speed = 1
			var desired_direction = (target - global_position).normalized() * current_speed * delta
			if dist < 0:
				global_rotation = desired_direction.angle() - PI
			else:
				global_rotation = desired_direction.angle()
			
			#print("start" + str(global_rotation))
			#print(turn_around)
			turn_around = turn_around.rotated(global_rotation)
			#print(turn_around)
			#move_and_slide(desired_direction+turn_around)
			global_position = global_position + desired_direction+turn_around
			
			if dist < (stop_distance+1):
				#print(path_target)
				#print(path_index)
				#print(path_target.size())
				if path_index < (path_target.size() - 1):
					# More points in path
					path_index = path_index + 1
					target = path_target[path_index]
				else:
					# Collect done
					collected = true
	else:
		var desired_direction = (parent_tower.global_position - global_position).normalized() * speed * delta
		global_rotation = desired_direction.angle()
		#laser.global_rotation = $Canon.global_rotation - 1.57
		#global_position = global_position
		#move_and_slide(desired_direction)
		global_position = global_position + desired_direction
		if parent_tower.global_position.distance_to(global_position) < 1:
			emit_signal("landed")
			give_gold_to_core()
			queue_free()
			pass
	
	pass

func give_gold_to_core():
	#global.gold_ressource = global.gold_ressource + collected_gold
	EventBus.emit_signal("add_gold", collected_gold)
	EventBus.emit_signal("add_points", collected_gold * global.action_points["gold"])
	global.player_stats["gold"] += collected_gold
	EventBus.emit_signal("add_super_gold", collected_super_gold)
	EventBus.emit_signal("add_points", collected_super_gold * global.action_points["super_gold"])
	global.player_stats["super_gold"] += collected_super_gold
	
	if current_bonus[global.ressource_type.RESSOURCE_REGEN_LIFE] > 0:
		EventBus.emit_signal("collect_bonus", current_bonus[global.ressource_type.RESSOURCE_REGEN_LIFE], global.ressource_type.RESSOURCE_REGEN_LIFE)
	if current_bonus[global.ressource_type.RESSOURCE_INC_DAMAGE] > 0:
		EventBus.emit_signal("collect_bonus", current_bonus[global.ressource_type.RESSOURCE_INC_DAMAGE], global.ressource_type.RESSOURCE_INC_DAMAGE)
	if current_bonus[global.ressource_type.RESSOURCE_INC_DEFENCE] > 0:
		EventBus.emit_signal("collect_bonus", current_bonus[global.ressource_type.RESSOURCE_INC_DEFENCE], global.ressource_type.RESSOURCE_INC_DEFENCE)
	if current_bonus[global.ressource_type.RESSOURCE_SLOW_ENEMIES] > 0:
		EventBus.emit_signal("collect_bonus", current_bonus[global.ressource_type.RESSOURCE_SLOW_ENEMIES], global.ressource_type.RESSOURCE_SLOW_ENEMIES)
	if current_bonus[global.ressource_type.RESSOURCE_METEOR] > 0:
		EventBus.emit_signal("collect_bonus", current_bonus[global.ressource_type.RESSOURCE_METEOR], global.ressource_type.RESSOURCE_METEOR)
	if current_bonus[global.ressource_type.RESSOURCE_LARGE_METEOR] > 0:
		EventBus.emit_signal("collect_bonus", current_bonus[global.ressource_type.RESSOURCE_LARGE_METEOR], global.ressource_type.RESSOURCE_LARGE_METEOR)
		
		
	pass

func _on_ShotTimer_timeout():
	pass


func _on_AreaCollector_area_entered(area):
	match area.type:
		global.ressource_type.RESSOURCE_GOLD:
			collected_gold = collected_gold + area.value
			EventBus.emit_signal("add_ground_gold", -area.value)
			var ftext = floating_text.instance()
			ftext.value = "+" + str(area.value)
			ftext.global_position = global_position
			ftext.color = Color("ffffff")
			$World.add_child(ftext)
			area.collect(self)
			pass
		global.ressource_type.RESSOURCE_SUPER_GOLD:
			collected_super_gold = collected_super_gold + area.value
			#EventBus.emit_signal("add_ground_gold", -area.value)
			var ftext = floating_text.instance()
			ftext.value = "+" + str(area.value)
			ftext.global_position = global_position
			ftext.color = Color("ff0000")
			$World.add_child(ftext)
			area.collect(self)
			pass
#		global.ressource_type.RESSOURCE_REGEN_LIFE:
#			area.call_deferred("activate")
#			pass
#		global.ressource_type.RESSOURCE_INC_DAMAGE:
#			area.call_deferred("activate")
#			pass
#		global.ressource_type.RESSOURCE_INC_DEFENCE:
#			area.call_deferred("activate")
#			pass
#		global.ressource_type.RESSOURCE_SLOW_ENEMIES:
#			area.call_deferred("activate")
#			pass
#		global.ressource_type.RESSOURCE_METEOR:
#			area.call_deferred("activate")
#			pass
		_:
			current_bonus[area.type] = current_bonus[area.type] + 1
			area.collect(self)
			pass
	pass # Replace with function body.
